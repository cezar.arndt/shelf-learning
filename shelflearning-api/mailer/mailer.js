const nodemailer = require("nodemailer")
const fs =  require('fs')
const path = require("path")

let transport = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  auth: {
    user: process.env.SMTP_AUTH_USER,
    pass: process.env.SMTP_AUTH_PASSWORD,
  },
});


const sendPasswordCode = ( to, code ) =>  {
  const subject = `[Shelflearning] Aqui esta o link para redefinir sua senha`
  const link = `${process.env.DOMAIN}/reset-password?code=${code}`

  let template = fs.readFileSync( path.resolve(__dirname,'resent-password-code-template.html')).toString()
  template = template
              .replace(/\[0\]/g, link)
              .replace(/\[1\]/g, process.env.VALIDATION_TOKEN_EXPIRES_HOURS + (process.env.VALIDATION_TOKEN_EXPIRES_HOURS == 1 ? ' Hora' : ' Horas'))
              .replace(/\[2\]/g, process.env.COMPANY_FOOTER)

  sendEmail(to, subject, template)
}

const sendNewAccount = ( to, name, code ) =>  {
  const subject = `[Shelflearning] Criação de conta`
  const link = `${process.env.DOMAIN}/activate-account`

  let template = fs.readFileSync( path.resolve(__dirname,'new-account-template.html')).toString()
  template = template
              .replace(/\[0\]/g, name)
              .replace(/\[1\]/g, link)
              .replace(/\[2\]/g, code)
              .replace(/\[3\]/g, process.env.VALIDATION_TOKEN_EXPIRES_HOURS + (process.env.VALIDATION_TOKEN_EXPIRES_HOURS == 1 ? ' Hora' : ' Horas'))
              .replace(/\[4\]/g, process.env.COMPANY_FOOTER)

  sendEmail(to, subject, template)
}

const sendActivationCode = ( to, code ) =>  {
  const subject = `[Shelflearning] Código para ativação de conta`
  const link = `${process.env.DOMAIN}/activate-account`

  let template = fs.readFileSync( path.resolve(__dirname,'resent-account-code-template.html')).toString()
  template = template
              .replace(/\[0\]/g, link)
              .replace(/\[1\]/g, code)
              .replace(/\[2\]/g, process.env.VALIDATION_TOKEN_EXPIRES_HOURS + (process.env.VALIDATION_TOKEN_EXPIRES_HOURS == 1 ? ' Hora' : ' Horas'))
              .replace(/\[3\]/g, process.env.COMPANY_FOOTER)

  sendEmail(to, subject, template)
}



const sendEmail = (to, subject, html) => {
  const message = {
    from: process.env.SMTP_FROM,
    to,
    subject,
    html,
  };

  transport.sendMail(message, function (err, info) {
    console.log(err || 'E-mail enviado');
  });
};

module.exports = { sendPasswordCode, sendNewAccount, sendActivationCode };
