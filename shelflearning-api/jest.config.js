module.exports = {
  testEnvironment: 'node',
  setupFiles: ["<rootDir>/.jest/env.vars.js"],
  setupFilesAfterEnv: ['<rootDir>/.jest/setup.js'],
  coverageReporters: ["text-summary", "html"]
}