const request = require('supertest')
const app = require('../../index')
const { commonAuthHeaders } = require('../utils')

class StudentAPI {

    endpoint = null
    token = null

    constructor(endpoint) {
        this.endpoint = endpoint
    }

    setToken(token) {
        this.token = token
        return this
    }

    async get(id) {
        const response = await request(app)
            .get(`${this.endpoint}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async list(filter=null) {
        const response = await request(app)
            .get(`${this.endpoint}${filter || ''}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async enroll(id) {
        const response = await request(app)
            .post(`${this.endpoint}/${id}/enroll`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async enrollment(filter=null) {
        const response = await request(app)
            .get(`${this.endpoint}/enrollment${filter || ''}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async enrollmentInfo() {
        const response = await request(app)
            .get(`${this.endpoint}/enrollment-info`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }
    

    async updateProgress(id, progress) {
        const response = await request(app)
            .put(`${this.endpoint}/${id}/progress`)
            .send(progress)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

}

module.exports = new StudentAPI('/api/v1/course/public')