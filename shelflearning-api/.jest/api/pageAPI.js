const request = require('supertest')
const app = require('../../index')
const { commonAuthHeaders } = require('../utils')

class PageAPI {

    endpoint = null
    token = null
    courseId = null
    sectionId = null

    constructor(endpoint) {
        this.endpoint = endpoint
    }

    setToken(token) {
        this.token = token
        return this
    }

    setCourse(courseId) {
        this.courseId = courseId
        return this
    }

    setSection(sectionId) {
        this.sectionId = sectionId
        return this
    }

    async create(page) {

        const url = this.endpoint
                    .replace(':courseId',this.courseId)
                    .replace(':sectionId',this.sectionId)

        const response = await request(app)
            .post(`${url}`)
            .send(page)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async update(id, page) {
        const url = this.endpoint
                    .replace(':courseId',this.courseId)
                    .replace(':sectionId',this.sectionId)

        const response = await request(app)
            .put(`${url}/${id}`)
            .send(page)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async updateDescription(id, page) {
        const url = this.endpoint
                    .replace(':courseId',this.courseId)
                    .replace(':sectionId',this.sectionId)

        const response = await request(app)
            .put(`${url}/${id}/description`)
            .send(page)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async updateOrder(id, page) {
        const url = this.endpoint
                    .replace(':courseId',this.courseId)
                    .replace(':sectionId',this.sectionId)

        const response = await request(app)
            .put(`${url}/${id}/order`)
            .send(page)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async get(id) {
        const url = this.endpoint
                    .replace(':courseId',this.courseId)
                    .replace(':sectionId',this.sectionId)

        const response = await request(app)
            .get(`${url}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async list(filter=null) {
        const url = this.endpoint
                    .replace(':courseId',this.courseId)
                    .replace(':sectionId',this.sectionId)

        const response = await request(app)
            .get(`${url}${filter || ''}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async remove(id) {
        const url = this.endpoint
                    .replace(':courseId',this.courseId)
                    .replace(':sectionId',this.sectionId)

        const response = await request(app)
            .del(`${url}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

}

module.exports = new PageAPI('/api/v1/course/instructor/:courseId/section/:sectionId/page')

