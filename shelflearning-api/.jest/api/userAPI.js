const request = require('supertest')
const app = require('../../index')
const { commonAuthHeaders } = require('../utils')

class UserAPI {

    endpoint = null
    token = null

    constructor(endpoint) {
        this.endpoint = endpoint
    }

    setToken(token) {
        this.token = token
        return this
    }

    async create(user) {

        const response = await request(app)
            .post(`${this.endpoint}`)
            .send(user)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async enable(id) {
        const response = await request(app)
            .put(`${this.endpoint}/${id}/enable`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async disable(id) {
        const response = await request(app)
            .put(`${this.endpoint}/${id}/disable`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async get(id) {
        const response = await request(app)
            .get(`${this.endpoint}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async list(filter=null) {
        const response = await request(app)
            .get(`${this.endpoint}${filter || ''}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

}

module.exports = new UserAPI('/api/v1/user')