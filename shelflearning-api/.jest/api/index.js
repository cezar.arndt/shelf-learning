const courseAPI = require('./courseAPI')
const sectionAPI = require('./sectionAPI')
const pageAPI = require('./pageAPI')
const userAPI = require('./userAPI')
const studentAPI = require('./studentAPI')
const tagAPI = require('./tagAPI')
const authAPI = require('./authAPI')

module.exports = {
    courseAPI,
    sectionAPI,
    pageAPI,
    userAPI,
    studentAPI,
    tagAPI,
    authAPI
}