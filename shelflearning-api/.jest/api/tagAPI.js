const request = require('supertest')
const app = require('../../index')
const { commonAuthHeaders } = require('../utils')

class TagAPI {

    endpoint = null
    token = null

    constructor(endpoint) {
        this.endpoint = endpoint
    }

    setToken(token) {
        this.token = token
        return this
    }

    async create(tag) {

        const response = await request(app)
            .post(`${this.endpoint}`)
            .send(tag)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async update(id, tag) {
        const response = await request(app)
            .put(`${this.endpoint}/${id}`)
            .send(tag)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async get(id) {
        const response = await request(app)
            .get(`${this.endpoint}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async list(filter=null) {
        const response = await request(app)
            .get(`${this.endpoint}${filter || ''}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }
    

    async deleteTag(id) {
        const response = await request(app)
            .delete(`${this.endpoint}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

}

module.exports = new TagAPI('/api/v1/tag')