const request = require('supertest')
const app = require('../../index')
const { commonAuthHeaders } = require('../utils')

class CourseAPI {

    endpoint = null
    token = null

    constructor(endpoint) {
        this.endpoint = endpoint
    }

    setToken(token) {
        this.token = token
        return this
    }

    async create(course) {

        const response = await request(app)
            .post(`${this.endpoint}`)
            .send(course)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async update(id, course) {
        const response = await request(app)
            .put(`${this.endpoint}/${id}`)
            .send(course)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async get(id) {
        const response = await request(app)
            .get(`${this.endpoint}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async getInfo(id) {
        const response = await request(app)
            .get(`${this.endpoint}/${id}/info`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async list(filter=null) {
        const response = await request(app)
            .get(`${this.endpoint}${filter || ''}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async changeStatus(id, history) {
        const response = await request(app)
            .put(`${this.endpoint}/${id}/status`)
            .send(history)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async remove(id) {
        const response = await request(app)
            .del(`${this.endpoint}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

}

module.exports = new CourseAPI('/api/v1/course/instructor')