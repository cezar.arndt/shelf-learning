const request = require('supertest')
const app = require('../../index')
const { commonAuthHeaders, commonHeaders } = require('../utils')

class AuthAPI {

    endpoint = null
    token = null

    constructor(endpoint) {
        this.endpoint = endpoint
    }

    setToken(token) {
        this.token = token
        return this
    }

    async login(user) {

        const response = await request(app)
            .post(`${this.endpoint}`)
            .send(user)
            .set(commonHeaders(this.token))

        return response.body
    }


    async me() {
        const response = await request(app)
            .get(`${this.endpoint}/me`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async basicInfo(basic) {
        const response = await request(app)
            .put(`${this.endpoint}/me/basic`)
            .send(basic)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async changePassword(currentPassword, newPassword, newPasswordConfirm) {
        const response = await request(app)
            .put(`${this.endpoint}/me/password`)
            .send({currentPassword, newPassword, newPasswordConfirm})
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async createAccount(account) {
        const response = await request(app)
            .post(`${this.endpoint}/create-account`)
            .send(account)
            .set(commonHeaders())

        return response.body
    }

    async resetActivationCode(account) {
        const response = await request(app)
            .post(`${this.endpoint}/reset-activation-code`)
            .send(account)
            .set(commonHeaders())

        return response.body
    }

    async resetPasswordCode(account) {
        const response = await request(app)
            .post(`${this.endpoint}/reset-password-code`)
            .send(account)
            .set(commonHeaders())

        return response.body
    }

    async activateAccount(validationToken) {
        const response = await request(app)
            .post(`${this.endpoint}/activate-account`)
            .send({validationToken})
            .set(commonHeaders())

        return response.body
    }

    async resetPassword(account, validationToken) {
        const response = await request(app)
            .post(`${this.endpoint}/reset-password`)
            .send({account, validationToken})
            .set(commonHeaders())

        return response.body
    }

    

}

module.exports = new AuthAPI('/api/v1/auth')