const request = require('supertest')
const app = require('../../index')
const { commonAuthHeaders } = require('../utils')

class SectionAPI {

    endpoint = null
    token = null
    courseId = null

    constructor(endpoint) {
        this.endpoint = endpoint
    }

    setToken(token) {
        this.token = token
        return this
    }

    setCourse(courseId) {
        this.courseId = courseId
        return this
    }

    async create(section) {

        const response = await request(app)
            .post(`${this.endpoint.replace(':courseId', this.courseId)}`)
            .send(section)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async updateTitle(id, section) {
        const response = await request(app)
            .put(`${this.endpoint.replace(':courseId', this.courseId)}/${id}/title`)
            .send(section)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async updateOrder(id, section) {
        const response = await request(app)
            .put(`${this.endpoint.replace(':courseId', this.courseId)}/${id}/order`)
            .send(section)
            .set(commonAuthHeaders(this.token))

        return response.body
    }


    async get(id) {
        const response = await request(app)
            .get(`${this.endpoint.replace(':courseId', this.courseId)}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async list(filter = null) {
        const response = await request(app)
            .get(`${this.endpoint.replace(':courseId', this.courseId)}${filter || ''}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

    async remove(id) {
        const url = this.endpoint
                    .replace(':courseId',this.courseId)

        const response = await request(app)
            .del(`${url}/${id}`)
            .set(commonAuthHeaders(this.token))

        return response.body
    }

}

module.exports = new SectionAPI('/api/v1/course/instructor/:courseId/section')