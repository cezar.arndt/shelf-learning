
const { courseAPI, sectionAPI, pageAPI } = require('./api')
const { COURSE_STATUS } = require('../schemas/enum')

async function bootstrap(token) {

    const courses = {
        notPublishied: {
            title: 'Curso não publicado',
            description: 'Curso de exemplo não publicado',
            tags: ['Nao publicado', 'Desenvolviment web'],
            sections: [
                {
                    title: "Secao de um curso nao publicado",
                    pages: [
                        { title: 'Pagina em progresso', description: '...' },
                    ]
                },
            ]
        },
        javascript: {
            title: 'Javascript Básico',
            description: 'Curso de javascript básico',
            tags: ['Javascript', 'Desenvolviment web'],
            sections: [
                {
                    title: "Configurando o ambiente",
                    pages: [
                        { title: 'Instalando NodeJS', description: 'Instalaçao do node JS' },
                        { title: 'Instalando VSCode', description: 'Instalaçao do vsCode' },
                        { title: 'Instalando do chrome', description: 'Instalaçao do chrome' }
                    ]
                },
                {
                    title: "Básico",
                    pages: [
                        { title: 'Console.log()', description: 'Utilização do console.log' },
                        { title: 'Comentários', description: 'Comentários do código' },
                        { title: 'Variáveis', description: 'Utilização do let' }
                    ]
                },
                {
                    title: "Lógica de programação",
                    pages: [
                        { title: 'if, else', description: 'Utilização de if\'s e else\'s' },
                        { title: 'Modelo HTML', description: 'HTML' },
                        { title: 'Loops', description: 'for, while, do while' }
                    ]
                }
            ]
        },
        html: {
            title: 'HTML basico',
            description: 'Curso de html básico',
            tags: ['Html', 'javascript', 'css', 'Desenvolviment web'],
            sections: [
                {
                    title: "Configurando o ambiente",
                    pages: [
                        { title: 'Instalando chrome', description: 'Instalaçao do chrome' },
                        { title: 'Instalando VSCode', description: 'Instalaçao do vsCode' },
                        { title: 'Instalando plugins', description: 'Instalaçao de plugins' }
                    ]
                }
            ]
        },
        python: {
            title: 'Fundamentos de Python',
            description: 'Curso de python básico',
            tags: ['python', 'Desenvolviment web', 'Flask'],
            sections: [
                {
                    title: "Configurando o ambiente",
                    pages: [
                        { title: 'Instalando phtyon 3', description: 'Instalaçao do python 3' },
                        { title: 'Instalação do pip', description: 'Gerenciador de dependências' }
                    ]
                },
                {
                    title: "Introdução",
                    pages: [
                        { title: 'O que é python', description: 'Descrição do python' },
                        { title: 'Algoritmo', description: 'Algoritmos em python' },
                        { title: 'Estrutura de dados', description: 'Estruturas de dados com python' }
                    ]
                },
                {
                    title: "Projeto",
                    pages: [
                        { title: 'Desafio', description: 'Criação da estrutura do projeto' },
                        { title: 'Encoding', description: 'utilização de encoding' },
                        { title: 'Obtendo dados', description: 'Utilização de dados' },
                        { title: 'Melhorando o projeto', description: 'Refactor do projeto com as melhores praticas' },
                    ]
                }
            ]
        }
    }


    courseAPI.setToken(token)
    let courseKeys = Object.keys(courses)
    for (let i = 0; i  < courseKeys.length; i ++) {
        let c = courseKeys[i];

        let course = await courseAPI.create(courses[c])
        sectionAPI.setToken(token).setCourse(course.id)

        for (let i = 0; i < courses[c].sections.length; i++) {
            const s = courses[c].sections[i]

            const section = await sectionAPI.create({ title: s.title })

            pageAPI.setToken(token).setSection(section.id).setCourse(course.id)
            for (let k = 0; k < s.pages.length; k++) {
                const p = s.pages[k]
                const pDB = await pageAPI.create(p)
                await pageAPI.updateDescription(pDB.id, { description: p.description })
            }
        }

        if (c !== 'notPublishied') {
            await courseAPI.changeStatus(course.id, { to: COURSE_STATUS.PUBLISHED, reason: 'Move course to publishied' })
        }
    }

}

module.exports = {
    bootstrap
}