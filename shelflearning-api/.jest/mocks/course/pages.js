const pages = {
    page1: { title: "Page 1", description: "Description" },
    page2: { title: "Page 2", description: "Description" },
    page3: { title: "Page 3", description: "Description" },
    page4: { title: "Page 4", description: "Description" },
    page5: { title: "Page 5", description: "Description" },

    pageNoTitle: { description: "No title" },
    pageNoDescription: { title: "No description", },
}

module.exports = pages