
const { COURSE_STATUS } = require('../../../schemas/enum')

const histories = {
    toBlocked: {
        to: COURSE_STATUS.BLOCKED,
        reason: 'Changed to Blocked'
    },
    toDraft: {
        to: COURSE_STATUS.DRAFT,
        reason: 'Changed to draft'
    },
    toPublished: {
        to: COURSE_STATUS.PUBLISHED,
        reason: 'Changed to Published'
    },
    toClosed: {
        to: COURSE_STATUS.CLOSED,
        reason: 'Changed to Closed'
    }
}

module.exports = histories