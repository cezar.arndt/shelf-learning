const sections = {
    section1: { title: "Section 1" },
    section2: { title: "Section 2" },
    section3: { title: "Section 3" },
    section4: { title: "Section 4" },
    section5: { title: "Section 5" },

    sectionNoTitle: {},
}

module.exports = sections