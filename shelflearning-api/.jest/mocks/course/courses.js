const courses = {
    simple1: { title: 'Simple course', tags: ['Tag 1', 'Tag 2', 'Tag 3', 'Tag 4', 'Tag 5'] },
    simple2: { title: 'Another course 2', tags: ['Tag 5'] },
    simple3: { title: 'Another course 3', tags: ['Tag 1', 'Tag 5'] },
    simple4: { title: 'Another course 4', tags: ['Tag 2', 'Tag 3', 'Tag 5'] },
    complete: { title: 'Complete course', description: 'Description of course', tags: ['Tag 1', 'Tag 2', 'Tag 3', 'Tag 4'] },
    noTags: { title: 'No tag course' },
    oneTag: { title: 'One tag course', tags: ['Single tag'] },

}

module.exports = courses