const tags = {
    javascript: {name:'Javascript'},
    html: {name:'Html'},
    css: {name:'Css'},
    java: {name:'Java'},
    scss: {name:'Scss'},
    react: {name:'React'},
    node: {name:'Node'},
    express: {name:'Express'},
}

module.exports = tags