const { USER_TYPE } = require('../../../schemas/enum')

const users = {
    admin: {
        name: 'Administrador',
        username: 'admin',
        password: '123456',
        type: USER_TYPE.ADMIN
    },
    notAdmin: {
        name: 'Not Admin',
        username: 'notadmin',
        password: '123456',
        type: USER_TYPE.USER
    },
    user1: {
        name: 'User 1',
        username: 'user1',
        password: '123456',
        type: USER_TYPE.USER
    },
    user2: {
        name: 'User 2',
        username: 'user2',
        password: '123456',
        type: USER_TYPE.USER
    },
    user3: {
        name: 'User 3',
        username: 'user3',
        password: '123456',
        type: USER_TYPE.USER
    }
}

module.exports = users