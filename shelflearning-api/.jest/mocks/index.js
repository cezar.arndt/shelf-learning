
const course = require('./course/courses')
const section = require('./course/sections')
const history = require('./course/history')
const page = require('./course/pages')
const user = require('./user/users')
const tag = require('./tag/tags')

module.exports = {
    course,
    section,
    history,
    user,
    page,
    tag,
    fakeId: '000000000000000000000000'
}