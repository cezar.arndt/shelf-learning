const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const request = require('supertest')
const app = require('../index')


const { User } = require('../schemas')
const { USER_TYPE } = require('../schemas/enum')


const commonAuthHeaders = (token) => {
    return Object.assign( (token ? { 'access-token': token } : {}), commonHeaders())
}

const commonHeaders = () => {
    return {
        'Content-Type': 'application/json'
    }
}

async function beforeEachSetup(options) {

    await mongoose.connect(process.env.MONGO_URL)
    await mongoose.connection.db.dropDatabase()

    const enabledUsers = []
    const disabledUsers = []
    const admins = []


    if (options.enabledUsers) {
        for (let i = 0, j = options.enabledUsers; i < j; i++) {
            const user = { username: `user.${i}`, password: '123456' }
            const userDB = await createUserEnabled(user.username, user.password)
            const loginRequest = await postLogin(user)

            enabledUsers.push({ username: userDB.username, id: userDB.id, token: loginRequest.token })
        }
    }

    if (options.disabledUsers) {
        for (let i = 0, j = options.disabledUsers; i < j; i++) {
            const user = { username: `user.disabled.${i}`, password: '123456' }
            const userDB = await createUserDisabled(user.username, user.password)
            disabledUsers.push({ username: userDB.username, id: userDB.id })
        }
    }

    if (options.admins) {
        for (let i = 0, j = options.admins; i < j; i++) {
            const admin = { username: `admin.${i}`, password: '123456' }
            const adminDB = await createAdmin(admin.username, admin.password)
            const loginRequest = await postLogin(admin)

            admins.push({ username: adminDB.username, id: adminDB.id, token: loginRequest.token })
        }
    }

    return {
        enabledUsers,
        admins,
        disabledUsers
    }

}


const createAdmin = async (username, password) => {
    return await createUser(username, password, null, USER_TYPE.ADMIN)
}

const createUserEnabled = async (username, password) => {
    return await createUser(username, password, null)
}

const createUserDisabled = async (username, password) => {
    return await createUser(username, password)
}


const createUser = async (username, password, disabledAt, type) => {

    var salt = bcrypt.genSaltSync(10)
    password = bcrypt.hashSync(password, salt)

    const user = await User.create({
        username,
        password,
        disabledAt,
        type,
        isActive:true
    })

    //TODO: WHAT?
    return JSON.parse(JSON.stringify(user.toJSON()))
}

const postLogin = async (user) => {
    const response = await request(app)
        .post('/api/v1/auth')
        .send(user)
        .set(commonHeaders())

    return response.body
}

module.exports = {
    beforeEachSetup,
    commonAuthHeaders,
    commonHeaders
}