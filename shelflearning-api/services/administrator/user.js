const { User } = require('../../schemas')


const bcrypt = require('bcryptjs')
const { forbidden, unauthorized } = require('../../validator/response')
const { throwIfEmpty, throwIf, diacriticSensitiveRegex } = require('../../validator/input')
const { throwIfUserNameExists, throwIfUserNotExists, throwIfUserIsDisabled, throwIfUserIsEnabled, throwIfNotAdmin } = require('../../validator/data')
const messages = require('../../validator/messages')
const { USER_TYPE } = require('../../schemas/enum')
const {  notFound } = require('../../validator/response')


const create = async (me, user) => {
    const { name, username, password, type } = user


    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)

    throwIfEmpty(username, messages.user.error.userNameNotFound)
    throwIfEmpty(password, messages.user.error.passwordNotFound)
    throwIfEmpty(type, messages.user.error.typeNotFound)

    await throwIfUserNameExists(username)


    const salt = bcrypt.genSaltSync(10)
    const EncryptPassword = bcrypt.hashSync(password, salt)

    const userDB = await User.create({
        name,
        username,
        password: EncryptPassword,
        type,
        disabledAt: null
    })

    return (await User.findById(userDB.id)).toJSON()
}

const enable = async (me, id) => {
    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)

    
    const user = await User.findById(id)

    throwIf(user.type === USER_TYPE.ADMIN, forbidden(messages.user.error.onlySuperUser))

    throwIfUserNotExists(user)
    throwIfUserIsEnabled(user)

    const update = await User.findByIdAndUpdate(id, {
        disabledAt: null
    }, { 'new': true })

    return update.toJSON()
}

const disable = async (me, id) => {
    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)


    const user = await User.findById(id)

    throwIf(user.type === USER_TYPE.ADMIN, forbidden(messages.user.error.onlySuperUser))

    throwIfUserNotExists(user)
    throwIfUserIsDisabled(user)

    const update = await User.findByIdAndUpdate(id, {
        disabledAt: Date.now()
    }, { 'new': true })

    return update.toJSON()
}

const get = async (me, id) => {
    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)


    let user
    try {
        user = await User.findById(id).select('disabledAt type  name username isActive')
    } catch(e) {
        throw new Error(JSON.stringify(notFound(messages.user.error.userNotFound)))
    }

    throwIfUserNotExists(user)

    return user.toJSON()
}

const list = async (me, filter) => {
    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)


    let filters = {}

    if (filter.name) {
        filters = {
            '$or': [
                { name: { '$regex': diacriticSensitiveRegex(filter.name), '$options': 'i' } },
                { username: { '$regex': diacriticSensitiveRegex(filter.name), '$options': 'i' } }
            ]
        }
    }

    const users = await User.find(filters).select('disabledAt type  name username isActive')
    return users
}

const exportData = async (me, filter) => {
    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)

    const users = await list(me, filter)

    const csvHeader = ['Id','Username','Nome','Desabilitado em', 'Tipo', 'Conta ativada' ]
    const csvBody = users.map((user) => [
      `"${user._id}"`,
      `"${user.username}"`,
      `"${user.name}"`,
        user.disabledAt ? `"${new Date(user.disabledAt).toLocaleString()}"` : null,
      `"${user.type}"`,
      user.isActive ? '"Sim"' : '"Não"',
    ]);

    const delimiter = ";"

    return (
        `${csvHeader.join(delimiter)}\n${csvBody.map( line => line.join(delimiter)).join('\n')}`
    )
}


module.exports = {
    create,
    enable,
    disable,
    exportData,
    get,
    list
}