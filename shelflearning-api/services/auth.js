const bcrypt = require("bcryptjs");
const axios = require("axios");
const { User, Course } = require("../schemas");
const { generateToken } = require("../JWTHelper");
const { badRequest, unauthorized } = require("../validator/response");
const { throwIfUserNotFound } = require("../validator/data");
const messages = require("../validator/messages");
const {
  throwIfEmpty,
  throwIf,
  throwIfPasswordInvalid,
  throwIfEmailInvalid,
} = require("../validator/input");
const { throwIfUserNameExists } = require("../validator/data");
const { USER_TYPE } = require("../schemas/enum");
const {
  sendPasswordCode,
  sendNewAccount,
  sendActivationCode,
} = require("../mailer/mailer");

const login = async (user) => {
  const { username, password } = user;

  throwIfEmpty(username, messages.auth.error.credentialsNotFound);
  throwIfEmpty(password, messages.auth.error.credentialsNotFound);

  const userDB = await User.findOne({
    username: username.toLowerCase(),
  }).select("name username disabledAt password type isActive");

  throwIfUserNotFound(userDB, messages.auth.error.credentialsWrong);

  const compare = bcrypt.compareSync(password, userDB.password);

  throwIf(!compare, badRequest(messages.auth.error.credentialsWrong));
  throwIf(userDB.disabledAt, unauthorized(messages.auth.error.userDisabled));
  throwIf(!userDB.isActive, unauthorized(messages.auth.error.userNotActive));

  return { token: generateToken(userDB) };
};

const me = async (me) => {
  const user = await User.findById(me);
  throwIfUserNotFound(user);
  throwIf(user.disabledAt, unauthorized(messages.auth.error.userDisabled));

  const hasCourses = (await Course.find({ createdBy: me }).select("_id"))
    .length;

  if (user.image) {
    user.image = user.image.toString("ascii");
  }

  const { id, name, username, image, type } = user.toJSON();

  return { id, name, username, image, type, hasCourses };
};

const basic = async (me, basicInfo) => {
  const { name, image = null } = basicInfo;

  const userDB = await User.findById(me);
  throwIfUserNotFound(userDB);

  throwIf(userDB.disabledAt, unauthorized(messages.auth.error.userDisabled));

  let imageToUpload = null;
  if (image) {
    imageToUpload = {
      data: Buffer.from(image.split(",")[1], "base64"),
      contentType: image.split(",")[0].split(":")[1].split(";")[0],
    };
  }

  const updatedUser = await User.findByIdAndUpdate(
    me,
    {
      name,
      image: imageToUpload,
    },
    { new: true }
  );

  const {
    name: nameUpdated,
    username: usernameUpdated,
    image: imageUpdated,
    type,
    id,
  } = updatedUser.toJSON();

  const hasCourses = (await Course.find({ createdBy: me }).select("_id"))
    .length;

  return {
    id,
    name: nameUpdated,
    username: usernameUpdated,
    image: imageUpdated,
    type,
    hasCourses,
  };
};

const changePassword = async (me, passwords) => {
  const { currentPassword, newPassword, newPasswordConfirm } = passwords;

  throwIfEmpty(currentPassword, messages.user.error.passwordNotFound);
  throwIfEmpty(newPassword, messages.user.error.newPasswordNotFound);

  throwIfPasswordInvalid(newPassword, newPasswordConfirm);

  let userDB = await User.findById(me).select("password");
  throwIfUserNotFound(userDB);
  throwIf(userDB.disabledAt, unauthorized(messages.auth.error.userDisabled));

  const compare = bcrypt.compareSync(currentPassword, userDB.password);
  throwIf(!compare, badRequest(messages.auth.error.currentPasswordWrong));

  const salt = bcrypt.genSaltSync(10);
  const EncryptPassword = bcrypt.hashSync(newPassword, salt);

  await User.findByIdAndUpdate(me, {
    password: EncryptPassword,
  });

  return null;
};

function generate_token(length) {
  //edit the token allowed characters
  var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-._~()'!*:@,;".split(
    ""
  );
  var b = [];
  for (var i = 0; i < length; i++) {
    var j = (Math.random() * (a.length - 1)).toFixed(0);
    b[i] = a[j];
  }
  return b.join("");
}

const createAccount = async (account) => {
  const { name, username, password, confirm } = account;

  throwIfEmpty(name, messages.user.error.nameNotFound);
  throwIfEmpty(username, messages.user.error.userNameNotFound);
  throwIfEmpty(password, messages.user.error.passwordNotFound);

  throwIfEmailInvalid(username);
  throwIfPasswordInvalid(password, confirm);

  await throwIfUserNameExists(username);

  const salt = bcrypt.genSaltSync(10);
  const EncryptPassword = bcrypt.hashSync(password, salt);

  const generatedToken = generate_token(20);
  const validationToken = bcrypt.hashSync(generatedToken, salt);

  let today = new Date();
  today.setHours(
    today.getHours() + parseInt(process.env.VALIDATION_TOKEN_EXPIRES_HOURS)
  );

  await User.create({
    name,
    username: username.toLowerCase(),
    password: EncryptPassword,
    type: USER_TYPE.USER,
    disabledAt: null,
    isActive: false,
    validationToken,
    tokenValidationTime: today,
  });

  sendNewAccount(username, name, generatedToken);

  return null;
};

const getUserByToken = async (validationToken) => {
  const usersDB = await User.find({ validationToken: { $ne: null } }).select(
    "username validationToken tokenValidationTime isActive"
  );

  throwIf(!usersDB, badRequest(messages.auth.error.invalidToken));

  let userDB = null;
  for (let i = 0, j = usersDB.length; i < j; i++) {
    const compare = bcrypt.compareSync(
      validationToken,
      usersDB[i].validationToken
    );

    if (compare) {
      userDB = usersDB[i];
      break;
    }
  }

  throwIf(!userDB, badRequest(messages.auth.error.invalidToken));

  return userDB;
};

const resetActivationCode = async (account) => {
  const [email, code] = await resetCode(account, "ACTIVATION");
  sendActivationCode(email, code);
};

const resetPasswordCode = async (account) => {
  const [email, code] = await resetCode(account, "PASSWORD");
  sendPasswordCode(email, code);
};

const resetCode = async (account, type) => {
  const { username } = account;

  throwIfEmpty(username, messages.auth.error.usernameNotFound);

  const userDB = await User.findOne({
    username: username.toLowerCase(),
  }).select("username validationToken tokenValidationTime isActive");

  throwIfUserNotFound(userDB);

  if (type === "ACTIVATION") {
    throwIf(userDB.isActive, badRequest(messages.auth.error.userAlreadyActive));
  } else if (type === "PASSWORD") {
    throwIf(!userDB.isActive, badRequest(messages.auth.error.userNotActive));
  }

  const salt = bcrypt.genSaltSync(10);
  const generatedToken = generate_token(20);
  const validationToken = bcrypt.hashSync(generatedToken, salt);

  let today = new Date();
  today.setHours(
    today.getHours() + parseInt(process.env.VALIDATION_TOKEN_EXPIRES_HOURS)
  );

  await User.findByIdAndUpdate(userDB.id, {
    validationToken,
    tokenValidationTime: today,
  });

  return [userDB.username, generatedToken];
};

const resetPassword = async (body) => {
  const {
    account: { password, confirm },
    validationToken,
  } = body;

  throwIfEmpty(validationToken, messages.auth.error.validationTokenNotFound);
  throwIfEmpty(password, messages.user.error.passwordNotFound);
  throwIfPasswordInvalid(password, confirm);

  let userDB = await getUserByToken(validationToken);

  let now = new Date();
  let tokenValidationTime = new Date(userDB.tokenValidationTime);

  throwIf(
    now.getTime() > tokenValidationTime.getTime(),
    badRequest(messages.auth.error.expiredToken)
  );

  const salt = bcrypt.genSaltSync(10);
  const EncryptPassword = bcrypt.hashSync(password, salt);

  await User.findByIdAndUpdate(userDB.id, {
    password: EncryptPassword,
    validationToken: null,
    tokenValidationTime: null,
  });
};

const activateAccount = async (body) => {
  const { validationToken } = body;

  throwIfEmpty(validationToken, messages.auth.error.validationTokenNotFound);

  let userDB = await getUserByToken(validationToken);

  let now = new Date();
  let tokenValidationTime = new Date(userDB.tokenValidationTime);

  throwIf(
    now.getTime() > tokenValidationTime.getTime(),
    badRequest(messages.auth.error.expiredToken)
  );

  await User.findByIdAndUpdate(userDB.id, {
    isActive: true,
    validationToken: null,
    tokenValidationTime: null,
  });

  return null;
};

const externalLogin = async (body) => {
  const { provider } = body;

  switch (provider) {
    case "Facebook":
      return authenticateWithFacebook(body.code);
  }
};

const authenticateWithFacebook = async (code) => {
  try {
    const { data } = await axios({
      url: "https://graph.facebook.com/v4.0/oauth/access_token",
      method: "get",
      params: {
        client_id: process.env.FACEBOOK_APP_ID,
        client_secret: process.env.FACEBOOK_SECRET,
        code,
        redirect_uri: `${process.env.DOMAIN}/authenticate/facebook`,
      },
    });

    if (data.access_token) {
      const { data: userdata } = await axios({
        url: "https://graph.facebook.com/me",
        method: "get",
        params: {
          fields: [
            "id",
            "email",
            "first_name",
            "last_name",
            "picture.width(500).height(500)",
          ].join(","),
          access_token: data.access_token,
        },
      });

      let userDB = await User.findOne({ username: userdata.email });

      let imageToUpload = null;

      if (!userDB) {
        if (userdata.picture) {
          const res = await axios.get(userdata.picture.data.url, {
            responseType: 'arraybuffer'
          });
          if (res.data) {
            imageToUpload = {
              data:  res.data,
              contentType: res.headers["content-type"],
            };
          }
        }

        await User.create({
          name: `${userdata.first_name} ${userdata.last_name}`,
          username: userdata.email.toLowerCase(),
          type: USER_TYPE.USER,
          disabledAt: null,
          isActive: true,
          image: imageToUpload,
          externalProviderToken: data.access_token,
        });

        userDB = await User.findOne({ username: userdata.email });
      }

      return { token: generateToken(userDB) };
    }
  } catch (e) {
    console.error(e)
    if (e.response.data.error.code === 100) {
      throw new Error(
        JSON.stringify(badRequest(messages.auth.error.facebookExpiredToken))
      );
    }

    throw new Error(JSON.stringify(badRequest(messages.auth.error.unknow)));
  }
};

module.exports = {
  login,
  me,
  createAccount,
  activateAccount,
  resetActivationCode,
  resetPasswordCode,
  resetPassword,
  basic,
  changePassword,
  externalLogin,
};
