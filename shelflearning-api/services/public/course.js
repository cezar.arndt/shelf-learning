const { Course, UserCourse, Page, User } = require('../../schemas')

const { diacriticSensitiveRegex, throwIfEmpty } = require('../../validator/input')

const { throwIfCourseNotFound, throwIfPageNotFound, throwIfUserNotEnrolled, throwIfCourseIsNotPublishied, throwIfCourseIsNotPublishiedOrClosed, throwIfStatusDraft, throwIfUserAlreadyEnrolled, throwIfCourseBelongsToMe, throwIfAdmin } = require('../../validator/data')
const messages = require('../../validator/messages')
const { COURSE_STATUS, USER_TYPE } = require('../../schemas/enum')
const { notFound } = require('../../validator/response')

const withPopulate = async (course) => {
    return (await course
        .populate('createdBy', ['name'])
        .populate('tags', ['name'])
        .populate('sections.pages', ['title', 'order'])
        .execPopulate()
    ).organize().toJSON()
}


const list = async (filter, me, enrolledOnly = false) => {
    let find = []

    let or = []
    let match = {}

    const meCourses = await UserCourse.find({ user: me }).select('-pagesDone -sectionsDone')

    if (filter) {

        if (filter.title) {
            or.push({ 'title': { '$regex': diacriticSensitiveRegex(filter.title || ''), '$options': 'i' } })
        }

        if (filter.tags) {
            filter.tags.split(',').forEach(t => {
                or.push({ 'tags.name': { '$regex': diacriticSensitiveRegex(t), '$options': 'i' } })
            })
        }

        if (filter.filter) {
            or = []
            or.push({ 'tags.name': { '$regex': diacriticSensitiveRegex(filter.filter), '$options': 'i' } })
            or.push({ 'title': { '$regex': diacriticSensitiveRegex(filter.filter), '$options': 'i' } })
            or = [{ $or: or }]
        }
    }
    
    if (enrolledOnly) {
        match = {
            $and: [
                { _id: { $in: meCourses.map(e => e.course)} },
                { $or: [{ status: COURSE_STATUS.PUBLISHED }, { status: COURSE_STATUS.CLOSED } , { status: COURSE_STATUS.BLOCKED }] },
                ...or,
            ]
        }
    } else {

        const user = await User.findById(me)
        if (user.type === USER_TYPE.ADMIN) {

            match = {
                $and: [
                    ...or,
                    { $or:[
                        {status: COURSE_STATUS.PUBLISHED }, 
                        { status: COURSE_STATUS.CLOSED }, 
                        { status: COURSE_STATUS.BLOCKED }
                    ]}
                ]
            }
        } else {
            match = {
                $and: [
                    { status: COURSE_STATUS.PUBLISHED },
                    ...or
                ]
            }
        }

        
    }

    let lookups = [
        {
            $lookup: { from: 'tags', localField: 'tags', foreignField: '_id', as: 'tags' }
        },
        {
            $lookup: { from: 'users', localField: 'createdBy', foreignField: '_id', as: 'createdBy' }
        }
    ]

    find = await Course.aggregate([
        ...lookups,
        {
            $match: match
        }
    ]).sort( { createdAt: -1 } )


    find = find.map(course => {

        const pagesCount = course.sections.reduce((acc, section) => { acc += section.pages.length; return acc }, 0)
        const sectionsCount = course.sections.length
        const tags = course.tags.map(t => t.name)
        course.sections = []

        const { __v, _id, description, statusHistory, createdBy, ...c } = course

        c.id = _id
        c.createdBy = { id: createdBy[0]._id, name: createdBy[0].name }

        let progress = meCourses.find(e => _id.equals(e.course) )?.progress

        return { ...c, pagesCount, sectionsCount, tags, progress }
    })

    return find
}

const enrollment = async (filter, me) => {
    await throwIfAdmin(me)

    const enrolledOnly = true
    return await list(filter, me, enrolledOnly)
}

const enrollmentInfo = async (me) => {
    await throwIfAdmin(me)

    const subs = await UserCourse.find({ user: me })
    return subs
}

const get = async (id, me) => {

    let courseDB

    try {
        courseDB = await Course.findById(id)
    } catch( e ) {
        throw new Error(JSON.stringify(notFound(messages.course.error.courseNotFound)))
    }
    

    throwIfCourseNotFound(courseDB)
    throwIfStatusDraft(courseDB)

    const user = await User.findById(me)
    if (user.type !== USER_TYPE.ADMIN ) {
        throwIfCourseIsNotPublishiedOrClosed(courseDB)
    } else if( courseDB.status === COURSE_STATUS.BLOCKED ){
        
        let { statusHistory:last } = await Course.findById(id, "statusHistory")
        courseDB.statusHistory = last[last.length-1]
    }

    if( courseDB.status === COURSE_STATUS.CLOSED ){
        if (user.type !== USER_TYPE.ADMIN && !courseDB.createdBy.equals(user._id)) {
            await throwIfUserNotEnrolled(courseDB, me, messages.course.error.courseNotFound)
        }
    }

    courseDB.sections.sort((a, b) => a.order < b.order ? -1 : 1)

    for (let i = 0, j = courseDB.sections.length; i < j; i++) {
        const section = courseDB.sections[i]

        for (let m = 0, n = section.pages.length; m < n; m++) {
            courseDB.sections[i].pages[m] = (await Page.findById(section.pages[m], 'title order descriptionLength'))
        }

        courseDB.sections[i].pages.sort((a, b) => a.order < b.order ? -1 : 1)
    }

    if (courseDB.image) {
        courseDB.image = courseDB.image.toString('ascii')
    }

    const pagesCount = courseDB.sections.reduce((acc, section) => { acc += section.pages.length; return acc }, 0)
    const sectionsCount = courseDB.sections.length

    const c = await withPopulate(courseDB)

    const meCourses = await UserCourse.find({ user: me }).select('-pagesDone -sectionsDone')
    let progress = meCourses.find(e => e.course.equals(courseDB.id) )?.progress

    return { ...c, pagesCount, sectionsCount, progress }
}

const enroll = async (me, id) => {

    await throwIfAdmin(me, messages.course.error.adminCantEnroll)

    const courseDB = (await Course.findById(id)).organize()
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToMe(courseDB, me)
    throwIfCourseIsNotPublishied(courseDB)
    await throwIfUserAlreadyEnrolled(courseDB, me)

    await UserCourse.create({
        user: me,
        course: id,
        currentPage: courseDB.sections[0].pages[0]
    })

    return null
}


const updateProgress = async (me, id, progress) => {
    await throwIfAdmin(me, messages.course.error.adminCantProgress)

    const { currentPage, isEndPage } = progress

    if (!isEndPage) {
        throwIfEmpty(currentPage, messages.course.error.currentPageNotInformed)
    }

    const courseDB = (await Course.findById(id))
    await throwIfUserNotEnrolled(courseDB, me)
    throwIfCourseNotFound(courseDB)
    throwIfCourseIsNotPublishiedOrClosed(courseDB)


    const page = courseDB.sections.some(s => s.pages.map(p => p._id).indexOf(currentPage) > -1)
    throwIfPageNotFound(page)


    let userCourse = await UserCourse.findOne({ course: id, user: me })


    if (!isEndPage || userCourse.pagesDone.indexOf(currentPage) > -1) {
        await UserCourse.findOneAndUpdate({ _id: userCourse.id }, {
            currentPage,
            lastAccessAt: Date.now()
        })
    }
    else {
        const [nextPage, pagesDone, sectionsDone, progress] = updateProgressPct(userCourse, currentPage, courseDB)

        userCourse = await UserCourse.findOneAndUpdate({ _id: userCourse.id }, {
            currentPage: nextPage,
            pagesDone,
            progress,
            sectionsDone,
            lastAccessAt: Date.now()
        })
    }

    return (await UserCourse.findById(userCourse.id)).toJSON()

}


const updateProgressPct = (userCourse, currentPage, courseDB) => {
    const pagesDone = [...userCourse.pagesDone]
    const sectionsDone = [...userCourse.sectionsDone]
    let nextPage = null
    let progress = userCourse.progress

    pagesDone.push(currentPage)



    for (let i = 0, j = courseDB.sections.length; i < j; i++) {


        const section = courseDB.sections[i]

        if( sectionsDone.length && sectionsDone.find( e => e.equals(section.id) ) ) {
            continue
        }

        const index = section.pages.findIndex(p => p.equals(currentPage))

        if (section.pages[index + 1]) {
            nextPage = section.pages[index + 1]
            break

        } else {
            sectionsDone.push(section.id)
            if (courseDB.sections[i + 1]) {
                nextPage = courseDB.sections[i + 1].pages[0]
            }
            break
        }
    }


    const totalPages = courseDB.sections.reduce((amount, section) => amount += section.pages.length, 0)
    progress = Math.floor(pagesDone.length / totalPages * 100)
    return [nextPage, pagesDone, sectionsDone, progress]
}

const getPage = async (me, courseId, id) => {

    let courseDB
    
    try {
        courseDB = await Course.findById(courseId)
    } catch( e ) {
        throw new Error(JSON.stringify(notFound(messages.course.error.courseNotFound)))
    }

    throwIfCourseNotFound(courseDB)

    const user = await User.findById(me)

    if (user.type !== USER_TYPE.ADMIN && !courseDB.createdBy.equals(user._id)) {
        await throwIfUserNotEnrolled(courseDB, me)
        throwIfCourseIsNotPublishiedOrClosed(courseDB)
    }

    let pageDB
    try {
        pageDB = await Page.findById(id)
    } catch( e ) {
        throw new Error(JSON.stringify(notFound(messages.course.page.error.pageNotFound)))
    }

    throwIfPageNotFound(pageDB)

    return pageDB.toJSON()
}

const exportData = async (me, filter ) => {
    return __exportData(await list(filter, me, false))
}

const exportDataEnroll =  async (me, filter ) => {
    return __exportDataEnroll(await list(filter, me, true))
}

const __exportData =  async (courses) => {

    const delimiter = ";"

    const csvHeader = ['Titulo', 'Seções/Páginas','Status', 'tags', 'professor' ]
    const csvBody = courses.map((course) => [
      `"${course.title}"`,
      `"${course.sectionsCount} Seções /${course.pagesCount} Páginas"`,
      `"${course.status}"`,
      `"${course.tags}"`,
      `"${course.createdBy.name}"`
    ]);

    return (
        `${csvHeader.join(delimiter)}\n${csvBody.map( line => line.join(delimiter)).join('\n')}`
    )
}

const __exportDataEnroll =  async (courses) => {

    const delimiter = ";"

    const csvHeader = ['Titulo', 'Seções/Páginas','Status', 'tags', 'professor', 'Progresso' ]
    const csvBody = courses.map((course) => [
      `"${course.title}"`,
      `"${course.sectionsCount} Seções /${course.pagesCount} Páginas"`,
      `"${course.status}"`,
      `"${course.tags}"`,
      `"${course.createdBy.name}"`,
      `"${course.progress}%"`
    ]);

    return (
        `${csvHeader.join(delimiter)}\n${csvBody.map( line => line.join(delimiter)).join('\n')}`
    )
}


const exportTree =  async (id, me) => {

    const course = await get(id, me)
    const delimiter = ";"

    const usercourse = await UserCourse.findOne({course:id,user:me})

    const csvHeader = usercourse ? [course.title,  `Progress (${usercourse.progress})`] : [course.title]
    const csvBody = course.sections.map( section => {
        return `        ${section.title} ${ usercourse?.sectionsDone.indexOf(section.id) > -1 ? `${delimiter}*` : '' }\n${ section.pages.map( page => {
            return `                ${page.title} ${ usercourse?.pagesDone.indexOf(page.id) > -1 ? `${delimiter}*` : '' }`
        }).join('\n') }`
    }).join('\n');

    return (
        `${csvHeader.join(delimiter)}\n\nEstrutura${ usercourse ? delimiter + 'Visualizado' : '' }\n${csvBody}`
    )
}


module.exports = {
    get,
    list,
    enroll,
    enrollmentInfo,
    enrollment,
    updateProgress,
    getPage,
    exportData,
    exportDataEnroll,
    exportTree
}