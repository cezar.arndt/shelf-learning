const mongoose = require('mongoose')
const { Course, User, Page } = require('../../schemas')
const tagService = require('../tag')

const { badRequest, notFound } = require('../../validator/response')
const { throwIfEmpty, isSameValue, throwIf, diacriticSensitiveRegex } = require('../../validator/input')
const { throwIfTitleExists, throwIfCourseNotFound, throwIfPublishiedAndHasNoPagesOrEmpty, throwIfStatusChangeNotAllowed, throwIfCourseBelongsToOther, throwIfStatusNotDraft, throwIfNotAdmin, throwIfAdmin } = require('../../validator/data')
const messages = require('../../validator/messages')
const { USER_TYPE, COURSE_STATUS } = require('../../schemas/enum')


const withPopulate = async (course) => {
    return (await course
        .populate('createdBy', ['name'])
        .populate('tags', ['name'])
        .execPopulate()
    ).toJSON()
}

const create = async (me, course) => {  

    await throwIfAdmin(me)

    const { title, description, tags: tagsName, image = null } = course
    const createdBy = me

    throwIfEmpty(title, messages.course.error.titleNotFound)
    throwIfEmpty(tagsName, messages.course.error.tagNotFound)
    
    await throwIfTitleExists(title)

    const tags = await tagService.getTagsId(tagsName)


    let imageToUpload = null
    if (image) {
        imageToUpload = {
            data: Buffer.from(image.split(',')[1], 'base64'),
            contentType: image.split(',')[0].split(':')[1].split(';')[0]
        }
    }

    const insert = await Course.create({
        title,
        description,
        createdBy,
        tags,
        image: imageToUpload
    })

    return withPopulate(insert)
}

const update = async (me, id, course) => {

    await throwIfAdmin(me)

    const { title, description, tags: tagsName, image = null } = course

    throwIfEmpty(title, messages.course.error.titleNotFound)
    throwIfEmpty(tagsName, messages.course.error.tagNotFound)

    const courseDB = await Course.findById(id)

    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)

    throwIf( [COURSE_STATUS.DRAFT,COURSE_STATUS.BLOCKED].indexOf(courseDB.status) == -1 , badRequest(messages.course.error.notDraftOrBlocked))
    
    if (!isSameValue(courseDB.title, title)) {
        await throwIfTitleExists(title)
    }

    const tags = await tagService.getTagsId(tagsName)

    let imageToUpload = null
    if (image) {
        imageToUpload = {
            data: Buffer.from(image.split(',')[1], 'base64'),
            contentType: image.split(',')[0].split(':')[1].split(';')[0]
        }
    }

    const update = await Course.findByIdAndUpdate(id, {
        title,
        description,
        tags,
        image: imageToUpload
    }, { 'new': true })

    return withPopulate(update)
}

const get = async (me, id) => {

    await throwIfAdmin(me)

    let courseDB
    try {
        courseDB = await Course.findById(id)
    } catch( e ) {
        throw new Error(JSON.stringify(notFound(messages.course.error.courseNotFound)))
    }

    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)

    courseDB.sections.sort((a, b) => a.order < b.order ? -1 : 1)

    for (let i = 0, j = courseDB.sections.length; i < j; i++) {
        const section = courseDB.sections[i]

        for (let m = 0, n = section.pages.length; m < n; m++) {
            courseDB.sections[i].pages[m] = (await Page.findById(section.pages[m], 'title order descriptionLength'))
        }

        courseDB.sections[i].pages.sort((a, b) => a.order < b.order ? -1 : 1)
    }

    let { statusHistory:last } = await Course.findById(id, "statusHistory")
    courseDB.statusHistory = last[last.length-1]

    if (courseDB.image) {
        courseDB.image = courseDB.image.toString('ascii')
    }

    return withPopulate(courseDB)
}

const getInfo = async (me, id) => {

    await throwIfAdmin(me)

    let courseDB
    try {
        courseDB = await Course.findById(id, '-sections')
    } catch( e ) {
        throw new Error(JSON.stringify(notFound(messages.course.error.courseNotFound)))
    }


    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)

    return withPopulate(courseDB)
}

const list = async (me, filter) => {

    await throwIfAdmin(me)

    let match = {}

    match = {
        $and: [
            { createdBy: mongoose.Types.ObjectId(me) }
        ]
    }

    if (filter.filter) {
        match.$and.push({
            title:  { '$regex': diacriticSensitiveRegex(filter.filter), '$options': 'i' }
        })
    }

    let lookups = [
        { 
            $lookup: { from: 'usercourses', localField: '_id', foreignField: 'course', as: 'students' } 
        },
        {
            $lookup: { from: 'tags', localField: 'tags', foreignField: '_id', as: 'tags' }
        },
        {
            $lookup: { from: 'pages', localField: '_id', foreignField: 'course', as: 'pages' }
        }
    ]

    let courses = await Course.aggregate([
        ...lookups,
        {
            $match: match
        },
        { $addFields: { "studentsCount": { $size: "$students" } } },
        { $addFields: { "pagesCount": { $size: "$pages" } } },
        { $addFields: { "sectionsCount": { $size: "$sections" } } },
        {
            $project:{
                title:1,
                studentsCount: 1,
                pagesCount:1,
                sectionsCount:1,
                tags:{
                    name:1
                },
                status:1
            }
        }
    ])

    courses = courses.map(course => {
        course.tags = course.tags.map( tag => tag.name)
        course.id = course._id
        delete course._id
        return course
    })

    return courses
}

const updateStatus = async (me, id, history) => {
    
    const { to, reason } = history

    throwIfEmpty(to, messages.course.error.history.toNotFound)
    throwIfEmpty(reason, messages.course.error.history.reasonNotFound)

    let courseDB = await Course.findById(id)
    const user = await User.findById(me)

    throwIfCourseNotFound(courseDB)

    if (!isSameValue(user.type, USER_TYPE.ADMIN)) {
        throwIfCourseBelongsToOther(courseDB, me)
    }


    await throwIfStatusChangeNotAllowed(courseDB, user, to)

    await throwIfPublishiedAndHasNoPagesOrEmpty(courseDB, to)

    const statusHistory = {
        changedBy: me,
        from: courseDB.status,
        to,
        reason
    }

    courseDB = await Course.findByIdAndUpdate(id, { status: to, $push: { statusHistory } }, { 'new': true })

    return withPopulate(courseDB)
}


const remove = async (me, id) => {

    await throwIfAdmin(me)
    
    const courseDB = await Course.findById(id)

    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfStatusNotDraft(courseDB)
    
    let sections = courseDB.toJSON().sections

    for( let i = 0; i < sections.length; i++ ){
        for( let m = 0; m < sections[i].pages.length; m++ ){
            await Page.findByIdAndDelete(sections[i].pages[m].id)
        }
    }

    await Course.findByIdAndDelete(id)

    return null
}

const exportData =  async (me, filter) => {

    const courses = await list(me,filter)
    const delimiter = ";"

    const csvHeader = ['Id','Titulo', 'Status', 'Alunos', 'Seções/Páginas', 'tags' ]
    const csvBody = courses.map((course) => [
      `"${course.id}"`,
      `"${course.title}"`,
      `"${course.status}"`,
      `${course.studentsCount}`,
      `"${course.sectionsCount} Seções /${course.pagesCount} Páginas"`,
      `"${course.tags}"`
    ]);

    return (
        `${csvHeader.join(delimiter)}\n${csvBody.map( line => line.join(delimiter)).join('\n')}`
    )
}

const exportTree =  async (me, id) => {

    const course = await get(me,id)
    const delimiter = ";"

    const csvHeader = [course.title]
    const csvBody = course.sections.map( section => {
        return `        ${section.title}\n${ section.pages.map( page => {
            return `                ${page.title} ${ page.descriptionLength === 0 ? '(Vazio)' : '' }`
        }).join('\n') }`
    }).join('\n');

    return (
        `${csvHeader.join(delimiter)}\n\nEstrutura\n${csvBody}`
    )
}

module.exports = {
    create,
    update,
    get,
    getInfo,
    list,
    updateStatus,
    exportData,
    exportTree,
    remove
}