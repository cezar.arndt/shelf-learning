const { Course, Page } = require('../../schemas')

const { badRequest } = require('../../validator/response')
const { throwIfEmpty, throwIf, isSameValue } = require('../../validator/input')
const { throwIfCourseNotFound, throwIfCourseBelongsToOther, throwIfStatusNotDraft, throwIfSectionNotFound, throwIfPageNotFound, throwIfPageTitleExists } = require('../../validator/data')
const messages = require('../../validator/messages')
const { COURSE_STATUS } = require('../../schemas/enum')



const create = async (me, courseId, sectionId, page) => {
    const { title } = page

    throwIfEmpty(title, messages.course.page.error.titleNotFound)

    let courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfStatusNotDraft(courseDB)
    throwIfSectionNotFound(courseDB, sectionId)

    await throwIfPageTitleExists(courseId, sectionId, title)

    const section = courseDB.sections.find(s => s._id.equals(sectionId))

    const pageDB = await Page.create({
        title,
        course: courseId,
        order: section.pages.length
    })

    await Course.findOneAndUpdate({ _id: courseId, 'sections._id': sectionId }, { $push: { 'sections.$.pages': pageDB } }, { 'new': true })

    return pageDB.toJSON()
}

const update = async (me, courseId, sectionId, id, page) => {
    const { title } = page

    throwIfEmpty(title, messages.course.page.error.titleNotFound)

    let courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIf( [COURSE_STATUS.DRAFT,COURSE_STATUS.BLOCKED].indexOf(courseDB.status) == -1 , badRequest(messages.course.error.notDraftOrBlocked))
    throwIfSectionNotFound(courseDB, sectionId)


    const pageDB = await Page.findById(id)
    throwIfPageNotFound(pageDB)

    if (!isSameValue(pageDB.title, title)) {
        await throwIfPageTitleExists(courseId, sectionId, title)
    }

    const update = await Page.findByIdAndUpdate(id, {
        title,
    }, { 'new': true })

    return update.toJSON()
}


const updateDescription = async (me, courseId, sectionId, id, page) => {
    const { description } = page

    throwIfEmpty(description, messages.course.page.error.descriptionNotFound)

    let courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIf( [COURSE_STATUS.DRAFT,COURSE_STATUS.BLOCKED].indexOf(courseDB.status) == -1 , badRequest(messages.course.error.notDraftOrBlocked))
    throwIfSectionNotFound(courseDB, sectionId)

    const pageDB = await Page.findById(id)
    throwIfPageNotFound(pageDB)

    const update = await Page.findByIdAndUpdate(id, {
        description,
        descriptionLength: description.length
    }, { 'new': true })

    return update.toJSON()
}

const updateOrder = async (me, courseId, sectionId, id, page) => {
    let { order } = page
    throwIf(isNaN(order), badRequest(messages.course.page.error.orderNotFound))
    throwIf(order < 0, badRequest(messages.course.page.error.orderLessThan0))


    let courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfStatusNotDraft(courseDB)
    throwIfSectionNotFound(courseDB, sectionId)

    let pageDB = await Page.findById(id)
    throwIfPageNotFound(pageDB)

    await courseDB.populate({ path: 'sections.pages', select: 'order' }).execPopulate()
    const section = courseDB.sections.find(s => s._id.equals(sectionId))

    const higher = section.pages.length - 1
    if (order > higher) {
        order = higher
    }

    const isGoingDown = pageDB.order < order;
    const pages = sortPages(section.pages, { id, order }, isGoingDown)

    for (let i = 0; i < pages.length; i++) {
        const { _id: id, order } = pages[i]
        await Page.findByIdAndUpdate(id, { order })
    }

    pageDB = await Page.findById(id)
    return pageDB.toJSON()
}

const sortPages = (pages, page, isGoingDown) => {

    let sorted = pages.sort((a, b) => a.order < b.order ? -1 : 1)

    if (isGoingDown) {

        const currentOrder = pages.find(s => s._id.equals(page.id)).order
        const order = page.order

        sorted = pages.map(s => {

            if (s._id.equals(page.id)) {
                s.order = page.order
            } else if (s.order > currentOrder && s.order <= order) {
                s.order = s.order - 1
            }

            return s
        })

    }
    else {
        let order = -1
        sorted = pages.map(s => {

            if (s._id.equals(page.id)) {
                s.order = page.order
            } else if (s.order == page.order) {
                order += 2
                s.order = order
            } else {
                order += 1
                s.order = order
            }

            return s
        })
    }

    return sorted
}

const get = async (me, courseId, sectionId, id) => {
    let courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfSectionNotFound(courseDB, sectionId)

    let pageDB = await Page.findById(id)
    throwIfPageNotFound(pageDB)

    return pageDB.toJSON()
}

const list = async (me, courseId, sectionId, filter) => {
    const courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfSectionNotFound(courseDB, sectionId)

    await courseDB.populate({ path: 'sections.pages', select: 'title order' }).execPopulate()

    let pages = courseDB.sections.find(s => s._id.equals(sectionId)).pages

    if (filter.title) {
        const regex = new RegExp(filter.title, "i")
        pages = pages.filter(p => regex.test(p.title))
    }

    return pages.sort((a, b) => a.order < b.order ? -1 : 1)
}

const remove = async (me, courseId, sectionId, id) => {
    const courseDB = await Course.findById(courseId)

    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfSectionNotFound(courseDB, sectionId)
    throwIfStatusNotDraft(courseDB)
    
    const pageDB = await Page.findById(id)
    throwIfPageNotFound(pageDB)


    await courseDB.populate({ path: 'sections.pages', select: 'order' }).execPopulate()
    const section = courseDB.sections.find(s => s._id.equals(sectionId))

    const isGoingDown = false;
    const pages = sortPages(section.pages, { id, order:Number.POSITIVE_INFINITY }, isGoingDown)


    for (let i = 0; i < pages.length; i++) {
        const { _id: id, order } = pages[i]
        await Page.findByIdAndUpdate(id, { order })
    }

    section.pages = pages.filter( p => !p._id.equals(id) )
    await Course.findOneAndUpdate({ _id: courseId }, { sections:courseDB.sections }, { 'new': true })

    await Page.findByIdAndDelete(id)

    return list(me,courseId,sectionId,{})
}


module.exports = {
    create,
    update,
    updateDescription,
    updateOrder,
    get,
    list,
    remove
}