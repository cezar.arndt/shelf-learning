const { Course, Page } = require('../../schemas')

const { badRequest } = require('../../validator/response')
const { throwIfEmpty, throwIf, isSameValue } = require('../../validator/input')
const { throwIfCourseNotFound, throwIfCourseBelongsToOther, throwIfStatusNotDraft, throwIfSectionNotFound, throwIfSectionTitleExists } = require('../../validator/data')
const messages = require('../../validator/messages')
const { COURSE_STATUS } = require('../../schemas/enum')



const create = async (me, courseId, section) => {
    const { title } = section
    throwIfEmpty(title, messages.course.section.error.titleNotFound)


    let courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfStatusNotDraft(courseDB)
    await throwIfSectionTitleExists(courseDB, title)

    section.order = courseDB.sections.length

    const course = await Course.findByIdAndUpdate(courseId, { $push: { sections: section } }, { 'new': true })
    return course.toJSON().sections.pop()
}

const updateTitle = async (me, courseId, id, section) => {
    const { title } = section
    throwIfEmpty(title, messages.course.section.error.titleNotFound)


    let courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIf( [COURSE_STATUS.DRAFT,COURSE_STATUS.BLOCKED].indexOf(courseDB.status) == -1 , badRequest(messages.course.error.notDraftOrBlocked))
    throwIfSectionNotFound(courseDB, id)

    const sectionDB = courseDB.sections.find(s => s._id.equals(id))
    if (!isSameValue(sectionDB.title, title)) {
        await throwIfSectionTitleExists(courseDB, title)
    }

    const course = await Course.findOneAndUpdate({ _id: courseId, 'sections._id': id }, { 'sections.$.title': title }, { 'new': true })
    return course.toJSON().sections.find(s => s.id.equals(id))
}

const updateOrder = async (me, courseId, id, section) => {
    let { order } = section
    throwIf(isNaN(order), badRequest(messages.course.section.error.orderNotFound))
    throwIf(order < 0, badRequest(messages.course.section.error.orderLessThan0))


    let courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfStatusNotDraft(courseDB)
    throwIfSectionNotFound(courseDB, id)

    const sectionDB = courseDB.sections.find(s => s._id.equals(id))

    const higher = courseDB.sections.length - 1
    if (order > higher) {
        order = higher
    }

    const isGoingDown = sectionDB.order < order
    const sections = sortSections(courseDB.sections, { id, order }, isGoingDown)

    const course = await Course.findOneAndUpdate({ _id: courseId }, { sections }, { 'new': true })
    return course.toJSON().sections.find(s => s.id.equals(id))
}

const sortSections = (sections, section, isGoingDown) => {

    let sorted = sections.sort((a, b) => a.order < b.order ? -1 : 1)

    if (isGoingDown) {
        const currentOrder = sections.find(s => s._id.equals(section.id)).order
        const order = section.order

        sorted = sections.map(s => {

            if (s._id.equals(section.id)) {
                s.order = section.order
            } else if (s.order > currentOrder && s.order <= order) {
                s.order = s.order - 1
            }

            return s
        })

    }
    else {
        let order = -1
        sorted = sections.map(s => {

            if (s._id.equals(section.id)) {
                s.order = section.order
            } else if (s.order == section.order) {
                order += 2
                s.order = order
            } else {
                order += 1
                s.order = order
            }

            return s
        })
    }

    return sorted
}

const get = async (me, courseId, id) => {
    const courseDB = await Course.findById(courseId)
    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfSectionNotFound(courseDB, id)

    const section = courseDB.toJSON().sections.find(s => s.id.equals(id))
    section.pages.sort((a, b) => a.order < b.order ? -1 : 1)

    return section
}

const list = async (me, courseId, filter) => {
    let courseDB = await Course.findById(courseId)

    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)

    const sections = courseDB.toJSON().sections

    if (filter.title) {
        const regexp = new RegExp(filter.title, 'i')
        return sections.filter(s => regexp.test(s.title))
    }

    for (let i = 0, j = sections.length; i < j; i++) {
        const section = sections[i]

        for (let m = 0, n = section.pages.length; m < n; m++) {
            const { id } = section.pages[m]
            section.pages[m] = (await Page.findById(id,'id title order')).toJSON()
        }

        sections[i].pages.sort((a, b) => a.order < b.order ? -1 : 1)
    }

    return sections.sort((a, b) => a.order < b.order ? -1 : 1)
}

const remove = async (me, courseId, id) => {
    const courseDB = await Course.findById(courseId)

    throwIfCourseNotFound(courseDB)
    throwIfCourseBelongsToOther(courseDB, me)
    throwIfStatusNotDraft(courseDB)
    throwIfSectionNotFound(courseDB, id)
    
    let section = courseDB.toJSON().sections.find(s => s.id.equals(id))

    const isGoingDown = false
    let sections = sortSections(courseDB.sections, { id, order:Number.POSITIVE_INFINITY }, isGoingDown)
    await Course.findOneAndUpdate({ _id: courseId }, { sections }, { 'new': true })
    await Course.findByIdAndUpdate(courseId, { $pull: { sections: {_id:id} } }, { 'new': true })
    for( let i = 0; i < section.pages.length; i++ ){
        await Page.findByIdAndDelete(section.pages[i].id)
    }


    return list(me,courseId,{})
}

module.exports = {
    create,
    updateTitle,
    updateOrder,
    get,
    list,
    remove
}