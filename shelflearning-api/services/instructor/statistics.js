const { Course, User, UserCourse } = require("../../schemas");
const { USER_TYPE, COURSE_STATUS } = require("../../schemas/enum");

const { throwIfAdmin } = require("../../validator/data");

const coursePerStatus = async (me) => {
  await throwIfAdmin(me);

  const courses = await Course.find({ createdBy: me }).select("status title");

  const data = courses.reduce((list, course) => {
    const { status } = course;
    if (!(status in list)) {
      list[status] = 0;
    }

    list[status]++;
    return list;
  }, {});

  return data;
};

const userAndStudents = async (me) => {
  await throwIfAdmin(me);

  const allEnabledUsersExceptMeAndAdmins = await User.find({
    type: USER_TYPE.USER,
    isActive: 1,
    disabledAt: null,
    _id: { $ne: me },
  }).select("_id name");

  let courseIds = (
    await Course.find({ createdBy: me }).select("_id")
  ).map((e) => ({ course: e._id }));
  const students = await UserCourse.find({ $or: courseIds })
    .select("_id")
    .distinct("user");

  return {
    users: allEnabledUsersExceptMeAndAdmins.length - students.length,
    students: students.length,
  };
};

const studentsPerCourse = async (me) => {
  await throwIfAdmin(me);

  let courses = await Course.find({
    createdBy: me,
    status: { $ne: COURSE_STATUS.DRAFT },
  }).select("_id title");
  let courseIds = courses.map((e) => ({ course: e._id }));
  const students = await UserCourse.find({ $or: courseIds }).select(
    "_id course"
  );

  const data = courses.reduce((list, course) => {
    if (!(course.title in list)) {
      list[course.title] = 0;
    }

    list[course.title] = students.filter((e) =>
      course._id.equals(e.course)
    ).length;

    return list;
  }, {});

  return Object.keys(data)
    .sort((a, b) => data[b] - data[a])
    .reduce((l, e) => {
      l[e] = data[e];
      return l;
    }, {});
};

const doneRatePerCourse = async (me) => {
  await throwIfAdmin(me);

  let courses = await Course.find({
    createdBy: me,
    status: { $ne: COURSE_STATUS.DRAFT },
  }).select("_id title");
  let courseIds = courses.map((e) => ({ course: e._id }));
  const students = await UserCourse.find({
    $and: [{ $or: courseIds }],
  }).select("_id course progress");

  const data = courses.reduce((list, course) => {
    if (!(course.title in list)) {
      list[course.title] = 0;
    }

    const studentsFiltered = students.filter((e) =>
      course._id.equals(e.course)
    );

    if (studentsFiltered.length === 0) {
      delete list[course.title];
      return list;
    }

    list[course.title] = [
      studentsFiltered.filter((e) => e.progress === 100).length,
      studentsFiltered.length,
    ];

    return list;
  }, {});

  return Object.keys(data)
    .sort((a, b) => data[b][1] - data[a][1])
    .reduce((l, e) => {
      l[e] = data[e];
      return l;
    }, {});
};

const enrollPerMonth = async (me) => {
  await throwIfAdmin(me);

  let courses = await Course.find({
    createdBy: me,
    status: { $ne: COURSE_STATUS.DRAFT },
  }).select("_id title createdAt")

  let courseIds = courses.map((e) => ({ course: e._id }))

  let students = await UserCourse.find({
    $and: [{ $or: courseIds }],
  }).select("_id course createdAt")


  var list = courses.reduce((list, course) => {

    if (!(course.title in list)) {
      var st = students.filter((e) => course._id.equals(e.course) )

      list[course.title] = st.reduce((perMonth, enroll) => {

        var date = new Date(enroll.createdAt)
        date.setHours(0, 0, 0, 0)
        date.setDate(2)

        if (!(date.getTime() in perMonth)) {
          perMonth[date.getTime()] = 1
        } else {
          perMonth[date.getTime()] += 1
        }

        return perMonth;
      }, {})
    }
    return list
  }, {});

  var allMonths = Object.keys(list)
    .map((e) => Object.keys(list[e]).map((ee) => parseInt(ee)))
    .reduce((m, curr) => {
      curr.forEach((e) => {
        if (m.indexOf(e) === -1) {
          m.push(e);
        }
      });

      return m
    }, [])
    .sort((a, b) => a - b);

  const data = Object.keys(list).reduce((data, curr) => {
    data[curr] = {};

    allMonths.forEach((all) => {
      data[curr][all] = 0;
      data[curr][all] = list[curr][all] || 0;
    });
    return data
  }, {})

  return [allMonths, data]
};

module.exports = {
  coursePerStatus,
  userAndStudents,
  studentsPerCourse,
  doneRatePerCourse,
  enrollPerMonth,
};
