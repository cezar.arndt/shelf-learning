
function notExistsTagValidation(tag){
    if (!tag) {
        throw new Error(`Tag não encontrada`)
    }
}

module.exports = {
    notExistsTagValidation
}