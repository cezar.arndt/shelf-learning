
const { User, Tag, Course } = require('../schemas')

const messages = require('../validator/messages')
const { throwIfEmpty, diacriticSensitiveRegex } = require('../validator/input')
const { throwIfNotAdmin, throwIfTagExists, throwIfCourseOnlyHasOneTag, throwIfTagNotFound } = require('../validator/data')
const {  notFound } = require('../validator/response')




const list = async (filter) => {
    let filters = {}
    
    if (filter.name) {
        filters = {
            "name": { '$regex': diacriticSensitiveRegex(filter.name), '$options': 'ig' }
        }
    }


    let lookups = [
        {
            $lookup: { from: 'courses', localField: '_id', foreignField: 'tags', as: 'courses' }
        }
    ]

    let tags = await Tag.aggregate([
        ...lookups,
        {
            $match: filters
        },
        { $addFields: { "coursesCount": { $size: "$courses" } } },
        {
            $project:{
                name:1,
                coursesCount: 1,
                courses: {
                    _id: 1,
                    title:1,
                    status:1
                }
            }
        }
    ])

    tags = tags.map( tag => {
        return {
            id: tag._id,
            name: tag.name,
            coursesCount: tag.coursesCount,
            courses: tag.courses.map( course => {
                return {
                    id:course._id,
                    title:course.title,
                    status:course.status
                }
            } )  
        }
    })

    return tags
}

const get = async (me, id) => {
    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)

    let tagDB
    try {
        tagDB = await Tag.findById(id)
    } catch(e) {
        throw new Error(JSON.stringify(notFound(messages.tag.error.tagNotFound)))
    }

    throwIfTagNotFound(tagDB)

    let courses = await Course.find({ tags:{ _id:id} })

    const tag = tagDB.toJSON()
    tag.coursesCount = courses.length
    tag.courses = courses.map( course => ({id:course._id, title:course.title, status:course.status}) )

    return tag
}

const create = async (me, tag) => {

    const { name } = tag
    throwIfEmpty(name, messages.tag.error.nameNotFound)

    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)

    await throwIfTagExists(name)

    const tagDB = await Tag.create({ name })
    return tagDB.toJSON()
}

const update = async (me, id, tag) => {
    const { name } = tag
    throwIfEmpty(name, messages.tag.error.nameNotFound)

    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)

    let tagDB = await Tag.findById(id)
    throwIfTagNotFound(tagDB)

    await throwIfTagExists(name)

    tagDB = await Tag.findByIdAndUpdate(id, { name }, { 'new': true })
    return tagDB.toJSON()
}

const deleteTag = async (me, id) => {
    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)

    let tagDB = await Tag.findById(id)
    throwIfTagNotFound(tagDB)

    await throwIfCourseOnlyHasOneTag(tagDB)

    await Tag.findByIdAndDelete(id)

    return null
}

const getTagsId = async (tagNames) => {

    const tags = []

    const regex_ = (name) => ({ name: new RegExp(["^", diacriticSensitiveRegex(name), "$"].join(""), "i") })

    for (let i = 0; i < tagNames.length; i++) {
        let tagDB = await Tag.findOne(regex_(tagNames[i]))

        if (!tagDB) {
            tagDB = await Tag.create({ name: tagNames[i] })
            tags.push(tagDB.toJSON())
        } else {
            tags.push(tagDB.toJSON())
        }
    }

    return tags.length ? tags.map(t => t.id) : tags

}

const exportData = async (me, filter) => {
    const meDB = await User.findById(me)
    throwIfNotAdmin(meDB)

    const delimiter = ";"

    const tags = await list(filter)

    const csvHeader = ['Id','Nome','Cursos Utilizando', 'Course', 'Titulo','Status' ]
    const csvBody = tags.map((tag) => [
      `"${tag.id}"`,
      `"${tag.name}"`,
      `"${tag.coursesCount}"`,
      [...tag.courses.map( course => ['\n','','',`"${course.id}"`,`"${course.title}"`,`"${course.status}"`].join(delimiter))].join(''),
      '\n'
    ]);

    return (
        `${csvHeader.join(delimiter)}\n${csvBody.map( line => line.join(delimiter)).join('\n')}`
    )
}


module.exports = {
    list,
    get,
    create,
    update,
    deleteTag,
    getTagsId,
    exportData
}