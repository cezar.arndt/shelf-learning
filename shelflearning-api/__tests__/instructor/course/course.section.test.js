const app = require('../../../index')
const mock = require('../../../.jest/mocks')

const messages = require('../../../validator/messages')
const { courseAPI, sectionAPI, pageAPI } = require('../../../.jest/api')
const { beforeEachSetup } = require('../../../.jest/utils')
const { COURSE_STATUS } = require('../../../schemas/enum')


describe('Sections: Instructor operations', () => {

    //#region setup
    let user1 = null
    let user2 = null

    afterAll(async () => {
        await app.close()
    })

    beforeEach(async () => {
        const options = {
            enabledUsers: 2
        }

        const { enabledUsers } = await beforeEachSetup(options)

        user1 = enabledUsers[0].token
        user2 = enabledUsers[1].token
    })
    //#endregion

    //#region Insert
    test('Insert: Should pass', async () => {
        const {
            section: { section1, section2 },
            course: { complete }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)

        const s1 = await sectionAPI.create(section1)
        const s2 = await sectionAPI.create(section2)

        expect(s1.id).not.toBe(null)
        expect(s1.title).toEqual(section1.title)
        expect(s1.order).toEqual(0)

        expect(s2.id).not.toBe(null)
        expect(s2.title).toEqual(section2.title)
        expect(s2.order).toEqual(1)
    })

    test('Insert no title: Should fail', async () => {
        const {
            course: { complete }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const { error } = await sectionAPI.create({})
        expect(error).toEqual(messages.course.section.error.titleNotFound)
    })

    test('Insert same name: Should fail', async () => {
        const {
            section: { section1 },
            course: { complete }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)

        await sectionAPI.create(section1)
        const { error } = await sectionAPI.create(section1)

        expect(error).toEqual(messages.course.section.error.titleExists)
    })

    test('Insert same name different course: Should pass', async () => {
        const {
            section: { section1 },
            course: { simple1, simple2 }
        } = mock

        courseAPI.setToken(user1)
        const { id: id1 } = await courseAPI.create(simple1)
        const { id: id2 } = await courseAPI.create(simple2)

        sectionAPI.setToken(user1).setCourse(id1)
        const s1 = await sectionAPI.create(section1)

        sectionAPI.setToken(user1).setCourse(id2)
        const s2 = await sectionAPI.create(section1)

        expect(s1.id).not.toBe(null)
        expect(s2.id).not.toBe(null)
    })

    test('Insert different owner: Should fail', async () => {

        const {
            section: { section1 },
            course: { simple1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        sectionAPI.setToken(user2).setCourse(id)
        const { error } = await sectionAPI.create(section1)

        expect(error).toEqual(messages.course.error.courseNotFound)
    })
    //#endregion

    //#region update Title
    test('Update title: Should pass', async () => {
        const {
            section: { section1, section2 },
            course: { simple1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)
        const { title, order } = await sectionAPI.updateTitle(s1.id, section2)

        expect(title).toEqual(section2.title)
        expect(order).toEqual(0)
    })

    test('Update title, section not found: Should fail', async () => {
        const {
            course: { simple1 },
            section: { section1 },
            fakeId
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        await sectionAPI.setToken(user1).setCourse(id)
        const { error } = await sectionAPI.updateTitle(fakeId, section1)

        expect(error).toEqual(messages.course.section.error.sectionNotFound)
    })

    test('Update title, same title: Should fail', async () => {
        const {
            course: { simple1 },
            section: { section1, section2 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        await sectionAPI.setToken(user1).setCourse(id)
        await sectionAPI.create(section1)
        const s2 = await sectionAPI.create(section2)

        const { error } = await sectionAPI.updateTitle(s2.id, section1)
        expect(error).toEqual(messages.course.section.error.titleExists)
    })
    //#endregion

    //#region update order
    test('Update order going up: Should pass', async () => {
        const {
            course: { simple1 },
            section: { section1, section2, section3, section4, section5 },
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        sectionAPI.setToken(user1).setCourse(id)
        await sectionAPI.create(section1)
        await sectionAPI.create(section2)
        await sectionAPI.create(section3)
        await sectionAPI.create(section4)
        await sectionAPI.create(section5)


        let course = await courseAPI.get(id)

        expect(course.sections.find( e=> e.title === 'Section 1').order).toEqual(0)
        expect(course.sections.find( e=> e.title === 'Section 2').order).toEqual(1)
        expect(course.sections.find( e=> e.title === 'Section 3').order).toEqual(2)
        expect(course.sections.find( e=> e.title === 'Section 4').order).toEqual(3)
        expect(course.sections.find( e=> e.title === 'Section 5').order).toEqual(4)


        let lastSection = course.sections.find( e=> e.title === 'Section 5')

        //Changing from 4 to 2
        await sectionAPI.updateOrder(lastSection.id, { order: 2 })
        course = await courseAPI.get(id)

        expect(course.sections.find( e=> e.title === 'Section 1').order).toEqual(0)
        expect(course.sections.find( e=> e.title === 'Section 2').order).toEqual(1)
        expect(course.sections.find( e=> e.title === 'Section 3').order).toEqual(3)
        expect(course.sections.find( e=> e.title === 'Section 4').order).toEqual(4)
        expect(course.sections.find( e=> e.title === 'Section 5').order).toEqual(2)

        //Changing from 2 to 0
        lastSection = course.sections.find( e=> e.title === 'Section 5')

        await sectionAPI.updateOrder(lastSection.id, { order: 0 })
        course = await courseAPI.get(id)

        expect(course.sections.find( e=> e.title === 'Section 1').order).toEqual(1)
        expect(course.sections.find( e=> e.title === 'Section 2').order).toEqual(2)
        expect(course.sections.find( e=> e.title === 'Section 3').order).toEqual(3)
        expect(course.sections.find( e=> e.title === 'Section 4').order).toEqual(4)
        expect(course.sections.find( e=> e.title === 'Section 5').order).toEqual(0)
    })

    test('Update order invalid order: Should fail', async () => {
        const {
            course: { simple1 },
            section: { section1 },
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        const { error } = await sectionAPI.updateOrder(s1.id, { order: -1 })
        expect(error).toEqual(messages.course.section.error.orderLessThan0)
    })

    test('Update order Higher: Should pass', async () => {
        const {
            course: { simple1 },
            section: { section1, section2, section3 },
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        sectionAPI.setToken(user1).setCourse(id)
        await sectionAPI.create(section1)
        await sectionAPI.create(section2)
        await sectionAPI.create(section3)

        course = await courseAPI.get(id)
        expect(course.sections.find( e=> e.title === 'Section 1').order).toEqual(0)
        expect(course.sections.find( e=> e.title === 'Section 2').order).toEqual(1)
        expect(course.sections.find( e=> e.title === 'Section 3').order).toEqual(2)

        let firstSection = course.sections.find( e=> e.title === 'Section 1')

        await sectionAPI.updateOrder(firstSection.id, { order: 5 })
        course = await courseAPI.get(id)

        expect(course.sections.find( e=> e.title === 'Section 1').order).toEqual(2)
        expect(course.sections.find( e=> e.title === 'Section 2').order).toEqual(0)
        expect(course.sections.find( e=> e.title === 'Section 3').order).toEqual(1)
    })

    test('Update order going down: Should pass', async () => {
        const {
            course: { simple1 },
            section: { section1, section2, section3 },
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        sectionAPI.setToken(user1).setCourse(id)
        await sectionAPI.create(section1)
        await sectionAPI.create(section2)
        await sectionAPI.create(section3)

        course = await courseAPI.get(id)
        expect(course.sections.find( e=> e.title === 'Section 1').order).toEqual(0)
        expect(course.sections.find( e=> e.title === 'Section 2').order).toEqual(1)
        expect(course.sections.find( e=> e.title === 'Section 3').order).toEqual(2)


        let secondSection = course.sections.find( e=> e.title === 'Section 2')

        await sectionAPI.updateOrder(secondSection.id, { order: 2 })
        course = await courseAPI.get(id)
        expect(course.sections.find( e=> e.title === 'Section 1').order).toEqual(0)
        expect(course.sections.find( e=> e.title === 'Section 2').order).toEqual(2)
        expect(course.sections.find( e=> e.title === 'Section 3').order).toEqual(1)
    })
    //#endregion


    //#region get
    test('Get: Should pass', async () => {
        const {
            course: { simple1 },
            section: { section1, section2 },
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)
        const s2 = await sectionAPI.create(section2)

        const get1 = await sectionAPI.get(s1.id)
        const get2 = await sectionAPI.get(s2.id)


        expect(get1.title).toEqual(section1.title)
        expect(get2.title).toEqual(section2.title)
    })
    //#endregion

    //#region list
    test('List all: Should pass', async () => {
        const {
            course: { simple1 },
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        sectionAPI.setToken(user1).setCourse(id)
        await sectionAPI.create({ title: 'Course' })
        await sectionAPI.create({ title: 'Section' })
        await sectionAPI.create({ title: 'Session' })
        await sectionAPI.create({ title: 'Tour' })

        const sections = await sectionAPI.list()

        expect(sections.length).toEqual(4)
    })

    test('List with filter: Should pass', async (done) => {
        const {
            course: { simple1 },
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        sectionAPI.setToken(user1).setCourse(id)
        await sectionAPI.create({ title: 'Course' })
        await sectionAPI.create({ title: 'Section' })
        await sectionAPI.create({ title: 'Session' })
        await sectionAPI.create({ title: 'Tour' })


        const result = await Promise.all([
            sectionAPI.list('?title=ION'),
            sectionAPI.list('?title=cou'),
        ])

        expect(result[0].length).toEqual(2)
        expect(result[1].length).toEqual(1)
        done()
    })
    //#endregion


    //#region delete page
    test('Delete: Should pass', async () => {
        const {
            course: { complete },
            section: { section1,section2 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)
        const s2 = await sectionAPI.create(section2)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        pageAPI.setToken(user1).setSection(s2.id).setCourse(id)
        await pageAPI.create(page1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        const { title } = await sectionAPI.get(s1.id)
        expect(title).toEqual(s1.title)

        await sectionAPI.remove(s1.id)
        const { error } = await sectionAPI.get(p1.id)
        expect(error).toEqual(messages.course.section.error.sectionNotFound)
    })

    test('Delete: Changing order, Should pass', async () => {
        const {
            course: { complete },
            section: { section1,section2,section3,section4,section5 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        await sectionAPI.create(section1)
        await sectionAPI.create(section2)
        const s3 = await sectionAPI.create(section3)
        await sectionAPI.create(section4)
        await sectionAPI.create(section5)

        let course = await courseAPI.get(id)
        expect(course.sections.find( e=> e.title === 'Section 1').order).toEqual(0)
        expect(course.sections.find( e=> e.title === 'Section 2').order).toEqual(1)
        expect(course.sections.find( e=> e.title === 'Section 3').order).toEqual(2)
        expect(course.sections.find( e=> e.title === 'Section 4').order).toEqual(3)
        expect(course.sections.find( e=> e.title === 'Section 5').order).toEqual(4)

        pageAPI.setToken(user1).setSection(s3.id).setCourse(id)
        const p1 = await pageAPI.create(page1)

        const { title } = await sectionAPI.get(s3.id)
        expect(title).toEqual(s3.title)

        await sectionAPI.remove(s3.id)
        const { error } = await sectionAPI.get(p1.id)
        expect(error).toEqual(messages.course.section.error.sectionNotFound)

        course = await courseAPI.get(id)
        expect(course.sections.find( e=> e.title === 'Section 1').order).toEqual(0)
        expect(course.sections.find( e=> e.title === 'Section 2').order).toEqual(1)
        expect(course.sections.find( e=> e.title === 'Section 4').order).toEqual(2)
        expect(course.sections.find( e=> e.title === 'Section 5').order).toEqual(3)
    })

    test('Delete published course: Should fail', async (done) => {
        const {
            course: { complete },
            history: { toPublished },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        const { status } = await courseAPI.changeStatus(id, toPublished)
        expect(status).toEqual(COURSE_STATUS.PUBLISHED)

        const { error } = await sectionAPI.remove(s1.id)
        expect(error).toEqual(messages.course.error.notDraft)

        done()
    })
    //#endregion

})