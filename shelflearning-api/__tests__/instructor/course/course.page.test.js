
const app = require('../../../index')
const mock = require('../../../.jest/mocks')
const messages = require('../../../validator/messages')
const { courseAPI, sectionAPI, pageAPI } = require('../../../.jest/api')
const { beforeEachSetup } = require('../../../.jest/utils')
const course = require('../../../services/public/course')
const { COURSE_STATUS } = require('../../../schemas/enum')


describe('Pages: Instructor operations', () => {

    //#region setup
    let user1 = null

    afterAll(async () => {
        await app.close()
    })

    beforeEach(async () => {
        const options = {
            enabledUsers: 2
        }

        const { enabledUsers } = await beforeEachSetup(options)

        user1 = enabledUsers[0].token
    })
    //#endregion

    //#region Insert
    test('Insert: Should pass', async () => {
        const {
            course: { complete },
            section: { section1, section2 },
            page: { page1, page2 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)
        const s2 = await sectionAPI.create(section2)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        const p2 = await pageAPI.create(page2)

        pageAPI.setSection(s2.id)
        const p3 = await pageAPI.create(page1)

        expect(p1.id).not.toBe(null)
        expect(p1.title).toEqual(page1.title)
        expect(p1.order).toEqual(0)

        expect(p2.id).not.toBe(null)
        expect(p2.title).toEqual(page2.title)
        expect(p2.order).toEqual(1)

        expect(p3.id).not.toBe(null)
        expect(p3.title).toEqual(page1.title)
        expect(p3.order).toEqual(0)
    })

    test('Insert Same name: Should fail', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        await pageAPI.create(page1)

        const { error } = await pageAPI.create(page1)
        expect(error).toEqual(messages.course.page.error.titleExists)
    })

    test('Insert no Title: Should fail', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { pageNoTitle }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const { error } = await pageAPI.create(pageNoTitle)

        expect(error).toEqual(messages.course.page.error.titleNotFound)
    })

    //#endregion

    //#region update

    test('Update description: Should pass', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        let page = await pageAPI.create(page1)
        let p1 = await pageAPI.updateDescription(page.id, page1)

        expect(p1.description).toEqual(page1.description)
        expect(p1.order).toEqual(0)
    })

    test('Update with no description: Should fail', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        let page = await pageAPI.create(page1)
        const { error } = await pageAPI.updateDescription(page.id, {})

        expect(error).toEqual(messages.course.page.error.descriptionNotFound)
    })

    test('Update: Should pass', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1, page2 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        const p1 = await pageAPI.create(page1)
        expect(p1.title).toEqual(page1.title)
        expect(p1.order).toEqual(0)

        const p2 = await pageAPI.update(p1.id, page2)
        expect(p2.title).toEqual(page2.title)
        expect(p2.order).toEqual(0)
    })

    test('Update same name: Should fail', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1, page2 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        await pageAPI.create(page1)
        const p2 = await pageAPI.create(page2)

        const { error } = await pageAPI.update(p2.id, page1)
        expect(error).toEqual(messages.course.page.error.titleExists)
    })

    test('Update page not exists: Should fail', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1 },
            fakeId
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)


        const { error } = await pageAPI.update(fakeId, page1)
        expect(error).toEqual(messages.course.page.error.pageNotFound)
    })
    //#endregion


    //#region update order
    test('Update order going up: Should pass', async () => {
        const {
            course: { complete },
            section: { section1, section2 },
            page: { page1, page2, page3, page4, page5 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)
        const s2 = await sectionAPI.create(section2)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        await pageAPI.create(page1)
        await pageAPI.create(page2)
        await pageAPI.create(page3)
        await pageAPI.create(page4)
        await pageAPI.create(page5)


        pageAPI.setSection(s2.id)
        await pageAPI.create(page1)
        await pageAPI.create(page2)
        await pageAPI.create(page3)

        pageAPI.setSection(s1.id)
        let pages = await pageAPI.list()


        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(0)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(1)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(2)
        expect(pages.find( e=> e.title === 'Page 4').order).toEqual(3)
        expect(pages.find( e=> e.title === 'Page 5').order).toEqual(4)

        let lastPage = pages.find( e=> e.title === 'Page 5')

        //Changing from 4 to 2
        await pageAPI.updateOrder(lastPage.id, { order: 2 })
        pages = await pageAPI.list()
        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(0)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(1)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(3)
        expect(pages.find( e=> e.title === 'Page 4').order).toEqual(4)
        expect(pages.find( e=> e.title === 'Page 5').order).toEqual(2)


        lastPage = pages.find( e=> e.title === 'Page 5')
        //Changing from 2 to 0
        await pageAPI.updateOrder(lastPage.id, { order: 0 })
        pages = await pageAPI.list()
        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(1)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(2)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(3)
        expect(pages.find( e=> e.title === 'Page 4').order).toEqual(4)
        expect(pages.find( e=> e.title === 'Page 5').order).toEqual(0)


        pageAPI.setSection(s2.id)
        pages = await pageAPI.list()

        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(0)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(1)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(2)
    })

    test('Update order going down: Should pass', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1, page2, page3 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        await pageAPI.create(page1)
        await pageAPI.create(page2)
        await pageAPI.create(page3)


        let pages = await pageAPI.list()
        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(0)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(1)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(2)

        let firstPage = pages.find( e=> e.title === 'Page 1')

        //Changing from 0 to 2
        await pageAPI.updateOrder(firstPage.id, { order: 2 })
        pages = await pageAPI.list()

        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(2)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(0)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(1)


        let secondPage = pages.find( e=> e.title === 'Page 2')
        //Changing from 0 to 1
        await pageAPI.updateOrder(secondPage.id, { order: 1 })
        pages = await pageAPI.list()
        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(2)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(1)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(0)
    })

    test('Update order invalid order: Should fail', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)

        const { error } = await pageAPI.updateOrder(p1.id, { order: -1 })
        expect(error).toEqual(messages.course.page.error.orderLessThan0)
    })

    test('Update order Higher: Should pass', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1, page2, page3 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        await pageAPI.create(page1)
        await pageAPI.create(page2)
        await pageAPI.create(page3)


        let pages = await pageAPI.list()
        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(0)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(1)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(2)

        let firstPage = pages.find( e=> e.title === 'Page 1')


        await pageAPI.updateOrder(firstPage.id, { order: 5 })
        pages = await pageAPI.list()
        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(2)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(0)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(1)
    })
    //#endregion

    //#region list all
    test('List all: Should pass', async () => {
        const {
            course: { complete },
            section: { section1, section2 },
            page: { page1, page2, page3, page4, page5 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)
        const s2 = await sectionAPI.create(section2)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        await pageAPI.create(page1)
        await pageAPI.create(page2)
        await pageAPI.create(page3)
        await pageAPI.create(page4)
        await pageAPI.create(page5)

        pageAPI.setSection(s2.id)
        await pageAPI.create(page1)
        await pageAPI.create(page2)
        await pageAPI.create(page3)


        pageAPI.setSection(s1.id)
        let pages = await pageAPI.list()
        expect(pages.length).toEqual(5)

        pageAPI.setSection(s2.id)
        pages = await pageAPI.list()
        expect(pages.length).toEqual(3)
    })

    test('List with filter: Should pass', async () => {
        const {
            course: { complete },
            section: { section1, section2 }
        } = mock
        const p = { description: 'Description' }

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)
        const s2 = await sectionAPI.create(section2)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)


        await pageAPI.create({ ...p, title: 'Title' })
        await pageAPI.create({ ...p, title: 'Freckle' })
        await pageAPI.create({ ...p, title: 'Name' })
        await pageAPI.create({ ...p, title: 'Shame' })
        await pageAPI.create({ ...p, title: 'Flame' })

        pageAPI.setSection(s2.id)
        await pageAPI.create({ ...p, title: 'Email' })
        await pageAPI.create({ ...p, title: 'Nail' })
        await pageAPI.create({ ...p, title: 'Last' })


        pageAPI.setSection(s1.id)
        let pages = await pageAPI.list('?title=AME')
        expect(pages.length).toEqual(3)

        pageAPI.setSection(s2.id)
        pages = await pageAPI.list('?title=st')
        expect(pages.length).toEqual(1)
    })
    //#endregion

    //#region 
    test('Get: Should pass', async () => {
        const {
            course: { complete },
            section: { section1, section2 },
            page: { page1, page2 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        await pageAPI.create(page1)
        const p2 = await pageAPI.create(page2)

        const { title } = await pageAPI.get(p2.id)
        expect(title).toEqual(page2.title)
    })
    //#endregion

    //#region delete page
    test('Delete: Should pass', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        const p1 = await pageAPI.create(page1)

        const { title } = await pageAPI.get(p1.id)
        expect(title).toEqual(page1.title)

        await pageAPI.remove(p1.id)
        const { error } = await pageAPI.get(p1.id)
        expect(error).toEqual(messages.course.page.error.pageNotFound)
    })


    test('Delete: Changing order, Should pass', async () => {
        const {
            course: { complete },
            section: { section1 },
            page: { page1, page2, page3, page4, page5 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        let s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        await pageAPI.create(page1)
        const p2 = await pageAPI.create(page2)
        await pageAPI.create(page3)
        await pageAPI.create(page4)
        await pageAPI.create(page5)

        let pages = await pageAPI.list()
        expect(pages.length).toEqual(5)
        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(0)
        expect(pages.find( e=> e.title === 'Page 2').order).toEqual(1)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(2)
        expect(pages.find( e=> e.title === 'Page 4').order).toEqual(3)
        expect(pages.find( e=> e.title === 'Page 5').order).toEqual(4)

        await pageAPI.remove(p2.id)
        const { error } = await pageAPI.get(p2.id)
        expect(error).toEqual(messages.course.page.error.pageNotFound)

        pages = await pageAPI.list()
        expect(pages.length).toEqual(4)
        expect(pages.find( e=> e.title === 'Page 1').order).toEqual(0)
        expect(pages.find( e=> e.title === 'Page 3').order).toEqual(1)
        expect(pages.find( e=> e.title === 'Page 4').order).toEqual(2)
        expect(pages.find( e=> e.title === 'Page 5').order).toEqual(3)

        s1 = await sectionAPI.get(s1.id)
        expect(s1.pages.length).toEqual(4)

    })

    test('Delete published course: Should fail', async (done) => {
        const {
            course: { complete },
            history: { toPublished },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        const { status } = await courseAPI.changeStatus(id, toPublished)
        expect(status).toEqual(COURSE_STATUS.PUBLISHED)

        const { error } = await pageAPI.remove(p1.id)
        expect(error).toEqual(messages.course.error.notDraft)

        done()
    })
    //#endregion

})