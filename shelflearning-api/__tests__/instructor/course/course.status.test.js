const app = require('../../../index')
const mock = require('../../../.jest/mocks')

const { COURSE_STATUS } = require('../../../schemas/enum')
const messages = require('../../../validator/messages')
const { courseAPI } = require('../../../.jest/api')
const { beforeEachSetup } = require('../../../.jest/utils')
const sectionAPI = require('../../../.jest/api/sectionAPI')
const pageAPI = require('../../../.jest/api/pageAPI')



describe('Change Status: Instructor operations', () => {

    //#region setup
    let user1 = null
    let admin = null

    afterAll(async () => {
        await app.close()
    })

    beforeEach(async () => {
        const options = {
            enabledUsers: 1,
            admins: 1
        }

        const { enabledUsers, admins } = await beforeEachSetup(options)

        user1 = enabledUsers[0].token
        admin = admins[0].token
    })
    //#endregion

    //#region DRAFT
    test('Status, Draft to Publishied: Should pass', async () => {
        const {
            course: { complete },
            history: { toPublished },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        const { status } = await courseAPI.changeStatus(id, toPublished)
        expect(status).toEqual(COURSE_STATUS.PUBLISHED)
    })

    test('Status, Draft to Publishied with No sections: Should fail', async () => {
        const {
            course: { complete },
            history: { toPublished }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        const { error } = await courseAPI.changeStatus(id, toPublished)
        expect(error).toEqual(messages.course.error.noSections)
    })

    test('Status, Draft to Publishied with No pages: Should fail', async () => {
        const {
            course: { complete },
            section: { section1 },
            history: { toPublished }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        const { error } = await courseAPI.changeStatus(id, toPublished)
        const e = {...messages.course.error.noPages}
        e.message = e.message.replace(':sectionId',s1.title)
        expect(error).toEqual(e)
    })


    test('Status, Draft to Publishied with page empty: Should fail', async () => {
        const {
            course: { complete },
            history: { toPublished },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)

        const { error } = await courseAPI.changeStatus(id, toPublished)
        const e = {...messages.course.error.emptyPage}
        e.message = e.message.replace(':pageId',p1.title)
        expect(error).toEqual(e)
    })

    test('Status, Darft to others: Should fail', async () => {
        const {
            course: { complete },
            history: { toClosed, toBlocked }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)


        const status = await Promise.all([
            courseAPI.changeStatus(id, toClosed),
            courseAPI.changeStatus(id, toBlocked)
        ])

        expect(status[0].error).toEqual(messages.course.error.history.invalidChange)
        expect(status[1].error).toEqual(messages.course.error.history.invalidChange)
    })

    test('Status[ADMIN], Draft to Publishied: Should fail', async () => {
        const {
            course: { complete },
            history: { toPublished }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        courseAPI.setToken(admin)
        const { error } = await courseAPI.changeStatus(id, toPublished)
        expect(error).toEqual(messages.course.error.history.onlyOwnerPublish)
    })
    //#endregion

    //#region PUBLISHED
    test('Status[ADMIN], Publishied to Block: Should pass', async () => {
        const {
            course: { complete },
            history: { toPublished, toBlocked },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.changeStatus(id, toPublished)

        courseAPI.setToken(admin)
        const { status } = await courseAPI.changeStatus(id, toBlocked)
        expect(status).toEqual(COURSE_STATUS.BLOCKED)
    })

    test('Status, Publishied to Closed: Should pass', async () => {
        const {
            course: { complete },
            history: { toPublished, toClosed },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)

        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.changeStatus(id, toPublished)

        const { status } = await courseAPI.changeStatus(id, toClosed)
        expect(status).toEqual(COURSE_STATUS.CLOSED)
    })

    test('Status[ADMIN], Publishied to Closed: Should fail', async () => {
        const {
            course: { complete },
            history: { toPublished, toClosed },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.changeStatus(id, toPublished)

        courseAPI.setToken(admin)
        const { error } = await courseAPI.changeStatus(id, toClosed)
        expect(error).toEqual(messages.course.error.history.onlyOwnerClose)
    })

    test('Status[USER], Publishied to blocked: Should fail', async () => {
        const {
            course: { complete },
            history: { toPublished, toBlocked },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.changeStatus(id, toPublished)


        const { error } = await courseAPI.changeStatus(id, toBlocked)
        expect(error).toEqual(messages.course.error.history.onlyAdminBlock)
    })

    test('Status, Publishied to others: Should fail', async () => {
        const {
            course: { complete },
            history: { toPublished, toDraft },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.changeStatus(id, toPublished)

        const { error } = await courseAPI.changeStatus(id, toDraft)
        expect(error).toEqual(messages.course.error.history.invalidChange)
    })
    //#endregion

    //#region CLOSED
    test('Status, Closed to others: Should fail', async () => {
        const {
            course: { complete },
            history: { toPublished, toClosed, toDraft, toBlocked },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.changeStatus(id, toPublished)
        await courseAPI.changeStatus(id, toClosed)

        const status = await Promise.all([
            courseAPI.changeStatus(id, toDraft),
            courseAPI.changeStatus(id, toPublished)
        ])

        expect(status[0].error).toEqual(messages.course.error.history.invalidChange)
        expect(status[1].error).toEqual(messages.course.error.history.invalidChange)

        courseAPI.setToken(admin)
        const { error } = await courseAPI.changeStatus(id, toBlocked)
        expect(error).toEqual(messages.course.error.history.invalidChange)
    })
    //#endregion

    //#region BLOCKED
    test('Status, Blocked to Published: Should pass', async () => {
        const {
            course: { complete },
            history: { toPublished, toBlocked },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.changeStatus(id, toPublished)

        courseAPI.setToken(admin)
        await courseAPI.changeStatus(id, toBlocked)
        const { status } = await courseAPI.changeStatus(id, toPublished)
        expect(status).toEqual(COURSE_STATUS.PUBLISHED)
    })

    test('Status[USER], Blocked to Published: Should fail', async () => {
        const {
            course: { complete },
            history: { toPublished, toBlocked },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.changeStatus(id, toPublished)

        courseAPI.setToken(admin)
        await courseAPI.changeStatus(id, toBlocked)

        courseAPI.setToken(user1)
        const { error } = await courseAPI.changeStatus(id, toPublished)
        expect(error).toEqual(messages.course.error.history.onlyAdminUnblock)
    })

    test('Status, Blocked to others: Should fail', async (done) => {
        const {
            course: { complete },
            history: { toPublished, toBlocked, toDraft, toClosed },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.changeStatus(id, toPublished)

        courseAPI.setToken(admin)
        await courseAPI.changeStatus(id, toBlocked)

        const errors = await Promise.all([
            courseAPI.changeStatus(id, toDraft),
            courseAPI.changeStatus(id, toClosed)
        ])

        expect(errors[0].error).toEqual(messages.course.error.history.invalidChange)
        expect(errors[1].error).toEqual(messages.course.error.history.invalidChange)

        done()
    })
    //#endregion
})