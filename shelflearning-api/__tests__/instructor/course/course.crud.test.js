const app = require('../../../index')
const mock = require('../../../.jest/mocks')

const { COURSE_STATUS } = require('../../../schemas/enum')
const messages = require('../../../validator/messages')
const { courseAPI, sectionAPI, pageAPI } = require('../../../.jest/api')
const { beforeEachSetup } = require('../../../.jest/utils')



describe('Course: Instructor operations', () => {

    //#region setup
    let user1 = null
    let idUser1 = null
    let user2 = null

    afterAll(async () => {
        await app.close()
    })

    beforeEach(async () => {
        const options = {
            enabledUsers: 2
        }

        const { enabledUsers } = await beforeEachSetup(options)

        user1 = enabledUsers[0].token
        idUser1 = enabledUsers[0].id
        user2 = enabledUsers[1].token
    })
    //#endregion

    //#region Insert
    test('Insert: Should pass', async () => {
        const { complete } = mock.course

        courseAPI.setToken(user1)
        const { id, title, description, createdBy, status, tags } = await courseAPI.create(complete)

        expect(id).not.toBe(null)
        expect(title).toEqual(complete.title)
        expect(description).toEqual(complete.description)
        expect(createdBy.id).toEqual(idUser1)
        expect(status).toEqual(COURSE_STATUS.DRAFT)
        expect(tags.length).toEqual(4)
    })

    test('Insert Empty: Should fail', async () => {
        courseAPI.setToken(user1)

        const { error } = await courseAPI.create({})
        expect(error).toEqual(messages.course.error.titleNotFound)
    })

    test('Insert No tags: Should fail', async () => {
        const { noTags } = mock.course

        courseAPI.setToken(user1)
        const { error } = await courseAPI.create(noTags)

        expect(error).toEqual(messages.course.error.tagNotFound)
    })

    test('Insert Same name: Should fail', async () => {
        const { simple1 } = mock.course
        const simple1Copy = { ...simple1, title: simple1.title.toUpperCase() }

        courseAPI.setToken(user1)
        await courseAPI.create(simple1)
        const { error } = await courseAPI.create(simple1Copy)

        expect(error).toEqual(messages.course.error.titleExists)
    })
    //#endregion

    //#region update
    test('Update: Should pass', async () => {
        const { simple1 } = mock.course

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        const update = { ...simple1 }
        update.title += ' updated'
        update.tags = ['New tag']
        update.description = 'New description'

        const { title, description, tags } = await courseAPI.update(id, update)

        expect(title).toEqual(update.title)
        expect(description).toEqual(update.description)
        expect(tags.length).toEqual(1)
    })

    test('Update change title same other: Should fail', async () => {
        const { simple1, simple2 } = mock.course

        courseAPI.setToken(user1)
        await courseAPI.create(simple1)
        const { id } = await courseAPI.create(simple2)

        const { error } = await courseAPI.update(id, simple1)
        expect(error).toEqual(messages.course.error.titleExists)
    })

    test('Update not owner: Should fail', async () => {
        const { simple1, simple2 } = mock.course

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        courseAPI.setToken(user2)
        const { error } = await courseAPI.update(id, simple2)

        expect(error).toEqual(messages.course.error.courseNotFound)
    })

    test('Update fakeId: Should fail', async () => {
        const { fakeId, course: { simple1 } } = mock

        courseAPI.setToken(user1)

        const { error } = await courseAPI.update(fakeId, simple1)
        expect(error).toEqual(messages.course.error.courseNotFound)
    })
    //#endregion

    //#region get

    test('Get: Should pass', async () => {
        const { simple1 } = mock.course

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        const { title } = await courseAPI.get(id)
        expect(title).toEqual(simple1.title)
    })

    test('Get Info: Should pass', async () => {
        const { simple1 } = mock.course

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        const { title } = await courseAPI.getInfo(id)
        
        expect(title).toEqual(simple1.title)
    })

    //#endregion

    //#region find
    test('list all: Should pass', async () => {
        const { simple1, simple2, complete } = mock.course

        courseAPI.setToken(user1)

        await courseAPI.create(simple1)
        await courseAPI.create(simple2)

        courseAPI.setToken(user2)
        await courseAPI.create(complete)

        courseAPI.setToken(user1)
        const courses = await courseAPI.list()
        expect(courses.length).toEqual(2)
    })

    test('list with title filter: Should pass', async () => {
        const { simple1, simple2, simple3, complete } = mock.course

        courseAPI.setToken(user1)

        await courseAPI.create(simple1)
        await courseAPI.create(simple2)
        await courseAPI.create(simple3)

        courseAPI.setToken(user2)
        await courseAPI.create(complete)

        courseAPI.setToken(user1)
        const result = await Promise.all([
            courseAPI.list('?filter=another'),
            courseAPI.list('?filter=SIMPLE'),
            courseAPI.list('?filter=CoursE')
        ])

        expect(result[0].length).toEqual(2)
        expect(result[1].length).toEqual(1)
        expect(result[2].length).toEqual(3)
    })

    //#endregion

    //#region remove

    test('Remove: Should pass', async () => {
        const { simple1 } = mock.course

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(simple1)

        const { title } = await courseAPI.get(id)
        expect(title).toEqual(simple1.title)

        await courseAPI.remove(id)

        const { error } = await courseAPI.get(id)
        expect(error).toEqual(messages.course.error.courseNotFound)
    })

    test('Remove: Published Should fail', async () => {
        const {
            course: { complete },
            history: { toPublished },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        const { status } = await courseAPI.changeStatus(id, toPublished)
        expect(status).toEqual(COURSE_STATUS.PUBLISHED)

        const {error} = await courseAPI.remove(id)
        expect(error).toEqual(messages.course.error.notDraft)
    })


    test('Remove: Remove cascade Should pass', async () => {
        const {
            course: { complete },
            history: { toPublished },
            section: { section1 },
            page: { page1 }
        } = mock

        courseAPI.setToken(user1)
        const { id } = await courseAPI.create(complete)

        sectionAPI.setToken(user1).setCourse(id)
        const s1 = await sectionAPI.create(section1)

        pageAPI.setToken(user1).setSection(s1.id).setCourse(id)
        const p1 = await pageAPI.create(page1)
        await pageAPI.updateDescription(p1.id, { description: 'Description' })

        await courseAPI.remove(id)

        const { error } = await courseAPI.get(id)
        expect(error).toEqual(messages.course.error.courseNotFound)
    })

    //#endregion

})