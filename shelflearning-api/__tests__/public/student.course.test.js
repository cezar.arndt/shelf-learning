const app = require('../../index')

const { studentAPI, courseAPI } = require('../../.jest/api')
const { beforeEachSetup } = require('../../.jest/utils')
const { bootstrap } = require('../../.jest/bootstrap')
const { COURSE_STATUS } = require('../../schemas/enum')
const messages = require('../../validator/messages')
const mock = require('../../.jest/mocks')

describe('Course: Public operations', () => {

    //#region setup
    let professor = null
    let student = null
    let student2 = null
    let admin = null

    afterAll(async () => {
        await app.close()
    })

    beforeEach(async () => {
        const options = {
            enabledUsers: 3,
            admins:1
        }

        const { enabledUsers, admins } = await beforeEachSetup(options)

        professor = enabledUsers[0].token
        student = enabledUsers[1].token
        student2 = enabledUsers[2].token
        admin = admins[0].token

        await bootstrap(professor)
    })

    //#endregion


    //#region list
    test('list all: Should pass', async () => {
        studentAPI.setToken(student)
        const courses = await studentAPI.list()
        expect(courses.length).toEqual(3)
    })

    test('list with title filter: Should pass', async () => {
        studentAPI.setToken(student)

        const result = await Promise.all([
            studentAPI.list('?title=Basico'),
            studentAPI.list('?title=python'),
            studentAPI.list('?title=publicado'),
        ])

        expect(result[0].length).toEqual(2)
        expect(result[1].length).toEqual(1)
        expect(result[2].length).toEqual(0)
    })

    test('list with tag filter: Should pass', async () => {
        studentAPI.setToken(student)

        const result = await Promise.all([
            studentAPI.list('?tags=desenvolviment web'),
            studentAPI.list('?tags=javascript'),
            studentAPI.list('?tags=html, javascript'),
            studentAPI.list('?tags=flask')
        ])

        expect(result[0].length).toEqual(3)
        expect(result[1].length).toEqual(2)
        expect(result[2].length).toEqual(1)
        expect(result[3].length).toEqual(1)
    })

    test('list with tags and title: Should pass', async () => {
        studentAPI.setToken(student)

        const result = await Promise.all([
            studentAPI.list('?title=basico&tags=html, javascript'),
            studentAPI.list('?title=python&tags=flask')
        ])

        expect(result[0].length).toEqual(1)
        expect(result[1].length).toEqual(1)
    })
    //#endregion


    //#region get
    test('Get course: Should pass', async () => {
        studentAPI.setToken(student)

        const [{ id }] = await studentAPI.list('?title=basico&tags=html, javascript')
        const { title } = await studentAPI.get(id)
        expect(title).toEqual('HTML basico')

    })

    test('Get course draft: Should fail', async () => {
        studentAPI.setToken(student)
        courseAPI.setToken(professor)

        const courses = await courseAPI.list()
        const { id } = courses.find(c => c.status === COURSE_STATUS.DRAFT)

        const { error } = await studentAPI.get(id)
        expect(error).toEqual(messages.course.error.courseNotFound)
    })

    test('Get course fakeId: Should fail', async () => {
        const { fakeId } = mock

        studentAPI.setToken(student)
        const { error } = await studentAPI.get(fakeId)
        expect(error).toEqual(messages.course.error.courseNotFound)
    })
    //#endregion


    //#region enroll

    test('enroll course: Should pass', async () => {
        studentAPI.setToken(student)

        const [{ id }] = await studentAPI.list('?filter=html')
        await studentAPI.enroll(id)

        studentAPI.setToken(student2)
        await studentAPI.enroll(id)

        studentAPI.setToken(student)
        const myCourses = await studentAPI.enrollment()

        expect(myCourses.length).toEqual(1)
        expect(myCourses[0].title).toEqual('HTML basico')
    })

    test('enroll course instructor: Should fail', async () => {
        studentAPI.setToken(professor)

        const [{ id }] = await studentAPI.list('?title=basico&tags=html, javascript')
        const { error } = await studentAPI.enroll(id)

        expect(error).toEqual(messages.course.error.isMycourse)
    })

    test('enroll course twice: Should fail', async () => {
        studentAPI.setToken(student)

        const [{ id }] = await studentAPI.list('?title=basico&tags=html, javascript')
        await studentAPI.enroll(id)
        const { error } = await studentAPI.enroll(id)
        expect(error).toEqual(messages.course.error.alreadyEnrolled)
    })

    test('enroll course draft: Should fail', async () => {
        studentAPI.setToken(student)
        courseAPI.setToken(professor)

        const courses = await courseAPI.list()
        const { id } = courses.find(c => c.status === COURSE_STATUS.DRAFT)

        const { error } = await studentAPI.enroll(id)
        expect(error).toEqual(messages.course.error.notPublished)
    })


    test('enroll course: width admin Should fail', async () => {
        studentAPI.setToken(admin)

        const [{ id }] = await studentAPI.list('?filter=html')
        const { error } = await studentAPI.enroll(id)

        expect(error).toEqual(messages.course.error.adminCantEnroll)
    })

    test('My courses: width admin Should fail', async () => {
        studentAPI.setToken(admin)
        const { error } = await studentAPI.enrollment()
        expect(error).toEqual(messages.course.error.adminCant)
    })

    //#endregion

    //#region update progress

    test('update progress: Should pass', async () => {
        studentAPI.setToken(student)

        const [{ id }] = await studentAPI.list('?title=javascript')
        await studentAPI.enroll(id)

        const course = await studentAPI.get(id)

        const progress = {
            currentPage: course.sections[0].pages[0].id,
        }

        const { progress: p, currentPage, lastAccess } = await studentAPI.updateProgress(id, progress)
        expect(p).toEqual(0)
        expect(currentPage).toEqual(course.sections[0].pages[0].id)
        expect(lastAccess).not.toBe(null)
    })

    test('Enrollment info: Should pass', async () => {
        studentAPI.setToken(student)

        const [{ id }] = await studentAPI.list('?title=javascript')
        await studentAPI.enroll(id)

        const course = await studentAPI.get(id)

        const progress = {
            currentPage: course.sections[0].pages[0].id,
        }

        await studentAPI.updateProgress(id, progress)
        
        const [{ progress: p, currentPage, lastAccess }] = await studentAPI.enrollmentInfo()
        expect(p).toEqual(0)
        expect(currentPage).toEqual(course.sections[0].pages[0].id)
        expect(lastAccess).not.toBe(null)
    })


    test('update progress: admin user Should fail', async () => {
        studentAPI.setToken(admin)

        const [{ id }] = await studentAPI.list('?title=javascript')

        const course = await studentAPI.get(id)

        const progress = {
            currentPage: course.sections[0].pages[0].id,
        }

        const { error } = await studentAPI.updateProgress(id, progress)
        expect(error).toEqual(messages.course.error.adminCantProgress)
    })


    test('Enrollment info: user admin Should fail', async () => {
        studentAPI.setToken(admin)

        const {error} = await studentAPI.enrollmentInfo()
        expect(error).toEqual(messages.course.error.adminCant)
    })

    //#endregion

    test('update progress endOfPage: Should pass', async (done) => {
        studentAPI.setToken(student)

        const [{ id }] = await studentAPI.list('?title=javascript')
        await studentAPI.enroll(id)

        const course = await studentAPI.get(id)

        let progress = {
            currentPage: course.sections[0].pages[0].id,
            isEndPage: true
        }

        let userProgress = await studentAPI.updateProgress(id, progress)
        expect(userProgress.progress).toEqual(11)
        expect(userProgress.pagesDone.length).toEqual(1)
        expect(userProgress.sectionsDone.length).toEqual(0)
        expect(userProgress.currentPage).toEqual(course.sections[0].pages[1].id)



        progress = {
            currentPage: course.sections[0].pages[1].id,
            isEndPage: true
        }

        userProgress = await studentAPI.updateProgress(id, progress)
        expect(userProgress.progress).toEqual(22)
        expect(userProgress.pagesDone.length).toEqual(2)
        expect(userProgress.sectionsDone.length).toEqual(0)
        expect(userProgress.currentPage).toEqual(course.sections[0].pages[2].id)



        progress = {
            currentPage: course.sections[0].pages[2].id,
            isEndPage: true
        }

        userProgress = await studentAPI.updateProgress(id, progress)
        expect(userProgress.progress).toEqual(33)
        expect(userProgress.pagesDone.length).toEqual(3)
        expect(userProgress.sectionsDone.length).toEqual(1)
        expect(userProgress.currentPage).toEqual(course.sections[1].pages[0].id)

        done()
    })

})