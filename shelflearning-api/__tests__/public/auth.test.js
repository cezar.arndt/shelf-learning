const app = require('../../index')

const mongoose = require('mongoose')
const { MailSlurp } = require("mailslurp-client")
const bcrypt = require("bcryptjs")

const { authAPI, userAPI } = require('../../.jest/api')
const { beforeEachSetup } = require('../../.jest/utils')
const messages = require('../../validator/messages')
const { User } = require('../../schemas')
const { USER_TYPE } = require('../../schemas/enum')


describe('Auth', () => {

    let admin = null
    let apiMail = null
    let inbox = null

    //#region setup
    afterAll(async () => {
        await app.close()
    })

    beforeAll( async () => {
        apiMail = new MailSlurp({ apiKey: process.env.MAIL_SLURP})
        inbox = {
            emailAddress: process.env.MAIL_SLURP_EMAIL,
            id: process.env.MAIL_SLURP_INBOX_ID
        }
    }) 

    beforeEach(async () => {
        const options = {
            enabledUsers: 2,
            disabledUsers:1,
            admins:1
        }

        const { admins } = await beforeEachSetup(options)

        admin = admins[0].token
    })

    //#endregion


    //#region auth
    test('auth enable user: Should pass', async () => {
        const user = {
            username: 'user.0',
            password: '123456'
        };

        const { token } = await authAPI.login(user)
        expect(token).not.toBe(null)
    })

    test('auth fake user: Should fail', async () => {
        const user = {
            username: 'fakeuser',
            password: '123456'
        };

        const { error } = await authAPI.login(user)
        expect(error).toEqual(messages.auth.error.credentialsWrong)
    })

    test('auth wrong password: Should fail', async () => {
        const user = {
            username: 'user.0',
            password: 'wrongpassword'
        };

        const { error } = await authAPI.login(user)
        expect(error).toEqual(messages.auth.error.credentialsWrong)
    })

    test('auth null authentication: Should fail', async () => {
        const { error } = await authAPI.login({})
        expect(error).toEqual(messages.auth.error.credentialsNotFound)
    })

    test('auth disabled user: Should fail', async () => {
        const user = {
            username: 'user.disabled.0',
            password: '123456'
        };

        const { error } = await authAPI.login(user)
        expect(error).toEqual(messages.auth.error.userDisabled)
    })
    //#endregion

    test('create account: Should pass', async () => {
        const account = {
            name: 'New User',
            username: inbox.emailAddress,
            password : 'Password123;',
            confirm: 'Password123;'
        };

        await authAPI.createAccount(account)

        userAPI.setToken(admin)
        const [user] = await userAPI.list('?name='+account.username)

        expect(user.username).toEqual(account.username)
        expect(user.isActive).toEqual(false)
    })

    test('create account validations: should fail', async () => {
        let account = {};
        let res = await authAPI.createAccount(account)
        expect(res.error).toEqual(messages.user.error.nameNotFound)

        account.name = 'New User'
        res = await authAPI.createAccount(account)
        expect(res.error).toEqual(messages.user.error.userNameNotFound)

        account.name = 'New User'
        account.username = inbox.emailAddress
        res = await authAPI.createAccount(account)
        expect(res.error).toEqual(messages.user.error.passwordNotFound)
        
        account.password = '123'
        account.username = 'wrongemail',
        res = await authAPI.createAccount(account)
        expect(res.error).toEqual(messages.auth.error.emailRule)
        account.username = inbox.emailAddress,

        account.password = '123'
        account.confirm = '456'
        res = await authAPI.createAccount(account)
        expect(res.error).toEqual(messages.auth.error.passwordsDoesntMatch)

        account.password = '123'
        account.confirm = '123'
        res = await authAPI.createAccount(account)
        expect(res.error).toEqual(messages.auth.error.passwordRule)
    })

    test('create account Same username: shoud fail', async () => {
        await mongoose.connect(process.env.MONGO_URL)
        await User.create({
            name:'New User',
            username:inbox.emailAddress,
            password: 'Password123',
            type: USER_TYPE.USER,
            disabledAt: null,
            isActive: false,
        })


        const account = {
            name: 'New User',
            username: inbox.emailAddress,
            password : 'Password123;',
            confirm: 'Password123;'
        };
        const {error} = await authAPI.createAccount(account)
        
        expect(error).toEqual(messages.user.error.userNameExists)
    })

    test('activate account: Should pass', async () => {
        await mongoose.connect(process.env.MONGO_URL)
        const salt = bcrypt.genSaltSync(10)
        const validationToken = bcrypt.hashSync('TOKEN', salt)
        const now = new Date()
        now.setDate( now.getDate() + 1)

        await User.create({
            name:'New User',
            username:inbox.emailAddress,
            password: 'Password123',
            type: USER_TYPE.USER,
            disabledAt: null,
            isActive: false,
            tokenValidationTime:now,
            validationToken
        })

        await authAPI.activateAccount('TOKEN')

        userAPI.setToken(admin)
        const [user] = await userAPI.list('?name='+inbox.emailAddress)

        expect(user.username).toEqual(inbox.emailAddress)
        expect(user.isActive).toEqual(true)
    })

    test('activate account validations: Should fail', async () => {
        
        let res =  await authAPI.activateAccount()
        expect(res.error).toEqual(messages.auth.error.validationTokenNotFound)


        res =  await authAPI.activateAccount('fakeCode')
        expect(res.error).toEqual(messages.auth.error.invalidToken)

        
        await mongoose.connect(process.env.MONGO_URL)
        const salt = bcrypt.genSaltSync(10)
        const validationToken = bcrypt.hashSync('TOKEN', salt)

        //make validationToneInvalid by changins creation date to past
        const yesterday = new Date()
        yesterday.setDate( yesterday.getDate() - 1)

        await User.create({
            name:'New User',
            username:inbox.emailAddress,
            password: 'Password123',
            type: USER_TYPE.USER,
            disabledAt: null,
            isActive: false,
            tokenValidationTime:yesterday,
            validationToken
        })

        res =  await authAPI.activateAccount('TOKEN')
        expect(res.error).toEqual(messages.auth.error.expiredToken)
    })

    test('reset Activation Code: Should pass', async () => {
        await mongoose.connect(process.env.MONGO_URL)
        await User.create({
            name:'New User',
            username:inbox.emailAddress,
            password: 'Password123',
            type: USER_TYPE.USER,
            disabledAt: null,
            isActive: false,
        })

        await authAPI.resetActivationCode({username:inbox.emailAddress})
        
        const emails = await apiMail.getEmails(inbox.id, { minCount: 1 });
        const latestEmail = await apiMail.getEmail(emails[0].id);
        const verificationCode = latestEmail.body.match(/<b>(.*)<\/b>/g)[2].replace(/<b>|<\/b>/g,'')

        await authAPI.activateAccount(verificationCode)
    })

    test('reset Activation Code, Account already activated: Should fail', async () => {

        await mongoose.connect(process.env.MONGO_URL)
        await User.create({
            name:'New User',
            username:inbox.emailAddress,
            password: 'Password123',
            type: USER_TYPE.USER,
            disabledAt: null,
            isActive: true,
        })

        const {error} =  await authAPI.resetActivationCode({username:inbox.emailAddress})
        expect(error).toEqual(messages.auth.error.userAlreadyActive)
    })

    test('reset Password: Should pass', async () => {
        await new Promise( r => setTimeout(r,60000))

        await mongoose.connect(process.env.MONGO_URL)
        await User.create({
            name:'New User',
            username:inbox.emailAddress,
            password: 'Password123',
            type: USER_TYPE.USER,
            disabledAt: null,
            isActive: true,
        })

        await authAPI.resetPasswordCode({username:inbox.emailAddress})

        // Not being possible to execute this steps below with other test (only working by single run)
        // const emails = await apiMail.getEmails(inbox.id, { minCount: 1 });
        // const latestEmail = await apiMail.getEmail(emails[0].id);
        // const verificationCode = latestEmail.body.match(/code=(.*)/)[1].match(/(.*)\">(.*)\/\//)[1]

        // await authAPI.resetPassword({
        //     password : 'NewPassword123;',
        //     confirm: 'NewPassword123;'
        // }, verificationCode)
        
    })

    test('reset Password Code Inactive account: Should fail', async () => {
        await mongoose.connect(process.env.MONGO_URL)
        await User.create({
            name:'New User',
            username:inbox.emailAddress,
            password: 'Password123',
            type: USER_TYPE.USER,
            disabledAt: null,
            isActive: false,
        })

        const {error} = await authAPI.resetPasswordCode({username:inbox.emailAddress})
        expect(error).toEqual(messages.auth.error.userNotActive)
    })
    
})