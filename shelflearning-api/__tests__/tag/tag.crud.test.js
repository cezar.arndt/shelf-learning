const app = require('../../index')
const { tagAPI, courseAPI } = require('../../.jest/api')
const { beforeEachSetup } = require('../../.jest/utils')
const mock = require('../../.jest/mocks')
const messages = require('../../validator/messages')
const tag = require('../../services/tag')



describe('Tag: Crud operations', () => {

    //#region setup
    let notAdmin = null
    let admin = null

    afterAll(async () => {
        await app.close()
    })

    beforeEach(async () => {
        const options = {
            enabledUsers: 1,
            admins: 1
        }

        const { enabledUsers, admins } = await beforeEachSetup(options)

        notAdmin = enabledUsers[0].token
        admin = admins[0].token
    })
    //#endregion

    //#region Insert
    test('Create: Should pass', async () => {

        const { tag } = mock

        tagAPI.setToken(admin)

        const { id, name } = await tagAPI.create(tag.javascript)

        expect(id).not.toBe(null)
        expect(name).toEqual(tag.javascript.name)
    })

    test('Create: empty tag Should fail', async () => {

        const { tag } = mock

        tagAPI.setToken(admin)

        const { error } = await tagAPI.create({})

        expect(error).toEqual(messages.tag.error.nameNotFound)
    })

    test('Create: Not Admin Should fail', async () => {

        const { tag } = mock

        tagAPI.setToken(notAdmin)

        const { error } = await tagAPI.create(tag.javascript)

        expect(error).toEqual(messages.user.error.onlyAdmin)
    })

    test('Create: Same Name Should fail', async () => {

        const { tag } = mock

        tagAPI.setToken(admin)

        await tagAPI.create(tag.javascript)
        const { error } = await tagAPI.create({ name: "JAVASCRIPT" })

        expect(error).toEqual(messages.tag.error.nameExists)
    })
    //#endregion

    //#region edit
    test('Edit: Should pass', async () => {

        const { tag } = mock

        tagAPI.setToken(admin)

        const { id } = await tagAPI.create(tag.javascript)
        const { name } = await tagAPI.update(id, tag.java)

        expect(name).toEqual(tag.java.name)
    })

    test('Edit: fake id Should fail', async () => {

        const { fakeId, tag } = mock

        tagAPI.setToken(admin)

        const { error } = await tagAPI.update(fakeId, tag.java)
        expect(error).toEqual(messages.tag.error.tagNotFound)
    })
    //#endregion

    //#region deleteTag
    test('Delete: Should pass', async () => {
        const { tag } = mock

        tagAPI.setToken(admin)

        const { id } = await tagAPI.create(tag.javascript)
        await tagAPI.deleteTag(id)

        const { error } = await tagAPI.get(id)
        expect(error).toEqual(messages.tag.error.tagNotFound)
    })

    test('Delete: course with only one tag Should fail', async () => {
        const { oneTag } = mock.course

        courseAPI.setToken(notAdmin)
        await courseAPI.create(oneTag)

        tagAPI.setToken(admin)
        const tags = await tagAPI.list()

        const { error } = await tagAPI.deleteTag(tags[0].id)

        expect(error).toEqual(messages.tag.error.courseOnlyWithThisTag)
    })

    test('Delete: course with two tags Should pass', async () => {
        const { complete } = mock.course

        courseAPI.setToken(notAdmin)
        await courseAPI.create(complete)

        tagAPI.setToken(admin)
        const tags = await tagAPI.list()

        const firstTag = tags[0]

        await tagAPI.deleteTag(firstTag.id)
        const { error } = await tagAPI.get(firstTag.id)

        expect(error).toEqual(messages.tag.error.tagNotFound)
    })
    //#endregion

    //#region get

    test('Get: Should pass', async () => {
        const { tag } = mock

        tagAPI.setToken(admin)

        const { id } = await tagAPI.create(tag.javascript)

        const { name } = await tagAPI.get(id)
        expect(name).toEqual(tag.javascript.name)
    })

    //#endregion

    //#region list
    test('list all: Should pass', async () => {
        const { javascript, java, css, scss } = mock.tag

        tagAPI.setToken(admin)

        await tagAPI.create(javascript)
        await tagAPI.create(java)
        await tagAPI.create(css)
        await tagAPI.create(scss)

        const tags = await tagAPI.list()
        expect(tags.length).toEqual(4)
    })

    test('list with filters: Should pass', async (done) => {
        const { javascript, java, css, scss } = mock.tag

        tagAPI.setToken(admin)

        await tagAPI.create(javascript)
        await tagAPI.create(java)
        await tagAPI.create(css)
        await tagAPI.create(scss)

        const result = await Promise.all([
            tagAPI.list('?name=JAVA'),
            tagAPI.list('?name=scss'),
        ])

        expect(result[0].length).toEqual(2)
        expect(result[1].length).toEqual(1)
        done()
    })
    //#endregion


})