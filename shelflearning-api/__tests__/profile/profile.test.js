const app = require('../../index')
const mock = require('../../.jest/mocks')

const { USER_TYPE } = require('../../schemas/enum')
const messages = require('../../validator/messages')
const { userAPI, authAPI } = require('../../.jest/api')
const { beforeEachSetup } = require('../../.jest/utils')



describe('Profile: logged user operations', () => {

    //#region setup
    let user = null
    let disabledUser = null
    let disabledUserId = null
    let admin = null

    afterAll(async () => {
        await app.close()
    })

    beforeEach(async () => {
        const options = {
            enabledUsers: 2,
            admins: 1
        }

        const { enabledUsers, admins } = await beforeEachSetup(options)

        user = enabledUsers[0].token
        admin = admins[0].token

        disabledUser = enabledUsers[1].token
        disabledUserId = enabledUsers[1].id
    })
    //#endregion


    test('me: should pass', async () => {        
        authAPI.setToken(user)
        const me = await authAPI.me()
        expect(me.username).toEqual('user.0')
    })

    test('me disabled user: should fail', async () => {        
        authAPI.setToken(disabledUser)

        const me = await authAPI.me()
        expect(me.username).toEqual('user.1')

        userAPI.setToken(admin)
        await userAPI.disable(disabledUserId)
        
        const {error} = await authAPI.me()
        expect(error).toEqual(messages.auth.error.userDisabled)

    })

    test('update basic info: should pass', async () => {        
        authAPI.setToken(user)

        await authAPI.basicInfo({name:'user updated name'})

        const {name} = await authAPI.me()
        expect(name).toEqual('user updated name')
    })


    test('update password: should pass', async () => {        
        authAPI.setToken(user)

        await authAPI.changePassword('123456','Password123#','Password123#')


        let auth = {
            username: 'user.0',
            password: 'Password123#'
        };

        const { token } = await authAPI.login(auth)
        expect(token).not.toBe(null)
    })

    test('update password, required fields: should fail', async () => {        
        authAPI.setToken(user)

        let request = await authAPI.changePassword('','Password123#','Password123#')
        expect(request.error).toEqual(messages.user.error.passwordNotFound)

        request = await authAPI.changePassword('123456','','')
        expect(request.error).toEqual(messages.user.error.newPasswordNotFound)
    })

    test('update password, wrong current password: should fail', async (done) => {        
        authAPI.setToken(user)

        let request = await authAPI.changePassword('wrong','Password123#', 'Password123#')
        expect(request.error).toEqual(messages.auth.error.currentPasswordWrong)

        done()
    })


})