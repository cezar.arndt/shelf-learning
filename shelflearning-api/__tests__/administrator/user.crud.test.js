const app = require('../../index')
const mock = require('../../.jest/mocks')

const { USER_TYPE } = require('../../schemas/enum')
const messages = require('../../validator/messages')
const { userAPI } = require('../../.jest/api')
const { beforeEachSetup } = require('../../.jest/utils')



describe('User: Administrator operations', () => {

    //#region setup
    let notAdmin = null
    let admin = null

    afterAll(async () => {
        await app.close()
    })

    beforeEach(async () => {
        const options = {
            enabledUsers: 1,
            admins: 1
        }

        const { enabledUsers, admins } = await beforeEachSetup(options)

        notAdmin = enabledUsers[0].token
        admin = admins[0].token
    })
    //#endregion

    //#region Insert
    test('Create: Should pass', async () => {
        const { user1 } = mock.user

        userAPI.setToken(admin)
        const { id, name, username, type } = await userAPI.create(user1)

        expect(id).not.toBe(null)
        expect(name).toEqual(user1.name)
        expect(username).toEqual(user1.username)
        expect(type).toEqual(USER_TYPE.USER)
    })

    test('Create same username: Should fail', async () => {
        const { user1 } = mock.user

        userAPI.setToken(admin)
        await userAPI.create(user1)
        const { error } = await userAPI.create(user1)

        expect(error).toEqual(messages.user.error.userNameExists)
    })

    test('Create not admin: Should fail', async () => {
        const { user1 } = mock.user

        userAPI.setToken(notAdmin)
        const { error } = await userAPI.create(user1)

        expect(error).toEqual(messages.user.error.onlyAdmin)
    })
    //#endregion

    //#region disable
    test('Disable: Should pass', async () => {
        const { user1 } = mock.user

        userAPI.setToken(admin)
        const { id, disabledAt } = await userAPI.create(user1)
        expect(disabledAt).toEqual(null)

        const { disabledAt: d1 } = await userAPI.disable(id)
        expect(d1).not.toBe(null)
    })

    test('Disable twice: Should fail', async () => {
        const { user1 } = mock.user

        userAPI.setToken(admin)
        const { id, disabledAt } = await userAPI.create(user1)
        expect(disabledAt).toEqual(null)

        await userAPI.disable(id)
        const { error } = await userAPI.disable(id)

        expect(error).toEqual(messages.user.error.alreadyDisabled)
    })
    //#endregion

    //#region enable
    test('Enable: Should pass', async () => {
        const { user1 } = mock.user

        userAPI.setToken(admin)
        const { id } = await userAPI.create(user1)
        const { disabledAt: d1 } = await userAPI.disable(id)
        expect(d1).not.toBe(null)

        const { disabledAt: d2 } = await userAPI.enable(id)
        expect(d2).toEqual(null)
    })

    test('Enable twice: Should fail', async () => {

        const { user1 } = mock.user

        userAPI.setToken(admin)
        const { id } = await userAPI.create(user1)

        const { error } = await userAPI.enable(id)
        expect(error).toEqual(messages.user.error.alreadyEnabled)
    })
    //#endregion

    //#region get
    test('Get: Should pass', async () => {
        const { user1 } = mock.user

        userAPI.setToken(admin)
        const { id } = await userAPI.create(user1)

        const { name } = await userAPI.get(id)
        expect(name).toEqual(user1.name)
    })
    //#endregion

    //#region list
    test('list All: Should pass', async () => {
        const { user1, user2, user3 } = mock.user

        userAPI.setToken(admin)
        await userAPI.create(user1)
        await userAPI.create(user2)
        await userAPI.create(user3)


        const users = await userAPI.list()
        expect(users.length).toEqual(5)
    })

    test('list with filters: Should pass', async (done) => {
        const { user1, user2, user3 } = mock.user

        userAPI.setToken(admin)
        await userAPI.create(user1)
        await userAPI.create(user2)
        await userAPI.create(user3)

        const result = await Promise.all([
            userAPI.list('?name=user'),
            userAPI.list('?name=admin')
        ])

        expect(result[0].length).toEqual(4)
        expect(result[1].length).toEqual(1)

        done()
    })
    //#endregion


})