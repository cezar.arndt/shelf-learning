const routes = require('express').Router()
const controller = require('../controllers/tag')
const { JWTHandler } = require('../middlewares/jwt')

routes.get('/', JWTHandler, controller.list)
routes.get('/export', JWTHandler, controller.exportData)
routes.get('/:id', JWTHandler, controller.get)

routes.post('/', JWTHandler, controller.create)
routes.put('/:id', JWTHandler, controller.update)
routes.delete('/:id', JWTHandler, controller.deleteTag)

module.exports = routes