const routes = require('express').Router()
const controller = require('../controllers/instructor/statistics')
const { JWTHandler } = require('../middlewares/jwt')


routes.get('/course-per-status',  JWTHandler, controller.coursePerStatus)
routes.get('/user-and-students',  JWTHandler, controller.userAndStudents)
routes.get('/students-per-course',  JWTHandler, controller.studentsPerCourse)
routes.get('/done-rate-per-course',  JWTHandler, controller.doneRatePerCourse)
routes.get('/enroll-per-month',  JWTHandler, controller.enrollPerMonth)


module.exports = routes