const routes = require('express').Router()
const controller = require('../../controllers/public/course')
const { JWTHandler } = require('../../middlewares/jwt')

routes.get('/enrollment-info', JWTHandler, controller.enrollmentInfo)
routes.get('/enrollment', JWTHandler, controller.enrollment)
routes.get('/export', JWTHandler, controller.exportData)
routes.get('/export-enroll', JWTHandler, controller.exportDataEnroll)
routes.get('/:id/export-tree', JWTHandler, controller.exportTree)
routes.get('/:id', JWTHandler, controller.get)
routes.get('/', JWTHandler, controller.list)
routes.get('/:id/page/:pageId', JWTHandler, controller.getPage)


routes.post('/:id/enroll', JWTHandler, controller.enroll)
routes.put('/:id/progress', JWTHandler, controller.progress)

module.exports = routes