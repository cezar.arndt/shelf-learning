const routes = require('express').Router()
const controller = require('../../controllers/administrator/user')
const { JWTHandler } = require('../../middlewares/jwt')

routes.post('/', JWTHandler, controller.create)
routes.put('/:id/enable', JWTHandler, controller.enable)
routes.put('/:id/disable', JWTHandler, controller.disable)
routes.get('/export', JWTHandler, controller.exportData)
routes.get('/:id', JWTHandler, controller.get)
routes.get('/', JWTHandler, controller.list)


module.exports = routes