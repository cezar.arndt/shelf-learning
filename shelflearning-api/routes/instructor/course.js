const routes = require('express').Router()
const controller = require('../../controllers/instructor/course')
const { JWTHandler } = require('../../middlewares/jwt')

routes.post('/', JWTHandler, controller.create)
routes.put('/:id', JWTHandler, controller.update)
routes.put('/:id/status', JWTHandler, controller.updateStatus)
routes.get('/export', JWTHandler, controller.exportData)
routes.get('/:id/export-tree', JWTHandler, controller.exportTree)
routes.get('/:id', JWTHandler, controller.get)
routes.get('/:id/info', JWTHandler, controller.getInfo)
routes.get('/', JWTHandler, controller.list)
routes.delete('/:id', JWTHandler, controller.remove)


module.exports = routes