const routes = require('express').Router()
const controller = require('../../controllers/instructor/section')
const { JWTHandler } = require('../../middlewares/jwt')


routes.post('/', JWTHandler, controller.create)
routes.put('/:id/title', JWTHandler, controller.updateTitle)
routes.put('/:id/order', JWTHandler, controller.updateOrder)
routes.get('/:id', JWTHandler, controller.get)
routes.get('/', JWTHandler, controller.list)
routes.delete('/:id', JWTHandler, controller.remove)


module.exports = routes