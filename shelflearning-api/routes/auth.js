const routes = require('express').Router()
const controller = require('../controllers/public/auth')
const { JWTHandler } = require('../middlewares/jwt')


routes.post('/',  controller.login)
routes.post('/create-account',  controller.createAccount)
routes.post('/activate-account',  controller.activateAccount)
routes.post('/reset-activation-code',  controller.resetActivationCode)
routes.post('/reset-password-code',  controller.resetPasswordCode)
routes.post('/reset-password',  controller.resetPassword)
routes.post('/external-login',  controller.externalLogin)


routes.get('/me',  JWTHandler, controller.me)
routes.put('/me/basic',  JWTHandler, controller.basic)
routes.put('/me/password',  JWTHandler, controller.changePassword)

module.exports = routes