const injectSectionId = (req, res, next) => {
    req.sectionId = req.params.sectionId
    next()
}

module.exports = { injectSectionId }
