const jwt = require('jsonwebtoken')
const { unauthorized } = require('../validator/response')
const messages = require('../validator/messages')

const JWTHandler = (req, res, next) => {
    var token = req.headers['access-token']

    if (!token) {
        const { message, code } = unauthorized(messages.auth.error.tokenNotFound)
        return res.status(code).send({ error: message })
    }

    jwt.verify(token, process.env.SECRET, (err, decoded) => {
        if (err) {
            const { message, code } = unauthorized(messages.auth.error.tokenNotValid)
            return res.status(code).send({ error: message })
        }

        req.me = decoded.id
        next()
    })
}


module.exports = { JWTHandler }