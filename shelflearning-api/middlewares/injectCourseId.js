const injectCourseId = (req, res, next) => {
    req.courseId = req.params.courseId
    next()
}

module.exports = { injectCourseId }
