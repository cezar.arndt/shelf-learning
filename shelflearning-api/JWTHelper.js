const jwt = require('jsonwebtoken')

const generateToken = (user) => {

    const payload = {
        id: user.id,
        username: user.username,
        name: user.name,
        type: user.type
    }

    const token = jwt.sign(payload, process.env.SECRET, {
        expiresIn: process.env.AUTH_EXPIRES_TIME
    })

    if (!token) {
        throw new "Error on generating token"
    }

    return token
}


module.exports = { generateToken }