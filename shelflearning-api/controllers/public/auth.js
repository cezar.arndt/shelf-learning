const { responseBuild } = require('../responseBuild')
const service = require('../../services/auth')


const login = async ({ body }, res) => responseBuild(service.login, res, body)
const createAccount = async ({ body }, res) => responseBuild(service.createAccount, res, body)
const activateAccount = async ({ body }, res) => responseBuild(service.activateAccount, res, body)
const resetActivationCode = async ({ body }, res) => responseBuild(service.resetActivationCode, res, body)
const resetPasswordCode = async ({ body }, res) => responseBuild(service.resetPasswordCode, res, body)
const resetPassword = async ({ body }, res) => responseBuild(service.resetPassword, res, body)
const me = async ({ me }, res) => responseBuild(service.me, res, me)
const externalLogin = async ({ body }, res) => responseBuild(service.externalLogin, res, body)



const basic = async ({ me, body }, res) => responseBuild(service.basic, res, me, body)
const changePassword = async ({ me, body }, res) => responseBuild(service.changePassword, res, me, body)

module.exports = {
    login,
    me,
    createAccount,
    activateAccount,
    resetPassword,
    resetActivationCode,
    resetPasswordCode,
    basic,
    changePassword,
    externalLogin
}