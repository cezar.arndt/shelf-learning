const { responseBuild, downloadResponseBuild } = require('../responseBuild')
const service = require('../../services/public/course')



const get = async ({ me, params: { id } }, res) => responseBuild(service.get, res, id, me)

const list = async ({ me, query }, res) => responseBuild(service.list, res, query, me)

const enroll = async ({ me, params: { id } }, res) => responseBuild(service.enroll, res, me, id )

const enrollmentInfo = async ({ me }, res) => responseBuild(service.enrollmentInfo, res, me )

const enrollment = async ({ me, query }, res) => responseBuild(service.enrollment, res, query, me )

const progress = async ({ me, params: { id }, body  }, res) => responseBuild(service.updateProgress, res, me, id, body)

const exportData = async ({ me, query }, res) => downloadResponseBuild('courses',service.exportData, res, me, query)

const exportDataEnroll = async ({ me, query }, res) => downloadResponseBuild('my-courses',service.exportDataEnroll, res, me, query)

const exportTree = async ({ me, params: { id } }, res) => downloadResponseBuild('course-tree',service.exportTree, res, id, me)

const getPage = async ({ me, params: { id, pageId }  }, res) => responseBuild(service.getPage, res, me, id, pageId)

module.exports = {
    get,
    list,
    enroll,
    enrollmentInfo,
    enrollment,
    progress,
    getPage,
    exportData,
    exportDataEnroll,
    exportTree
}