const { responseBuild } = require('../responseBuild')
const service = require('../../services/instructor/page')


const create = async ({ me, courseId, sectionId, body }, res) => { responseBuild(service.create, res, me, courseId, sectionId, body) }

const update = async ({ me, courseId, sectionId, params: { id }, body }, res) => responseBuild(service.update, res, me, courseId, sectionId, id, body)

const updateDescription = async ({ me, courseId, sectionId, params: { id }, body }, res) => responseBuild(service.updateDescription, res, me, courseId, sectionId, id, body)

const updateOrder = async ({ me, courseId, sectionId, params: { id }, body }, res) => responseBuild(service.updateOrder, res, me, courseId, sectionId, id, body)

const get = async ({ me, courseId, sectionId, params: { id } }, res) => responseBuild(service.get, res, me, courseId, sectionId, id)

const list = async ({ me, courseId, sectionId, query }, res) => responseBuild(service.list, res, me, courseId, sectionId, query)

const remove = async ({ me, courseId, sectionId, params:{id} }, res) => responseBuild(service.remove, res, me, courseId, sectionId, id)


module.exports = {
    create,
    update,
    updateDescription,
    updateOrder,
    get,
    list,
    remove
}