const { responseBuild } = require("../responseBuild");
const service = require("../../services/instructor/statistics");

const coursePerStatus = async ({ me }, res) => responseBuild(service.coursePerStatus, res, me);
const userAndStudents = async ({ me }, res) => responseBuild(service.userAndStudents, res, me);
const studentsPerCourse = async ({ me }, res) => responseBuild(service.studentsPerCourse, res, me);
const doneRatePerCourse = async ({ me }, res) => responseBuild(service.doneRatePerCourse, res, me);
const enrollPerMonth = async ({ me }, res) => responseBuild(service.enrollPerMonth, res, me);

module.exports = {
  coursePerStatus,
  userAndStudents,
  studentsPerCourse,
  doneRatePerCourse,
  enrollPerMonth
};
