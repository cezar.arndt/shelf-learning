const { responseBuild, downloadResponseBuild } = require('../responseBuild')
const service = require('../../services/instructor/course')


const create = async ({ body, me }, res) => responseBuild(service.create, res, me, body)

const get = async ({ me, params: { id } }, res) => responseBuild(service.get, res, me, id)

const getInfo = async ({ me, params: { id } }, res) => responseBuild(service.getInfo, res, me, id)

const list = async ({ me, query }, res) => responseBuild(service.list, res, me, query)

const updateStatus = async ({ me, params: { id }, body }, res) => responseBuild(service.updateStatus, res, me, id, body)

const update = async ({ me, params: { id }, body }, res) => responseBuild(service.update, res, me, id, body)

const remove = async ({ me, params: { id } }, res) => responseBuild(service.remove, res, me, id)

const exportData = async ({ me, query }, res) => downloadResponseBuild('teach',service.exportData, res, me, query)

const exportTree = async ({ me, params: { id } }, res) => downloadResponseBuild('course-tree',service.exportTree, res, me, id)



module.exports = {
    create,
    update,
    get,
    getInfo,
    list,
    updateStatus,
    remove,
    exportData,
    exportTree
}