const { responseBuild } = require('../responseBuild')
const service = require('../../services/instructor/section')


const create = async ({ me, courseId, body }, res) => { responseBuild(service.create, res, me, courseId, body) }

const updateTitle = async ({ me, courseId, params: { id }, body }, res) => responseBuild(service.updateTitle, res, me, courseId, id, body)

const updateOrder = async ({ me, courseId, params: { id }, body }, res) => responseBuild(service.updateOrder, res, me, courseId, id, body)

const get = async ({ me, courseId, params: { id } }, res) => responseBuild(service.get, res, me, courseId, id)

const list = async ({ me, courseId, query }, res) => responseBuild(service.list, res, me, courseId, query)

const remove = async ({ me, courseId, params: { id } }, res) => responseBuild(service.remove, res, me, courseId, id)


module.exports = {
    create,
    updateTitle,
    updateOrder,
    get,
    list,
    remove
}