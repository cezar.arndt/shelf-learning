const { responseBuild, downloadResponseBuild } = require('./responseBuild')
const service = require('../services/tag')


const list = async ({ query }, res) => responseBuild(service.list, res, query)

const get = async ({ me, params: { id } }, res) => responseBuild(service.get, res, me, id)

const create = async ({ me, body }, res) => responseBuild(service.create, res, me, body)

const update = async ({ me, body, params: { id } }, res) => responseBuild(service.update, res, me, id, body)

const deleteTag = async ({ me, params: { id } }, res) => responseBuild(service.deleteTag, res, me, id)

const exportData = async ({ me, query }, res) => downloadResponseBuild('tags',service.exportData, res, me, query)

module.exports = {
    get,
    list,
    create,
    update,
    deleteTag,
    exportData
}