const responseBuild = async (method, res, ...params) => {
    try {
        const response = await method(...params)
        res.status(200).send(response)
    } catch (error) {
        try {
            e = JSON.parse(error.message)
            res.status(e.code || 500).send({ error: e.message })
        } catch (ee) {
            console.error(error)
        }
    }

}

const downloadResponseBuild = async (filename, method, res, ...params) => {
    try {
        const response = await method(...params)

        res.setHeader("Content-Type", "text/csv; charset=utf-8");
        res.setHeader("Content-Disposition", `attachment; filename=${filename}_${(new Date).toISOString().slice(0,10)}.csv`);
        res.charset = 'utf-8';
    
        res.status(200).end(response);
        
    } catch (error) {
        try {
            e = JSON.parse(error.message)
            res.status(e.code || 500).send({ error: e.message })
        } catch (ee) {
            console.error(error)
        }
    }

}



module.exports = { responseBuild, downloadResponseBuild }