const { responseBuild, downloadResponseBuild } = require('../responseBuild')
const service = require('../../services/administrator/user')



const create = async ({ body, me }, res) => responseBuild(service.create, res, me, body)

const get = async ({ me, params: { id } }, res) => responseBuild(service.get, res, me, id)

const list = async ({ me, query }, res) => responseBuild(service.list, res, me, query)

const enable = async ({ me, params: { id } }, res) => responseBuild(service.enable, res, me, id)

const disable = async ({ me, params: { id } }, res) => responseBuild(service.disable, res, me, id)

const exportData = async ({ me, query }, res) => downloadResponseBuild('users',service.exportData, res, me, query)


module.exports = {
    create,
    enable,
    disable,
    get,
    list,
    exportData
}