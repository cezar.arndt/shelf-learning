const { Course, User, UserCourse, Page, Tag } = require('../schemas')
const { COURSE_STATUS, USER_TYPE } = require('../schemas/enum')
const messages = require('./messages')
const { badRequest, notFound, forbidden } = require('./response')
const { diacriticSensitiveRegex } = require('./input')


const throwIfTitleExists = async (title) => {
    const regex = new RegExp(["^", diacriticSensitiveRegex(title), "$"].join(""), "i")
    let course = await Course.findOne({ title: regex })

    if (course) {
        throw new Error(JSON.stringify(badRequest(messages.course.error.titleExists)))
    }

}

const throwIfTagExists = async (name) => {
    const regex = new RegExp(["^", diacriticSensitiveRegex(name), "$"].join(""), "i")
    let tag = await Tag.findOne({ name: regex })

    if (tag) {
        throw new Error(JSON.stringify(badRequest(messages.tag.error.nameExists)))
    }

}

const throwIfCourseOnlyHasOneTag = async(tag) => {

    let courses = await Course.find({ tags:{ _id:tag._id} })

    for( let i = 0, j = courses.length; i < j; i++ ){
        if( courses[i].tags.length == 1 ){
            throw new Error(JSON.stringify(badRequest(messages.tag.error.courseOnlyWithThisTag)))
        }
    }
}

const throwIfSectionTitleExists = async (course, title) => {

    const regex = new RegExp(["^", diacriticSensitiveRegex(title), "$"].join(""), "i")
    let section = await Course.findOne({ _id: course.id, sections: { $elemMatch: { title: regex } } })

    if (section) {
        throw new Error(JSON.stringify(badRequest(messages.course.section.error.titleExists)))
    }

}

const throwIfPageTitleExists = async (courseId, sectionId, title) => {

    const regex = new RegExp(["^", diacriticSensitiveRegex(title), "$"].join(""), "i")

    const courseDB = await Course.findOne({ _id: courseId })
    await courseDB.populate({ path: 'sections.pages', select: 'title -_id' }).execPopulate()

    if (courseDB.sections.find(s => s._id.equals(sectionId)).pages.find(p => regex.test(p.title))) {
        throw new Error(JSON.stringify(badRequest(messages.course.page.error.titleExists)))
    }
}

const throwIfUserNameExists = async (username) => {
    const regex = new RegExp(["^", diacriticSensitiveRegex(username), "$"].join(""), "i")
    const user = await User.findOne({ username: regex })

    if (user) {
        throw new Error(JSON.stringify(badRequest(messages.user.error.userNameExists)))
    }
}

const throwIfPublishiedAndHasNoPagesOrEmpty = async (course, toStatus) => {
    if (toStatus === COURSE_STATUS.PUBLISHED) {

        if( course.sections.length === 0 ) {
            throw new Error(JSON.stringify(badRequest(messages.course.error.noSections)))
        }

        for( var i = 0, j = course.sections.length; i < j; i++ ){
            const section = course.sections[i]
            if( section.pages.length == 0 ) {
                const error = {...messages.course.error.noPages}
                error.message =error.message.replace(':sectionId',section.title)
                throw new Error(JSON.stringify(badRequest(error)))
            }
            
            for( var m = 0, n = section.pages.length; m < n; m++ ){
                const page = await Page.findById(section.pages[m], 'title descriptionLength')
                if( page.descriptionLength === 0 ){

                    const error = {...messages.course.error.emptyPage}
                    error.message = error.message.replace(':pageId',page.title)
                    throw new Error(JSON.stringify(badRequest(error)))
                }
            }
        }

    }
}


const throwIfUserAlreadyEnrolled = async (course, me) => {

    const enrollment = await UserCourse.findOne({ user: me, course: course.id })

    if (enrollment) {
        throw new Error(JSON.stringify(notFound(messages.course.error.alreadyEnrolled)))
    }
}

const throwIfUserNotEnrolled = async (course, me, customMessage=messages.course.error.notEnrolled) => {

    const enrollment = await UserCourse.findOne({ user: me, course: course.id })

    if (!enrollment) {
        throw new Error(JSON.stringify(notFound(customMessage)))
    }
}

const throwIfPageNotFound = (page) => {
    if (!page) {
        throw new Error(JSON.stringify(notFound(messages.course.page.error.pageNotFound)))
    }
}


const throwIfUserNotExists = (user) => {
    if (!user) {
        throw new Error(JSON.stringify(notFound(messages.user.error.userNotFound)))
    }
}

const throwIfUserIsDisabled = (user) => {

    if (user.disabledAt !== null) {
        throw new Error(JSON.stringify(badRequest(messages.user.error.alreadyDisabled)))
    }
}

const throwIfUserIsEnabled = (user) => {

    if (user.disabledAt === null) {
        throw new Error(JSON.stringify(badRequest(messages.user.error.alreadyEnabled)))
    }
}


const throwIfStatusChangeNotAllowed = (course, user, toStatus) => {
    const { history: e } = messages.course.error

    const statusMessages = {
        statusSame: badRequest(e.statusSame),
        invalidChange: badRequest(e.invalidChange),
        invalidChangeToPublishied: badRequest(e.invalidChangeToPublishied),
        onlyOwnerPublish: badRequest(e.onlyOwnerPublish),
        onlyAdminBlock: badRequest(e.onlyAdminBlock),
        onlyOwnerClose: badRequest(e.onlyOwnerClose),
        onlyAdminUnblock: badRequest(e.onlyAdminUnblock)
    }


    if (toStatus !== COURSE_STATUS.BLOCKED && toStatus === course.status) {
        throw new Error(JSON.stringify(statusMessages.statusSame))
    }

    // Rascunho para excluído

    switch (course.status) {

        case COURSE_STATUS.DRAFT:
            if (!toPublishied())
                throw new Error(JSON.stringify(statusMessages.invalidChange))

            if (!isUser())
                throw new Error(JSON.stringify(statusMessages.onlyOwnerPublish))
            break

        case COURSE_STATUS.CLOSED:
            throw new Error(JSON.stringify(statusMessages.invalidChange))
            break

        case COURSE_STATUS.PUBLISHED:
            if (!toBlocked() && !toClosed())
                throw new Error(JSON.stringify(statusMessages.invalidChange))

            if (toBlocked() && !isAdmin())
                throw new Error(JSON.stringify(statusMessages.onlyAdminBlock))

            if (toClosed() && !isUser())
                throw new Error(JSON.stringify(statusMessages.onlyOwnerClose))

            break

        case COURSE_STATUS.BLOCKED:

            if (!toPublishied() && !toBlocked())
                throw new Error(JSON.stringify(statusMessages.invalidChange))

            if (!isAdmin())
                throw new Error(JSON.stringify(statusMessages.onlyAdminUnblock))

            break

    }


    function isAdmin() {
        return user.type === USER_TYPE.ADMIN
    }

    function isUser() {
        return user.type === USER_TYPE.USER
    }

    function toClosed() {
        return toStatus === COURSE_STATUS.CLOSED
    }

    function toBlocked() {
        return toStatus === COURSE_STATUS.BLOCKED
    }

    function toPublishied() {
        return toStatus === COURSE_STATUS.PUBLISHED
    }

}

const throwIfSectionNotFound = (course, id) => {
    const sectionDB = course.sections.find(s => s._id.equals(id))
    if (!sectionDB) {
        throw new Error(JSON.stringify(notFound(messages.course.section.error.sectionNotFound)))
    }
}

const throwIfCourseNotFound = (course) => {
    if (!course) {
        throw new Error(JSON.stringify(notFound(messages.course.error.courseNotFound)))
    }
}

const throwIfTagNotFound = (tag) => {
    if (!tag) {
        throw new Error(JSON.stringify(notFound(messages.tag.error.tagNotFound)))
    }
}

const throwIfCourseBelongsToOther = (course, createdBy) => {
    if (!course.createdBy.equals(createdBy)) {
        throw new Error(JSON.stringify(notFound(messages.course.error.courseNotFound)))
    }
}

const throwIfStatusNotDraft = (course) => {
    if (course.status !== COURSE_STATUS.DRAFT) {
        throw new Error(JSON.stringify(badRequest(messages.course.error.notDraft)))
    }
}

const throwIfStatusDraft = (course) => {
    if (course.status === COURSE_STATUS.DRAFT) {
        throw new Error(JSON.stringify(badRequest(messages.course.error.courseNotFound)))
    }
}

const throwIfStatusDraftOrClosed = (course) => {
    if (course.status === COURSE_STATUS.DRAFT || course.status === COURSE_STATUS.CLOSED) {
        throw new Error(JSON.stringify(badRequest(messages.course.error.courseNotFound)))
    }
}

const throwIfCourseIsNotPublishiedOrClosed = (course) => {
    if (course.status !== COURSE_STATUS.PUBLISHED && course.status !== COURSE_STATUS.CLOSED) {
        throw new Error(JSON.stringify(forbidden(messages.course.error.courseNotFound)))
    }
}

const throwIfCourseIsNotPublishied = (course) => {
    if (course.status !== COURSE_STATUS.PUBLISHED) {
        throw new Error(JSON.stringify(forbidden(messages.course.error.notPublished)))
    }
}

const throwIfCourseBelongsToMe = (course, createdBy) => {
    if (course.createdBy.equals(createdBy)) {
        throw new Error(JSON.stringify(notFound(messages.course.error.isMycourse)))
    }
}

const throwIfNotAdmin = (user) => {
    if (user.type !== USER_TYPE.ADMIN) {
        throw new Error(JSON.stringify(forbidden(messages.user.error.onlyAdmin)))
    }
}

const throwIfAdmin = async (me, message=messages.course.error.adminCant) => {
    const user = await User.findById(me)
    if (user.type === USER_TYPE.ADMIN) {
        throw new Error(JSON.stringify(forbidden(message)))
    }
}

const throwIfUserNotFound = (user, message=messages.auth.error.userNotFound) => {
    if (!user) {
        throw new Error(JSON.stringify(notFound(message)))
    }
}

module.exports = {
    throwIfTitleExists,
    throwIfTagExists,
    throwIfCourseOnlyHasOneTag,
    throwIfCourseNotFound,
    throwIfTagNotFound,
    throwIfCourseBelongsToOther,
    throwIfStatusNotDraft,
    throwIfStatusDraft,
    throwIfSectionTitleExists,
    throwIfSectionNotFound,
    throwIfStatusChangeNotAllowed,
    throwIfPageTitleExists,
    throwIfPageNotFound,
    throwIfUserNameExists,
    throwIfUserNotExists,
    throwIfUserIsDisabled,
    throwIfUserIsEnabled,
    throwIfCourseBelongsToMe,
    throwIfUserAlreadyEnrolled,
    throwIfUserNotEnrolled,
    throwIfCourseIsNotPublishiedOrClosed,
    throwIfCourseIsNotPublishied,
    throwIfPublishiedAndHasNoPagesOrEmpty,
    throwIfNotAdmin,
    throwIfAdmin,
    throwIfUserNotFound,
    throwIfStatusDraftOrClosed
}