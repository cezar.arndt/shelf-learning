const { badRequest } = require("./response");
const messages = require("./messages");

const throwIfEmpty = (field, message) => {
  if (Array.isArray(field) && field.length < 1) {
    throw new Error(JSON.stringify(badRequest(message)));
  }

  if (field === undefined || field.toString().trim().length == 0) {
    throw new Error(JSON.stringify(badRequest(message)));
  }
};

const isSameValue = (field1, field2) => {
  return field1.trim().toLowerCase() === field2.trim().toLowerCase();
};

const throwIf = (condition, message) => {
  if (condition) {
    throw new Error(JSON.stringify(message));
  }
};

const diacriticSensitiveRegex = (string = "") => {
  return string
    .trim()

    .replace(/[a,á,à,ä,â,ã]/gi, "a")
    .replace(/[e,é,ë,ê]/gi, "e")
    .replace(/[i,í,ï,î]/gi, "i")
    .replace(/[o,ó,ö,ò,ô,õ]/gi, "o")
    .replace(/[c,ç]/gi, "c")
    .replace(/[u,ü,ú,ù,û]/gi, "u")

    .replace(/a/gi, "[a,á,à,ä,â,ã]")
    .replace(/e/gi, "[e,é,ë,ê]")
    .replace(/i/gi, "[i,í,ï,î]")
    .replace(/o/gi, "[o,ó,ö,ò,ô,õ]")
    .replace(/c/gi, "[c,ç]")
    .replace(/u/gi, "[u,ü,ú,ù,û]");
};

const throwIfPasswordInvalid = (password, passwordConfirm) => {
  throwIf(
    password !== passwordConfirm,
    badRequest(messages.auth.error.passwordsDoesntMatch)
  );

  const re = /^(?=.*[\*.! @#\$%\^&\(\)\{\}\[\]:;<>,\.\?\/~_\+\-=\|])(?=.*[0-9])(?=.*[a-z]).{6,}$/;

  throwIf(
    !re.test(password.toLowerCase()),
    badRequest(messages.auth.error.passwordRule)
  );
};

const throwIfEmailInvalid = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  throwIf(
    !re.test(String(email).toLowerCase()),
    badRequest(messages.auth.error.emailRule)
  )
};

module.exports = {
  throwIfEmpty,
  isSameValue,
  throwIf,
  diacriticSensitiveRegex,
  throwIfPasswordInvalid,
  throwIfEmailInvalid
};
