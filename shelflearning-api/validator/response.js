const badRequest = (message) => {
    return { message, code: 400 }
}

const forbidden = (message) => {
    return { message, code: 403 }
}

const unauthorized = (message) => {
    return { message, code: 401 }
}

const notFound = (message) => {
    return { message, code: 404 }
}

module.exports = {
    badRequest,
    forbidden,
    notFound,
    unauthorized
}