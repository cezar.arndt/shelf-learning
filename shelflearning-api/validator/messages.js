
const course = {
    error: {
        idNotFound: {code:'C01', message:'O identificador deve ser informado'},
        titleNotFound: {code:'C02', message:'O título deve ser informado'},
        tagNotFound: {code:'C03', message:'Ao menos uma tag deve ser associada/criada'},
        titleExists: {code:'C04', message:'Já existe um curso com esse mesmo nome'},
        notDraft: {code:'C05', message:'O curso deve estar em "rascunho"'},
        notDraftOrBlocked: {code:'C06', message:'O curso deve estar em "rascunho" ou "Bloqueado"'},
        notPublished: {code:'C07', message:'O curso não esta publicado'},
        courseNotFound: {code:'C08', message:'Curso não encontrado'},
        isMycourse: {code:'C09', message:'Você é o professor nesse curso'},
        alreadyEnrolled: {code:'C10', message:'Você já é matriculado nesse curso'},
        notEnrolled: {code:'C11', message:'Você não é matriculado nesse curso'},
        currentPageNotInformed: {code:'C12', message:'A página deve ser informada para atualizar o progresso do curso'},
        currentPositionNotInformed: {code:'C13', message:'A posição da página deve ser informada para atualizar o progresso do curso'},
        noPages:{code:'C15', message:'A seção ":sectionId" não possúi nenhuma página'},
        noSections: {code:'C15', message:'O curso não tem nenhuma seção'},
        emptyPage: {code:'C16', message:'A página ":pageId" não tem conteúdo'},
        adminCantEnroll: {code:'C17', message:'Você esta logado como administrador, essa conta não pode se inscrever em nenhum curso'},
        adminCantProgress: {code:'C18', message:'Você esta logado como administrador, essa conta não tem progresso'},
        adminCant: {code:'C19', message:'Você esta logado como administrador'},

        history: {
            toNotFound: {code:'CH01', message: 'O status deve ser informado'},
            reasonNotFound: {code:'CH02', message: 'O motivo deve ser informado'},
            statusSame: {code:'CH03', message: 'O status deve ser diferente do atual'},
            invalidStatus: {code:'CH04', message: 'O status é inválido'},

            invalidChange: {code:'CH05', message: 'Alteração de status inválida'},
            onlyOwnerPublish: {code:'CH06', message: 'Somente o professor pode publicar o curso'},
            onlyAdminBlock: {code:'CH07', message: 'Somente um administrador pode bloquear o curso'},
            onlyAdminUnblock: {code:'CH08', message: 'Somente um administrador pode desbloquear o curso'},
            onlyOwnerClose: {code:'CH09', message: 'Somente o professor pode fechar o curso'}
        },

    },

    page: {
        error: {
            titleNotFound: {code:'P01', message: 'O título deve ser informado'},
            descriptionNotFound: {code:'P02', message: 'A descrição deve ser informada'},
            titleExists: {code:'P03', message: 'Já existe uma página com esse mesmo nome'},
            pageNotFound: {code:'P04', message: 'Página não encontrada'},
            orderNotFound: {code:'P05', message: 'Ordem deve ser informada'},
            orderLessThan0: {code:'P06', message: 'Ordem deve ser no minimo 0'},
        }
    },

    section: {
        error: {
            titleNotFound: {code:'S01', message: 'O título deve ser informado'},
            titleExists: {code:'S02', message: 'Já existe uma seção com esse mesmo nome'},
            sectionNotFound: {code:'S03', message: 'Seção não encontrada'},
            orderNotFound: {code:'S04', message: 'Ordem deve ser informada'},
            orderLessThan0: {code:'S05', message: 'Ordem deve ser no minimo 0'},
        }
    }
}

const tag = {
    error: {
        idNotFound: {code: 'T01', message: 'O identificador deve ser informado'},
        nameNotFound: {code: 'T02', message: 'O nome deve ser informado'},
        nameExists: {code: 'T03', message: 'Já existe uma tag com esse mesmo nome'},
        tagNotFound: {code: 'T04', message: 'Tag não encontrada'},
        courseOnlyWithThisTag: {code: 'T05', message: 'Existem cursos que possuem apenas essa tag como relacionamento'}
    }
}

const user = {
    error: {
        onlyAdmin: {code: 'U01', message: 'Somente um administrador pode executar essa operação'},
        userNameNotFound: {code: 'U02', message: 'Email deve ser informado'},
        nameNotFound: {code: 'U03', message: 'Nome deve ser informado'},
        passwordNotFound: {code: 'U04', message: 'Senha deve ser informada'},
        newPasswordNotFound: {code: 'U05', message: 'a Nova Senha deve ser informado'},
        typeNotFound: {code: 'U06', message: 'Tipo deve ser informado'},
        userNameExists: {code: 'U07', message: 'Esse email já existe'},
        userNotFound: {code: 'U08', message: 'Usuário não encontrado'},
        alreadyDisabled: {code: 'U09', message: 'Usuário já esta desabilitado'},
        alreadyEnabled: {code: 'U10', message: 'Usuário já esta ativo'},
        onlySuperUser: {code: 'U11', message: 'Esse usuário só pode ser desabilitado pelo \'super usuário\''}
    }
}

const auth = {
    error: {
        unknow:{code: 'A00', message: 'Erro desconhecido, entre em contato com o administrador'},
        tokenNotFound: {code: 'A01', message: 'Token de acesso não encontrado'},
        tokenNotValid: {code: 'A02', message: 'Token inválido'},
        userNotFound: {code: 'A03', message: 'Usuário não encontrado'},
        userDisabled: {code: 'A04', message: 'Usuário desabilitado, contate o administrador'},
        userNotActive: {code: 'A05', message: 'Essa conta não esta ativada'},
        credentialsWrong: {code: 'A06', message: 'Usuário ou senha incorreto'},
        currentPasswordWrong: {code: 'A07', message: 'Senha atual incorreta'},
        credentialsNotFound: {code: 'A08', message: 'Username e Senha são obrigatórios'},
        passwordsDoesntMatch: {code: 'A09', message: 'A senhas não são identicas'},
        validationTokenNotFound: {code: 'A10', message: 'Código de ativação não informado'},
        invalidToken: {code: 'A11', message: 'Código inválido'},
        userAlreadyActive: {code: 'A12', message: 'Essa conta já esta ativada'},
        expiredToken: {code: 'A13', message: 'Código expirado, reenvie um novo código de ativação para o seu e-mail'},
        usernameNotFound: {code: 'A14', message: 'Email deve ser informado'},
        passwordRule: {code: 'A15', message: 'Deve ter 6 dígitos contendo numero letra e caracter especial'},
        emailRule: {code: 'A16', message: 'E-mail inválido'},
        facebookExpiredToken: {code: 'A17', message: 'Esse código de acesso esta expirado'},
    }
}


module.exports = {
    course,
    user,
    tag,
    auth
}