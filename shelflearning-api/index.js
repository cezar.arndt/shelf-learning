const dotenv = require('dotenv')
const path = require('path');
const express = require('express')
const { injectCourseId } = require('./middlewares/injectCourseId')
const { injectSectionId } = require('./middlewares/injectSectionId')
const app = express()

dotenv.config()
app.use(express.json({limit:'10mb'}))

if( process.env.ENVIROMENT === 'production'){
    app.use(express.static('public'));
}

app.use('/api/v1/auth', require('./routes/auth'))
app.use('/api/v1/user', require('./routes/administrator/user'))


app.use('/api/v1/tag', require('./routes/tag'))

app.use('/api/v1/course/public', require('./routes/public/course'))

app.use('/api/v1/course/instructor', require('./routes/instructor/course'))
app.use('/api/v1/statistics', require('./routes/statistics'))
app.use('/api/v1/course/instructor/:courseId/section', injectCourseId, require('./routes/instructor/section'))
app.use('/api/v1/course/instructor/:courseId/section/:sectionId/page', injectCourseId, injectSectionId, require('./routes/instructor/page'))


app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public','index.html'));
});

module.exports = app
