const mongoose = require('mongoose')
const toJSON = require('./toJSON')

var UserCourseSchema = new mongoose.Schema(
    {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users',
            required: true
        },

        createdAt: {
            type: Date,
            required: true,
            default: Date.now
        },

        course: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Courses',
            required: true
        },

        currentPage: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Pages',
            required: false
        },

        progress: {
            type: Number,
            required: true,
            default: 0
        },

        lastAccessAt: {
            type: Date,
            required: false
        },

        pagesDone: {
            type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Pages' }],
            required: false
        },

        sectionsDone: {
            type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Course.sections' }],
            required: false
        },

    }
)

UserCourseSchema.method('toJSON', toJSON)
module.exports = UserCourseSchema