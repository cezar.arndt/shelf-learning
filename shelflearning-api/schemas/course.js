const mongoose = require('mongoose')
const { COURSE_STATUS } = require('./enum')

var CourseSchema = new mongoose.Schema(
    {
        createdBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users',
            required: true
        },

        createdAt: {
            type: Date,
            required: true,
            default: Date.now
        },

        tags: {
            type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Tags' }],
            validate: v => Array.isArray(v) && v.length > 0,
        },

        title: {
            type: String,
            required: true,
            index: true,
            trim: true
        },
        description: {
            type: String,
            trim: true
        },
        status: {
            type: String,
            enum: Object.values(COURSE_STATUS),
            default: 'DRAFT',
            required: true
        },
        sections: [
            {
                title: {
                    type: String,
                    trim: true,
                    index: true,
                    required: true
                },
                order: {
                    type: Number,
                    required: true
                },
                pages: {
                    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Pages' }],
                },
            }
        ],
        statusHistory: {
            select: false,
            type: [
                {
                    changedBy: {
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'Users',
                        required: true
                    },

                    atDate: {
                        type: Date,
                        required: true,
                        default: Date.now
                    },

                    reason: {
                        type: String,
                        trim: true,
                        required: true
                    },

                    from: {
                        type: String,
                        enum: Object.values(COURSE_STATUS),
                        required: true
                    },

                    to: {
                        type: String,
                        enum: Object.values(COURSE_STATUS),
                        required: true
                    }
                }
            ]
        },
        image: {
            data: {
                type: Buffer
            },
            contentType: {
                type: String
            }
        },
    }
)

function toJSONCourse() {

    const { __v, _id, ...object } = this.toObject()
    object.id = _id

    if (object.sections) {
        object.sections = object.sections.map(section => {
            const { _id, ...object } = section
            object.id = _id

            if (object.pages) {
                object.pages = object.pages.map(page => {
                    const { __v, _id, ...object } = page
                    object.id = _id
                    return object
                })
            }

            return object
        })
    }

    if (object.tags) {
        object.tags = object.tags.map(tags => {
            const { __v, _id, ...object } = tags
            return object.name
        })
    }

    if (object.createdBy) {
        const { __v, _id, ...user } = object.createdBy
        user.id = _id
        object.createdBy = user
    }


    return object
}

function organize() {
    this.sections.sort((a, b) => (a.order < b.order ? -1 : 1))

    this.sections.forEach(s => {
        s.pages.sort((a, b) => (a.order < b.order ? -1 : 1))
    })

    return this
}

CourseSchema.method('toJSON', toJSONCourse)
CourseSchema.method('organize', organize)
module.exports = CourseSchema