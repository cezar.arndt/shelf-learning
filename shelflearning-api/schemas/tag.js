const mongoose = require('mongoose')
const toJSON = require('./toJSON')

var TagSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            index: true,
            trim: true,
            unique: true
        }
    }
)

TagSchema.method('toJSON', toJSON)
module.exports = TagSchema