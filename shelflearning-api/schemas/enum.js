const COURSE_STATUS = {
    DRAFT:'DRAFT',
    PUBLISHED: 'PUBLISHED', 
    CLOSED: 'CLOSED', 
    BLOCKED: 'BLOCKED'
}

const USER_TYPE = {
    ADMIN:'ADMIN',
    USER: 'USER', 
}

module.exports = { COURSE_STATUS, USER_TYPE }