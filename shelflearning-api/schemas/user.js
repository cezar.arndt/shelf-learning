const mongoose = require('mongoose')
const { USER_TYPE } = require('./enum')
const toJSON = require('./toJSON')

var UserSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: false,
            index: false,
            trim: true
        },
        username: {
            type: String,
            required: true,
            index: true,
            trim: true,
            unique: true      
        },
        password: {
            type: String,
            required: false,
            select: false
        },
        disabledAt: { 
            type: Date,
            required: false,
            default: Date.now()
        },
        type: {
            type: String,
            enum: Object.values(USER_TYPE),
            required: true,
            default:USER_TYPE.USER
        },
        image: {
            data: {
                type: Buffer
            },
            contentType: {
                type: String
            }
        },
        isActive:{
            type:Boolean,
            default: false,
            select: false
        },
        validationToken:{
            type: String,
            select: false
        },
        tokenValidationTime:{
            type: Date,
            select: false
        },
        externalProviderToken: {
            type:String,
            required: false,
            select: false
        }
    }
)

UserSchema.method('toJSON', toJSON)
module.exports = UserSchema