const mongoose = require('mongoose')
const CourseSchema = require('./course')
const PageSchema = require('./page')
const TagSchema = require('./tag')
const UserSchema = require('./user')
const UserCourseSchema = require('./userCourse')

var conn = mongoose.createConnection(process.env.MONGO_URL, { useFindAndModify: false, useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true, })

const User = conn.model('Users', UserSchema)
const Tag = conn.model('Tags', TagSchema)
const Course = conn.model('Courses', CourseSchema)
const Page = conn.model('Pages', PageSchema)
const UserCourse = conn.model('UserCourses', UserCourseSchema)


module.exports = {
    User,
    Tag,
    Course,
    Page,
    UserCourse
}
