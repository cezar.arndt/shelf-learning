const mongoose = require('mongoose')
const toJSON = require('./toJSON')

var PageSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
            index: true,
            trim: true
        },
        description: {
            type: String,
            trim: true,
            trim: true
        },
        order: {
            type: Number,
            required: true
        },
        course: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Courses',
            required: true
        },
        descriptionLength: {
            type: Number,
            default: 0
        }
    }
)

PageSchema.method('toJSON', toJSON)
module.exports = PageSchema