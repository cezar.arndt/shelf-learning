import { Pane, Heading, TextInputField, Button, Spinner, toaster, Text } from "evergreen-ui";
import React, { useContext, useEffect, useState } from "react";

import '../Login/styles.css';
import { AuthContext } from "../../context/auth";
import history from "../../history";
import { useLocation } from "react-router-dom";
import SendActivationCodeDialog from "../../components/SendActivationCodeDialog";

import {
    validatePassword,
    validateConfirm
} from '../CreateAccount/validateFields'

export default function ResetPassword(props: any) {

    const {
        handleResetPassword
    } = useContext(AuthContext)

    const [isLoading, setLoading] = useState<boolean>(false)
    const [isCodeInvalid, setCodeInvalid] = useState<boolean>(false)
    const [errors, setErrors] = useState<any>({})
    const [touched, setTouched] = useState<any>({})
    const [isResendingCode, setResendingCode] = useState<boolean>(false)
    const [account, setAccount] = useState<any>({ password: null, confirm: null })

    const params = useLocation<any>()

    useEffect(() => {
        setErrors(validate())
    }, [account.password, account.confirm])

    const submitResetPassword = async () => {

        if (Object.values(errors).every(e => e === false)) {
            setLoading(true)

            try {
                const query = new URLSearchParams(params.search);
                await handleResetPassword(account, query.get("code"))
                setAccount({ password: null, confirm: null })
                history.push('/')
                toaster.success(
                    "Senha atualizada",
                )
            } catch (error) {
                if (error.code === 'A11') {
                    setCodeInvalid(true)
                }
                toaster.danger(
                    error.message,
                )
            } finally {
                setLoading(false)
            }

        }
    }

    const goBackLogin = () => {
        history.push('/')
    }

    const disabledResetButton = () => {
        const { password, confirm } = account
        return isLoading || !password || !confirm || password !== confirm
    }

    const handleChange = (evt: any) => {
        const { name, value: newValue } = evt.target;
        setAccount({
            ...account,
            [name]: newValue
        })

        setTouched({
            ...touched,
            [name]: true
        })
    }

    const validations = {
        password: validatePassword,
        confirm: validateConfirm
    }

    const validate = () => {
        const errors = {}
        for (var i in validations) {
            //@ts-ignore
            errors[i] = validations[i](account)
        }
        return errors
    }

    return (
        <Pane
            className="Login resetPassword"
            style={isCodeInvalid ? {height:600} : {}}
            elevation={1}
            float="left"
            display="flex"
            flexDirection="column"
        >

            <section>
                <Heading size={800}>Shelf learning</Heading>
                <Heading size={500}>Redefinir senha</Heading>
            </section>


            { isCodeInvalid &&
                <div className="box" style={{
                    textAlign: 'center',
                    margin: '8px 0 32px',
                }}>
                    <Text>Você pode enviar um novo código para o seu e-mail para redefinir sua senha.</Text>
                    <br />
                    <Button disabled={isLoading} onClick={() => setResendingCode(true)} marginTop={16} appearance="minimal">Enviar um novo código</Button>
                </div>
            }

            <section>

                <TextInputField name="password"
                    autoFocus={true}
                    label="Nova senha"
                    type="password"
                    disabled={isLoading}
                    onChange={handleChange}
                    isInvalid={touched.password && errors.password}
                    validationMessage={touched.password && errors.password}
                />


                <TextInputField
                    name="confirm"
                    label="Confirmação da nova senha"
                    type="password"
                    disabled={isLoading}
                    onChange={handleChange}
                    isInvalid={touched.confirm && errors.confirm}
                    validationMessage={touched.confirm && errors.confirm}
                />

                <div className="box">
                    <div></div>
                    <div>
                        <Spinner hidden={!isLoading} size={32} marginRight={16} />
                        <Button height={40} appearance="primary" disabled={disabledResetButton()} intent="success" onClick={submitResetPassword}>
                            {isLoading ? 'redefinindo senha' : 'Redefinir senha'}
                        </Button>
                    </div>
                    <div></div>
                </div>

                <div className="box">
                    <div></div>
                    <div>
                        <Button disabled={isLoading} onClick={goBackLogin} appearance="minimal">Voltar para o login</Button>
                    </div>
                    <div></div>
                </div>

            </section>

            <SendActivationCodeDialog password  isVisible={isResendingCode} onClose={() => { setResendingCode(false);}} />

        </Pane>
    )

}