import { Button, Heading, IconButton, ArrowLeftIcon, SideSheet, Menu, Spinner } from "evergreen-ui";
import React, { useContext, useEffect, useRef, useState } from "react";
import ReactQuill, { Quill } from "react-quill";
import 'react-quill/dist/quill.snow.css';
import Main from "../../../components/Main";
import './styles.css';
import { Link, useParams } from "react-router-dom";
import { TeachContext } from "../../../context/teach";

//@ts-ignore
import ImageCompress from 'quill-image-compress';
//@ts-ignore
import QuillResize from 'quill-resize-module';

import { COURSE_STATUS, Page, Section } from "../../../api/interfaces";

export default function PageForm() {

  const { course, isLoadingCourse, courseId, findSectionByPage, loadPageHandler, updateDescriptionHandler } = useContext(TeachContext)


  const [html, setHtml] = useState<string>('')
  const [currentSection, setCurrentSection] = useState<string>()
  const [isPageMenuShown, setPageMenuShown] = useState<boolean>(false)
  const [isSavingPage, setSavinPage] = useState<boolean>(false)
  const [isLoadingPage, setLoading] = useState<boolean>(true)
  const [pageName, setPageName] = useState<string>()


  const [isHeaderVisible, _setHeaderVisible] = useState<boolean>(true)
  const isVisibleRef = useRef<boolean>(isHeaderVisible)
  const timeoutToSaveRef = useRef<any>()

  const headingRef = useRef<HTMLDivElement>(null)

  const setHeaderVisible = (data: boolean) => {
    isVisibleRef.current = data
    _setHeaderVisible(data)
  }

  const params = useParams<any>()
  let intervalRef: any = null

  useEffect(() => {

    (async () => {
      if (params.id) {
        setPageMenuShown(false)

        const section = findSectionByPage(params.id)
        setCurrentSection(section ? section.title : null)

        setLoading(true)
        const page = await loadPageHandler(params.id)

        if (page) {
          setHtml(page.description || '')
          setPageName(page.title)
        }

        setLoading(false)

      }

    })()

  }, [course, params.id])


  useEffect(() => {
    intervalRef = setInterval(scrollHandler, 500)

    return () => {
      clearInterval(intervalRef)
      clearTimeout(timeoutToSaveRef.current)
    }
  }, [])


  const triggerSave = (content: any) => {

    if( course.status === COURSE_STATUS.PUBLISHED || course.status === COURSE_STATUS.CLOSED ) {
      return
    }

    setHtml(content)

    if (timeoutToSaveRef.current) {
      clearTimeout(timeoutToSaveRef.current)
    }

    timeoutToSaveRef.current = setTimeout(() => {

      updateDescription()

    }, 10000)
  }


  const scrollHandler = (e: Event) => {
    const rect = headingRef.current?.getBoundingClientRect()
    if (rect) {

      if (rect.top > 0 && isVisibleRef.current === false) {
        setHeaderVisible(true)
      } else if (rect.top <= 0 && isVisibleRef.current === true) {
        setHeaderVisible(false)
      }
    }

  }

  async function updateDescription() {
    setSavinPage(true)
    await updateDescriptionHandler(params.id, html)
    setSavinPage(false)

    if (timeoutToSaveRef.current) {
      clearTimeout(timeoutToSaveRef.current)
    }
  }

  function canEdit() {
    return course?.status !== COURSE_STATUS.PUBLISHED && course?.status !== COURSE_STATUS.CLOSED
  }

  Quill.register('modules/imageCompress', ImageCompress)
  Quill.register('modules/resize', QuillResize)

  const modules = {
    resize: {
      modules: ['Resize', 'DisplaySize']
    },
    imageCompress: {
      debug: false
    },
    toolbar: [
      [{ 'header': '1' }, { 'header': '2' }],
      [{ size: [] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [{ 'list': 'ordered' }, { 'list': 'bullet' },
      { 'indent': '-1' }, { 'indent': '+1' }],
      ['link', 'image', 'video'],
      ['clean'],
      ['code-block']
    ],
    clipboard: {
      matchVisual: false,
    }
  }

  const formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image', 'video', 'code-block'
  ]

  return (
    <Main>
      <section className={!isHeaderVisible ? 'PageForm PageForm-FixeTools' : 'PageForm'}>

        {isLoadingPage || isLoadingCourse ?
          <section className="PageForm-loading">
            <Spinner size={64} />
            <Heading size={800}>Carregando página</Heading>
          </section>
          :

          <>
            <section>
              <div >

                <SideSheet
                  width={340}
                  isShown={isPageMenuShown}
                  onCloseComplete={() => setPageMenuShown(false)}
                >

                  <Menu>

                    {course?.sections.map((e: Section, index: number) =>
                      <Menu.Group title={e.title} key={index}>
                        {e.pages.length ? e.pages.map((ee: Page, key: number) =>
                          <Menu.Item
                          style={{
                            height: 'auto',
                            padding: '9px 0',
                            fontWeight: (params.id === ee.id ? 'bold' : 'normal')
                          }}
                          key={key} 
                          is={Link} 
                          to={`/teach/course/${courseId}/page/${ee.id}`} >{ee.title} {ee.descriptionLength === 0 ? '(Vazio)' : ''}</Menu.Item>
                        ) : <Menu.Item>Nenhuma página</Menu.Item>}
                      </Menu.Group>
                    )}

                  </Menu>

                </SideSheet>

                <Spinner hidden={!isSavingPage} size={32} marginRight={16} />

                <Button disabled={isSavingPage} onClick={() => setPageMenuShown(true)}>
                  {canEdit() ? `Editar outra página` : `Visualizar outra página`}
                </Button>

                <Button disabled={isSavingPage || !canEdit()} onClick={updateDescription} appearance="primary">Salvar</Button>
              </div>

              <div>
                <Heading size={600}>{course?.title}</Heading>
              </div>
              <Heading size={600}>{currentSection}</Heading>

              <div ref={headingRef}>
                <IconButton icon={ArrowLeftIcon} is={Link} to={`/teach/course/${courseId}/sections-n-pages`} />
                <Heading size={800}>{pageName}</Heading>
              </div>
            </section>

            <ReactQuill
              readOnly={!canEdit()}
              theme="snow"
              onChange={(content: any) => triggerSave(content)}
              value={html}
              modules={modules}
              formats={formats}
              bounds={'.app'}
              placeholder="Escreva o conteudo da página..."
            />
          </>
        }
      </section>
    </Main>
  );
}