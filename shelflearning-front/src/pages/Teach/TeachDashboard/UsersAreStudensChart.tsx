import { Heading, toaster } from 'evergreen-ui'
import React, { useEffect, useState } from 'react'
import { Pie } from 'react-chartjs-2'
import * as statisticsAPI from "../../../api/statisticsAPI";


export default function UsersAreStudensChart() {

    const [dataset, setDataset] = useState<any>({})

    useEffect(() => {
        (async () => {
            try {
                const data = await statisticsAPI.userAndStudents()

                setDataset({
                    labels: [
                        'Usuários não alunos',
                        'Alunos',
                    ],
                    datasets: [{
                        data: [data.users, data.students],
                        backgroundColor: [
                            '#425A70',
                            '#1070CA',
                        ],
                        hoverBackgroundColor: [
                            '#234361',
                            '#084B8A',
                        ],
                    }]
                })

            } catch (error) {
                toaster.danger(
                    error.message,
                )
            }
        })()
    }, [])


    return (
        <>
            <Heading size={400}>Usuários que são alunos</Heading>
            <Pie data={dataset} />
        </>
    )
}
