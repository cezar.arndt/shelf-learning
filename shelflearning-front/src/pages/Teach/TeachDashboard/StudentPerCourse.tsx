import { Heading, toaster } from 'evergreen-ui'
import React, { useEffect, useState } from 'react'
import { Bar } from 'react-chartjs-2'
import * as statisticsAPI from "../../../api/statisticsAPI";

function setColor(color: string, alpha: number) {
    return color.replace(/A/, alpha.toString())
}

function getColor() {
    function random_rgba() {
        var o = Math.floor,
            r = Math.random,
            s = 255;
        return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',A)';
    }

    const color = random_rgba()

    return [
        setColor(color, 0.2),
        setColor(color, 0.4),
        setColor(color, 1)
    ]
}

export default function StudentPerCourse() {

    const [dataset, setDataset] = useState<any>({})

    useEffect(() => {
        (async () => {


            try {
                const data = await statisticsAPI.studentsPerCourse()

                setDataset({
                    labels: Object.keys(data),
                    datasets: Object.values(data).map((e, key, arr) => {
                        const data = []
                        data[key] = e
                        const color = getColor()

                        return {
                            backgroundColor: color[0],
                            borderColor: color[2],
                            borderWidth: 1,
                            hoverBackgroundColor: color[1],
                            hoverBorderColor: color[2],
                            data,
                        }

                    })
                })


            } catch (error) {
                toaster.danger(
                    error.message,
                )
            }

        })()
    }, [])


    return (
        <>
            <Heading size={400}>Alunos por curso</Heading>
            <Bar data={dataset} options={
                {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0,
                                stepSize: 1
                            }
                        }],
                        xAxes: [{
                            stacked: true,
                            ticks: {
                                callback: (value: string) => value.length > 10 ? `${value.substr(0, 10)}...` : value
                            }
                        }]
                    },
                }
            } />
        </>
    )
}