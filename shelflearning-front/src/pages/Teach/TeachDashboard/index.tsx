import React from 'react'
import { Heading } from 'evergreen-ui'

import Main from '../../../components/Main'

import './styles.css';
import CoursePerStatusChart from './CoursePerStatusChart';
import UsersAreStudensChart from './UsersAreStudensChart';
import StudentPerCourse from './StudentPerCourse';
import CourseDoneProgress from './CourseDoneProgress';
import EnrollPerMonth from './EnrollPerMonth';

export default function TeachDashboard() {

    return (
        <Main className="Main-Dashboard">
            <section className="TeachDashboard">
                <Heading size={600} marginTop="default">Estatísticas</Heading>

                <div>
                    <div>
                        <CoursePerStatusChart />
                    </div>
                    <div>
                        <UsersAreStudensChart />
                    </div>
                    <div>
                        <StudentPerCourse />
                    </div>
                    <div>
                        <EnrollPerMonth />
                    </div>
                    <div>
                        <CourseDoneProgress />
                    </div>
                </div>

            </section>
        </Main>
    )
}
