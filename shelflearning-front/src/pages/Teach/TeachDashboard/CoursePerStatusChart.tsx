import { Heading, toaster } from 'evergreen-ui'
import React, { useEffect, useState } from 'react'
import { Bar } from 'react-chartjs-2'
import { COURSE_STATUS } from '../../../api/interfaces'
import * as statisticsAPI from "../../../api/statisticsAPI";


export default function CoursePerStatusChart() {

    const [dataset, setDataset] = useState<any>({})

    useEffect(() => {
        (async () => {
            try {
                const data = await statisticsAPI.coursePerStatus()
                setDataset({

                    labels: ["DRAFT", "PUBLISHIED", "BLOCKED", "CLOSED"],
                    datasets: [
                        {
                            label: "Rascunho",
                            backgroundColor: "rgba(66, 90, 112,0.2)",
                            borderColor: "rgba(35, 67, 97,1)",
                            borderWidth: 1,
                            hoverBackgroundColor: "rgba(66, 90, 112,0.4)",
                            hoverBorderColor: "rgba(35, 67, 97,1)",
                            data: [data[COURSE_STATUS.DRAFT],0,0,0],
                        },
                        {
                            label: "Publicado",
                            backgroundColor: "rgba(71, 184, 129, 0.2)",
                            borderColor: "rgba(71, 184, 129, 1)",
                            borderWidth: 1,
                            hoverBackgroundColor: "rgba(71, 184, 129, 0.4)",
                            hoverBorderColor: "rgba(71, 184, 129, 1)",
                            data: [0,data[COURSE_STATUS.PUBLISHED],0,0],
                        },
                        {
                            label: "Bloqueado",
                            backgroundColor: "rgba(236, 76, 71,0.2)",
                            borderColor: "rgba(236, 76, 71,1)",
                            borderWidth: 1,
                            hoverBackgroundColor: "rgba(236, 76, 71,0.4)",
                            hoverBorderColor: "rgba(236, 76, 71,1)",
                            data: [0,0,data[COURSE_STATUS.BLOCKED],0],
                        },
                        {
                            label: "Fechado",
                            backgroundColor: "rgba(20, 181, 208, 0.2)",
                            borderColor: "rgba(20, 181, 208, 1)",
                            borderWidth: 1,
                            hoverBackgroundColor: "rgba(20, 181, 208, 0.4)",
                            hoverBorderColor: "rgba(20, 181, 208, 1)",
                            data: [0,0,0,data[COURSE_STATUS.CLOSED]],
                        }
                    ]
                })
            } catch (error) {
                toaster.danger(
                    error.message,
                )
            }
        })()
    }, [])


    return (
        <>
            <Heading size={400}>Curso por status</Heading>
            <Bar data={dataset} options={
                {
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            ticks: {
                                min: 0,
                                stepSize: 1
                            }
                        }]
                    }
                }
            } />
        </>
    )
}