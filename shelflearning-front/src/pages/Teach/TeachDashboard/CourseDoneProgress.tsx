import { Heading, toaster } from 'evergreen-ui'
import React, { useEffect, useState } from 'react'
import { Bar } from 'react-chartjs-2'
import * as statisticsAPI from "../../../api/statisticsAPI";

export default function CourseDoneProgress() {

    const [dataset, setDataset] = useState<any>({})

    useEffect(() => {
        (async () => {
            try {
                const data = await statisticsAPI.doneRatePerCourse()
                setDataset(
                    {
                        labels: Object.keys(data),
                        datasets: [
                            {
                                label: 'Alunos que finalizaram',
                                data: Object.values(data).map((e: any) => e[0]),
                                backgroundColor: '#1070CA',
                            },
                            {
                                label: 'Alunos matriculados',
                                data: Object.values(data).map((e: any) => e[1]),
                                backgroundColor: 'rgba(16, 112, 202, 0.3)',
                            },
                        ],
                    }
                )
            } catch (error) {
                toaster.danger(
                    error.message,
                )
            }
        })()
    }, [])


    return (
        <>
            <Heading size={400}>Conclusão por curso</Heading>
            <Bar data={dataset} options={
                {
                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0,
                                stepSize: 1,
                                stacked: true,
                            }
                        }],
                        xAxes: [{
                            stacked: true,
                            ticks: {
                                callback: (value: string) => value.length > 10 ? `${value.substr(0, 10)}...` : value
                            }
                        }]
                    },
                }
            } />
        </>
    )
}