import { Heading, toaster } from 'evergreen-ui'
import React, { useEffect, useState } from 'react'
import { Line } from 'react-chartjs-2'
import * as statisticsAPI from "../../../api/statisticsAPI";



function getMonth(timestamp: number) {

    var names = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
    ];

    let date = new Date(timestamp)

    return `${names[date.getMonth()].substr(0, 3)} (${date.getFullYear().toString().substr(2, 2)})`
}

function setColor(color: string, alpha: number) {
    return color.replace(/A/, alpha.toString())
}

function getColor() {
    function random_rgba() {
        var o = Math.floor,
            r = Math.random,
            s = 255;
        return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',A)';
    }

    const color = random_rgba()

    return [
        setColor(color, 0.2),
        setColor(color, 0.4),
        setColor(color, 1)
    ]
}

export default function EnrollPerMonth() {

    const [dataset, setDataset] = useState<any>({})

    useEffect(() => {
        (async () => {

            try {
                const [xAxis, data] = await statisticsAPI.enrollPerMonth()

                setDataset(
                    {
                        labels: xAxis.map((m: number) => getMonth(m)),
                        datasets: Object.keys(data).map((course: any) => {

                            const color = getColor()
                            return {
                                label: course,
                                lineTension: 0,
                                fill: false,
                                backgroundColor: color[1],
                                borderColor: color[2],
                                borderCapStyle: 'butt',
                                borderDash: [],
                                borderDashOffset: 0.0,
                                borderJoinStyle: 'miter',
                                pointBorderColor: color[2],
                                pointBackgroundColor: '#fff',
                                borderWidth: 1,
                                pointBorderWidth: 1,
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: color[2],
                                pointHoverBorderColor: color[2],
                                pointHoverBorderWidth: 2,
                                pointRadius: 1,
                                pointHitRadius: 10,
                                data: Object.values(data[course])
                            }
                        })
                    }
                )
            } catch (error) {
                toaster.danger(
                    error.message,
                )
            }

        })()
    }, [])


    return (
        <>
            <Heading size={400}>Inscrições por mês</Heading>
            <Line data={dataset} options={{
                legend: {
                    display: false
                },
                yAxes: [{
                    ticks: {
                        min: 0,
                        stepSize: 1
                    }
                }],
            }} />
        </>
    )
}