import { Button, Text, Spinner, Heading, IconButton, ArrowLeftIcon } from "evergreen-ui";
import React, { useContext,  useState } from "react";
import { Link } from "react-router-dom";
import { COURSE_STATUS } from "../../../../api/interfaces";


import Main from "../../../../components/Main";
import Stepts from "../../../../components/Steps";
import { TeachContext } from "../../../../context/teach";
import history from "../../../../history";

import './styles.css';

export default function CourseFormPublish() {

    const { course, courseId, isLoadingCourse, publishCourseHandler, isPublishingCourse } = useContext(TeachContext)

    const [intent, setIntent] = useState<boolean>(false)

    async function publishCourse() {
        await publishCourseHandler()
    }

    function intentToPublish() {
        setIntent(!intent)
    }

    function backToList() {
        history.push('/teach')
    }



    return (
        <Main>

            <Link className="Form-Back" to={`/teach/course/${courseId}/sections-n-pages`}>
                <IconButton icon={ArrowLeftIcon} />
                <span>Voltar para seções e páginas</span>
            </Link>

            {course ?
                <div className="CourseForm-Status" style={{ marginTop: 26 }}>
                    <Heading size={600}>{course.title}</Heading>
                </div>
                : null}

            <div className="CourseForm-Status">
                <Spinner hidden={!isLoadingCourse} size={32} marginRight={16} />
                {!isLoadingCourse ? <Text>Status: {course ? course.status : null}</Text> : <Text>&nbsp;</Text>}
            </div>

            <Stepts steps={3} done={3} texts={['Básico', 'Seções e páginas', 'Publicação']} />

            <div className="CourseFormPublish Form">

                {course.status !== COURSE_STATUS.DRAFT ?

                    <>
                        <Text>
                            Não há mais nada a fazer aqui.<br />
                        Este curso já passou pelo processo de publicação.</Text>

                        <Button onClick={backToList} disabled={intent} className="Form-SaveButton" appearance="primary" marginTop={36}>Voltar para a listagem dos meus cursos</Button>
                    </>

                    : <>
                        <Button onClick={intentToPublish} disabled={intent} className="Form-SaveButton" appearance="primary" marginTop={36}>Publicar curso</Button>

                        {intent ?
                            <>

                                <Text>
                                    Após publicado, o status do seu curso mudará de "Rascunho" para "Publicado", e ficará visível para todos.<br />
                                     Uma vez publicado você não pode mais move-lo para Rascunho. A edição das seções e páginas também ficam bloqueadas.</Text>

                                <Text>
                                    Se algum adminstrador entender que seu curso possui algum conteúdo improprio ou equivocado, ele pode bloquear o seu curso informando o motivo.
                                    No caso de bloqueio o seu curso volta a ficar disponível para edição PARCIAL.<br/>
                                    <strong>Criação/exclusão/Ordenação de Seções e Páginas não são permitidas</strong>    
                                </Text>

                                <Text>
                                    Se você entender que o conteúdo do curso esta defasado, é possível atualizar o status para "Fechado", nesse status
                                    ninguém mais pode se inscrever, porém quem esta matriculado ainda tem a chance de finalizá-lo</Text>
                                <div>

                                    <Spinner hidden={!isPublishingCourse} size={32} />

                                    <Button onClick={intentToPublish} disabled={isPublishingCourse} className="Form-SaveButton" appearance="default">Ainda não tenho certeza</Button>
                                    <Button onClick={publishCourse} disabled={isPublishingCourse} className="Form-SaveButton" appearance="primary" intent={"warning"}>Publicar curso</Button>

                                </div>

                            </>
                            : null}

                    </>}

            </div>
        </Main >
    );
}