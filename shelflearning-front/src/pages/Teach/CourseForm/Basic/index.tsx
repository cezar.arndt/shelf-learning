import { Alert, Button, FilePicker, Textarea, TextInput, Text, Spinner, IconButton, ArrowLeftIcon } from "evergreen-ui";
import React, { useContext, useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
//@ts-ignore
import ReactTags from 'react-tag-autocomplete'
import { Course, COURSE_STATUS, Tag } from "../../../../api/interfaces";

import Main from "../../../../components/Main";
import Stepts from "../../../../components/Steps";
import { TeachContext } from "../../../../context/teach";
import useImage from "../../../../hooks/useImage";
import InputDialog from "../../../../components/InputDialog";

import './styles.css';
import BlockedCourseAlert from "../../../../components/BlockedCourseAlert";
import ConfirmDialog from "../../../../components/ConfirmDialog";
import history from "../../../../history";

export default function CourseFormBasic() {

    const { course, isLoadingCourse,
         courseId,
         saveCourseHandler,
         isSavingCourse,
         closeCourse,
         removeCourseHandler,
         listTagsHandler,
         allTags } = useContext(TeachContext)
    const { image, uploadImage, loadImage, removeImage } = useImage()

    const titleRef = useRef<HTMLInputElement | null>(null)
    const descriptionRef = useRef<HTMLTextAreaElement | null>(null)
    const reactTagsRef = useRef<any>(null)


    const [tags, setTags] = useState<Tag[]>([])
    const [suggestions, setSuggestions] = useState<Tag[]>([])
    const [closingCourse, setClosingCourse] = useState<boolean>(false)
    const [isClosingCourse, setIsclosingCourse] = useState<boolean>(false)



    const [deletingCourse, setDeletinCourse] = useState<any>()

    useEffect(() => {
        if (course) {
            if (titleRef.current) titleRef.current.value = course.title
            if (descriptionRef.current) descriptionRef.current.value = course.description || ""
            setTags([...course.tags.map((e: any) => ({ name: e }))])

            if (course.image) {
                loadImage(course.image)
            }
        }
    }, [course])

    useEffect(() => {
        listTagsHandler()
    }, [])

    useEffect(() => { 
        const list = allTags.filter( (e:Tag) => {
            return tags.map(e=>e.name).indexOf(e.name) === -1
        })
        setSuggestions([...list])

    }, [allTags, tags])


    const onTagDelete = (i: Number) => {
        const currentTags = tags.slice(0)
        //@ts-ignore
        currentTags.splice(i, 1)
        setTags([...currentTags])
    }

    const onTagAddition = (tag: Tag) => {

        if( tags.map(e=>e.name.toLowerCase()).indexOf(tag.name.toLowerCase()) === -1 )
        {
            setTags([...tags, tag])
        }
    }


    async function saveCourse() {
        if (titleRef.current && descriptionRef.current && tags.length) {
            const courseToSave: Course = {
                title: titleRef.current.value,
                description: descriptionRef.current.value,
                tags: tags.map((e: Tag) => e.name)
            }

            if (image) {
                courseToSave.image = image
            }
            await saveCourseHandler(courseToSave)
        }
    }

    async function updateToClose(reason: string) {
        setIsclosingCourse(true)
        setClosingCourse(false)
        await closeCourse(reason)
        setIsclosingCourse(false)
    }

    function canEdit() {
        return !isSavingCourse && !isLoadingCourse && course?.status !== COURSE_STATUS.PUBLISHED && course?.status !== COURSE_STATUS.CLOSED
    }

    async function removeCourse() {
        await removeCourseHandler(courseId)
        setDeletinCourse(undefined)
        history.replace('/teach')
    }

    function renderCloseAction() {

        if (course && [COURSE_STATUS.PUBLISHED].indexOf(course.status) > -1) {
            return (
                <section className={"Form"}>
                    <Alert
                        style={{ maxWidth: 600 }}
                        intent="none"
                        title="Esse curso pode ser fechado"
                        marginTop={32}
                        marginBottom={32}>

                        <Text>
                            Se você entender que o conteúdo do curso esta defasado, é possível atualizar o status para "Fechado", nesse status
                        ninguém mais pode se inscrever, porém quem esta matriculado ainda tem a chance de finalizá-lo</Text>
                        <br />

                        {
                            isClosingCourse ?
                                <Spinner size={32} marginTop={16} marginBottom={8} /> :
                                <Button marginTop={16} marginBottom={8} appearance="primary" onClick={() => setClosingCourse(true)} intent="warning">Fechar o curso</Button>
                        }

                    </Alert>
                </section>
            )
        }
    }

    function renderRemoveCourse() {
        if (course && [COURSE_STATUS.DRAFT].indexOf(course.status) > -1) {
            return (
                <section className={"Form"}>
                    <Alert
                        style={{ maxWidth: 600 }}
                        intent="none"
                        title="Esse curso pode ser deletado"
                        marginTop={32}
                        marginBottom={32}>

                        <Text>
                            Enquanto esse curso estiver em "Rascunho" ele pode ser deletado, <strong>todo o conteudo sera perdido</strong></Text>
                        <br />

                        <Button marginTop={16} marginBottom={8} appearance="primary" onClick={() => setDeletinCourse(courseId)} intent="danger">Deletar o curso</Button>

                    </Alert>
                </section>
            )
        }
    }

    function renderWarningDuplicateCourse() {
        if (!course || [COURSE_STATUS.DRAFT, COURSE_STATUS.BLOCKED].indexOf(course.status) > -1) {
            return (
                <Alert
                    intent="none"
                    title="Certifique-se de já não existe um curso com
                    conteúdo igual."
                    marginTop={32}
                />
            )
        }
    }

    function renderBlockedStatus() {
        if (course && COURSE_STATUS.BLOCKED === course.status) {
            return <BlockedCourseAlert statusHistory={course.statusHistory[0]} />
        }
    }

    function renderSteps() {
        return <Stepts steps={3} done={2} texts={['Básico', 'Seções e páginas', 'Publicação']} />
    }


    return (
        <Main>

            <Link className="Form-Back" to={`/teach`}>
                <IconButton icon={ArrowLeftIcon} />
                <span>Voltar para a listagem</span>
            </Link>

            <div className="CourseForm-Status">
                <Spinner hidden={!isLoadingCourse} size={32} marginRight={16} />
                {!isLoadingCourse ? <Text>Status: {course ? course.status : 'DRAFT'}</Text> : <Text>&nbsp;</Text>}
            </div>

            {renderRemoveCourse()}
            {renderCloseAction()}
            {renderBlockedStatus()}
            {renderSteps()}

            <div className="CourseFormBasic Form">
                <TextInput name="title"
                    disabled={!canEdit()}
                    autoFocus
                    placeholder="Título do curso"
                    ref={titleRef}
                    autoComplete="off"
                />

                <div style={{
                    display: 'flex',
                    marginTop: 32,
                    marginBottom: 8
                }}>
                    <ReactTags
                        ref={reactTagsRef}
                        tags={tags}
                        allowNew
                        allowBackspace
                        suggestions={suggestions}
                        placeholderText="Tags"
                        onDelete={onTagDelete}
                        onAddition={onTagAddition} />
                </div>

                <Textarea
                    disabled={!canEdit()}
                    name="description"
                    placeholder="Descrição"
                    ref={descriptionRef}
                />

                <FilePicker
                    disabled={!canEdit()}
                    className="Form-DivInput"
                    multiple
                    marginBottom={32}
                    onChange={files => uploadImage(files)}
                    placeholder="Selecione a imagem do curso"
                />

                {image ?
                    <>
                        <img alt="" src={image} style={{ maxWidth: '100%', alignSelf: 'center', marginTop: 32 }} />

                        {!course || [COURSE_STATUS.DRAFT, COURSE_STATUS.BLOCKED].indexOf(course.status) > -1 &&
                            <Button
                                onClick={() => removeImage()}
                                disabled={isLoadingCourse || isSavingCourse}
                                intent={"danger"}
                                style={{
                                    maxWidth: 128,
                                    margin: '16px auto'
                                }}
                                appearance="primary">Remover imagem</Button>}
                    </>
                    : null}

                {renderWarningDuplicateCourse()}

                <div style={{ marginTop: 32 }}>

                    {courseId ?
                        <>
                            <Button className="Form-SaveButton" disabled={!canEdit()} onClick={saveCourse} appearance="primary">Atualizar informações</Button>
                            <Button className="CourseFormBasic-GoToSections" disabled={isSavingCourse || isLoadingCourse} is={Link} to={`/teach/course/${courseId}/sections-n-pages`}>Ir para seções e páginas</Button>
                            <Spinner hidden={!isSavingCourse} size={32} marginRight={16} style={{ right: '51%' }} />
                        </>
                        :
                        <>
                            <Button className="Form-SaveButton" disabled={isSavingCourse || isLoadingCourse} onClick={saveCourse} appearance="primary">Continuar para seções e páginas</Button>
                            <Spinner hidden={!isSavingCourse} size={32} marginRight={16} />
                        </>
                    }

                </div>

            </div>

            <InputDialog
                isVisible={closingCourse}
                onClose={() => setClosingCourse(false)}
                onSubmit={updateToClose}
                intent="danger"
                notes="<p>Atenção, uma vez fechado o curso, ele <strong>não poderá ser republicado</strong></p>"
                submitLabel="Quero fechar o curso"
                title="Fechamento do curso"
                placeHolder="Adicione uma observação ao fechamento (não obrigatório)" />


            <ConfirmDialog
                isVisible={deletingCourse !== undefined}
                title="Remover Curso"
                text="A exclusão não pode ser revertida<br/> você tem certeza que deseja deletar esse curso e <strong>todas as suas seções e páginas</strong> ?"
                onClose={() => setDeletinCourse(undefined)}
                onSubmit={() => { removeCourse() }}
                submitLabel="Sim, deletar curso"
                cancelLabel="Não, mudei de idéia"
                intentButton="danger"
            />

        </Main>
    );
}