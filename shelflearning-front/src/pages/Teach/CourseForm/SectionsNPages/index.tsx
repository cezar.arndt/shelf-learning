import { Button, Text, AddIcon, Heading, Spinner, IconButton, ArrowLeftIcon } from "evergreen-ui";
import React, { useContext, useRef } from "react";
import { Link } from "react-router-dom";
import CourseTreeEdit from "../../../../components/CourseTree/CourseTreeEdit";
import Main from "../../../../components/Main";
import Stepts from "../../../../components/Steps";

import './styles.css';
import { TeachContext } from "../../../../context/teach";
import history from "../../../../history";
import { COURSE_STATUS } from "../../../../api/interfaces";
import BlockedCourseAlert from "../../../../components/BlockedCourseAlert";


export default function CourseFormSectionsNPages() {

    const { course, isLoadingCourse, courseId } = useContext(TeachContext);
    const treeEditRef = useRef<any>()

    const goToPublish = () => {
        history.push(`/teach/course/${courseId}/publish`)
    }

    function canEdit() {
        return !isLoadingCourse && course?.status !== COURSE_STATUS.PUBLISHED && course?.status !== COURSE_STATUS.CLOSED
    }

    function renderBlockedStatus() {
        if (course && COURSE_STATUS.BLOCKED === course.status) {
            return <BlockedCourseAlert statusHistory={course.statusHistory[0]} />
        }
    }

    function renderSteps() {
        return <Stepts steps={3} done={2} texts={['Básico', 'Seções e páginas', 'Publicação']} />
    }

    function renderButtonPublish() {
        if (course && COURSE_STATUS.BLOCKED !== course.status) {
            return (
                <div>
                    <Button className="Form-SaveButton" onClick={goToPublish} appearance="primary">Continuar para publicação</Button>
                </div>
            )
        }
    }

    return (
        <Main>

            <Link className="Form-Back" to={`/teach/course/${courseId}`}>
                <IconButton icon={ArrowLeftIcon} />
                <span>Voltar para o básico</span>
            </Link>

            {course ?
                <div className="CourseForm-Status" style={{ marginTop: 26 }}>
                    <Heading size={600}>{course.title}</Heading>
                </div>
                : null}


            <div className="CourseForm-Status">
                <Spinner hidden={!isLoadingCourse} size={32} marginRight={16} />
                {!isLoadingCourse ? <Text>Status: {course ? course.status : null}</Text> : <Text>&nbsp;</Text>}
            </div>

            {renderBlockedStatus()}
            {renderSteps()}

            <section className="CourseFormSectionsNPages Form">

                <Text style={{ margin: '16px 0' }}>
                    Tente definir primeiro a estrutura do seu curso, adicione o nome de cada seção e suas respectivas páginas.<br /><br />
                    <Text style={course?.status === COURSE_STATUS.BLOCKED ? {textDecoration: 'line-through'} : {}}>Não se preocupe você pode adicionar editar e remover enquanto seu curso estiver em "Rascunho"</Text>
                </Text>

                <Button onClick={() => treeEditRef.current.setAddingSection(true)}
                    className="CourseForm-AddSection"
                    intent="success"
                    height={40}
                    disabled={!canEdit() || course?.status !== COURSE_STATUS.DRAFT}
                    iconBefore={AddIcon}>Adicionar Seção</Button>

                {courseId ? <CourseTreeEdit disabled={!canEdit()} ref={treeEditRef} /> : null}


                {renderButtonPublish()}
            </section>



        </Main>
    )
}