import { Button, Heading, Pane, Spinner, Table, toaster } from "evergreen-ui";
import React, { useEffect, useRef, useState } from "react";
import { Link } from 'react-router-dom';
import { Course, COURSE_STATUS } from "../../api/interfaces";
import * as courseAPI from "../../api/courseAPI";
import Main from "../../components/Main";
import history from "../../history";


import './styles.css';

export default function Teach() {

    const [isLoadingCourses, setLoadingCourses] = useState<boolean>(true)
    const [courses, setCourses] = useState<Course[]>([])
    const [search, setSearch] = useState<string>("")
    const [isExporting, setExporting] = useState<boolean>(false)


    const timeoutRef = useRef<any>()

    useEffect(() => {

        if( timeoutRef.current ) {
            clearTimeout(timeoutRef.current)
        }

        timeoutRef.current = setTimeout( async ()=>{
            try {
                setLoadingCourses(true)
                setCourses([...(await courseAPI.instructors.list(search))])
            } catch ({ error }) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setLoadingCourses(false)
            }
        } ,500)

    }, [search])

    async function exportData() {
        setExporting(true)

        try {
            await courseAPI.instructors.exportData(search)    
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setExporting(false)
        }

    } 


    function renderContent(){

        if( isLoadingCourses ){
            return loadingData()
        } else if( courses.length ) {
            return tableData()
        } else {
            return emptyTable()
        }            
    }

    function loadingData(){
        return <Spinner size={32} marginRight={16} style={{ margin: '20px auto', height: 'auto' }} />
    }

    function tableData(){
        return (<Table.Body height={240}>
            {courses.map(course => (
                <Table.Row key={course.title} isSelectable
                    intent={course.status === COURSE_STATUS.BLOCKED ? 'danger' : 'none'}
                    onSelect={() => history.push(`/teach/course/${course.id}`) }>
                    <Table.TextCell>{course.title}</Table.TextCell>
                    <Table.TextCell>{course.status}</Table.TextCell>
                    <Table.TextCell>{course.studentsCount}</Table.TextCell>
                    <Table.TextCell>{`${course.sectionsCount}/${course.pagesCount}`}</Table.TextCell>
                </Table.Row>
            ))}
        </Table.Body>)
    }

    function emptyTable(){
        return (<Table.Body height={240}>
            <Table.Row>
                <Table.TextCell>{search.length ? `Nenhum curso encontrado` : `Você não tem nenhum curso criado`}</Table.TextCell>
            </Table.Row>
        </Table.Body>)
    }

    return (
        <Main>
            <Pane
                className="Teach-Banner"
                height={120}
                display="flex"
                alignItems="center"
                justifyContent="center"
                border={false}>
                
                <div>
                    <Heading size={600}>Lista de cursos</Heading>
                </div>

                <div style={{display:'flex',alignItems:'center', justifyContent:"flex-end", flex:1}}>
                    <Spinner size={24} marginRight={8} hidden={!isExporting}/>
                    <Button onClick={exportData} disabled={isExporting} size={400}>Exportar cursos</Button>
                    <Button is={Link} to="/teach/new" marginLeft={16} size={400} appearance="primary">Criar curso</Button>
                </div>


            </Pane>

            <Table className="Teach-CourseList">
                <Table.Head>
                    <Table.SearchHeaderCell onChange={value => setSearch(value)} />
                    <Table.TextHeaderCell>
                        Status
                    </Table.TextHeaderCell>
                    <Table.TextHeaderCell>
                        Alunos
                    </Table.TextHeaderCell>
                    <Table.TextHeaderCell>
                        Seções/Páginas
                    </Table.TextHeaderCell>
                </Table.Head>

                {renderContent()}
            </Table>
        </Main>
    );
}