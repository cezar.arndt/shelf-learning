import React, { useContext, useEffect, useState } from 'react'
import * as queryString from 'query-string';
import { externalLogin, PROVIDER } from '../../api/authAPI';
import { AuthContext } from "../../context/auth";
import history from '../../history';
import { ArrowLeftIcon, Button, Heading, Pane, Spinner, Text } from 'evergreen-ui';

import './styles.css'
import authenticationErrorImage from '../../assets/authentication-error.png';



export default function FacebookRedirect() {

    const { setAuthenticated,
        setLocalStorage } = useContext(AuthContext);

    const [error, setError] = useState<string>('')
    const [isLoading, setLoading] = useState<boolean>(false)

    useEffect(() => {
        const urlParams = queryString.parse(window.location.search);

        (async () => {
            if( urlParams.code ){
                setLoading(true)
                try {
                    const {token} = await externalLogin(urlParams.code.toString(), PROVIDER.Facebook)
                    setAuthenticated(true)
                    await setLocalStorage(token)
                    history.push('/')
                } catch (error){
                    setError(error.message)
                } finally {
                    setLoading(false)
                }
            } else if( urlParams.error ) {

                if( urlParams?.error_code?.toString() === '200' ) {
                    setError("Acesso negado")
                } else {
                    setError(urlParams.error.toString())
                }
            }
        })()
    },[])

    const goBack = () => {
        history.push(`/`)
    }

    return (
        <Pane
                className="ExternalProvider"
                elevation={1}
                float="left"
                display="flex"
                flexDirection="column"
            >

                <section>
                <Heading size={400}>Shelf learning</Heading>

                {!isLoading ? 
                <>
                    <Heading size={600}>Ops, ocorreu um problema com a autenticação</Heading>
                    <img src={authenticationErrorImage} alt=""/>
                    <Text style={{color:'red'}}>{error}</Text>

                    <Button height={40} appearance="minimal" onClick={goBack} iconBefore={ArrowLeftIcon}>
                        Voltar para o login
                    </Button>
                </>

                : 
                <>
                    <Heading size={600}></Heading>
                    <img src={authenticationErrorImage}/>
                    <Spinner size={64} />
                    <Text>Realizando a autenticação</Text>
                </>
                }
                    
                   
                </section>
            </Pane>
    )
}
