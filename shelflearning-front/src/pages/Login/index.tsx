import { Pane, Heading, TextInput, Button, Spinner, Text } from "evergreen-ui";
import React, { useContext, useRef, useState } from "react";

import './styles.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookF } from "@fortawesome/free-brands-svg-icons";
import { AuthContext } from "../../context/auth";
import history from "../../history";
import SendActivationCodeDialog from "../../components/SendActivationCodeDialog";
import {urlToLoginWithFacebook} from "../../api/authAPI"


export default function Login(props: any) {

    const referer = props.location.state ? props.location.state.referer : '/';

    const [isLoading, setLoading] = useState<boolean>(false)
    const [isResendingCode, setResendingCode] = useState<boolean>(false)
    const [isResetingPassword, setResetingPassword] = useState<boolean>(false)
    const [isAccountInactive, setAccountInactive] = useState<boolean>(false)
    const usernameRef = useRef<HTMLInputElement>(null)
    const passwordRef = useRef<HTMLInputElement>(null)
    const { handleLogin } = useContext(AuthContext);


    const submitLogin = async () => {
        setLoading(true)

        if (usernameRef.current && passwordRef.current) {
            let code = await handleLogin(usernameRef.current.value, passwordRef.current.value, referer)

            //Account inactive
            if (code === 'A05') {
                setAccountInactive(true)
            }
        }

        setLoading(false)
    }

    const onKeyPress = (e: any) => {
        if (e.key === 'Enter') {
            submitLogin()
        }
    }

    return (
        <Pane
            className="Login"
            elevation={1}
            float="left"
            display="flex"
            flexDirection="column"
        >

            <section>
                <Heading size={800}>Shelf learning</Heading>
                <Heading size={500}>{isAccountInactive ? 'Ativar conta' : 'Fazer login'}</Heading>
            </section>

            { isAccountInactive ?
                <></>
                :
                <section>
                    <Button is="a" href={urlToLoginWithFacebook()} 
                        iconBefore={<FontAwesomeIcon icon={faFacebookF} />} 
                        disabled={isLoading}>Continuar com facebook</Button>
                </section>
            }


            {isAccountInactive ?
                <>
                    <div className="box" style={{
                        textAlign: 'center',
                        margin: '8px 0 32px',
                    }}>
                        <Text style={{ whiteSpace: 'nowrap' }}>Antes de autenticar, você precisa ativar essa conta.</Text>
                        <br />
                        <Button disabled={isLoading} onClick={() => setResendingCode(true)} marginTop={20} appearance="primary">enviar código ativação</Button>
                    </div>

                    <section>
                        <div className="box">
                            <div></div>
                            <div>
                                <Button disabled={isLoading} onClick={() => setAccountInactive(false)} appearance="minimal">Voltar para o login</Button>
                            </div>
                            <div></div>
                        </div>
                    </section>
                </> :

                <section>



                    <>
                        <TextInput
                            ref={usernameRef}
                            autoFocus={true}
                            height={40}
                            onKeyPress={onKeyPress}
                            name="text-input-username"
                            placeholder="E-mail"
                            disabled={isLoading}
                        />

                        <TextInput
                            ref={passwordRef}
                            height={40}
                            onKeyPress={onKeyPress}
                            name="text-input-password"
                            placeholder="Password"
                            type="password"
                            disabled={isLoading}
                        />

                        <Button disabled={isLoading} onClick={() => setResetingPassword(true)} appearance="minimal">Esqueceu sua senha?</Button>

                        <div className="box">
                            <Button disabled={isLoading} onClick={() => history.push("/create-account")} height={40} appearance="minimal" marginRight={16}>Criar conta</Button>

                            <div>
                                <Spinner hidden={!isLoading} size={32} marginRight={16} />
                                <Button height={40} appearance="primary" disabled={isLoading} onClick={submitLogin}>Entrar</Button>
                            </div>
                        </div>
                    </>

                </section>
            }

            <SendActivationCodeDialog activation isVisible={isResendingCode} onClose={() => { setResendingCode(false); }} />
            <SendActivationCodeDialog password isVisible={isResetingPassword} onClose={() => { setResetingPassword(false); }} />


        </Pane>
    )

}