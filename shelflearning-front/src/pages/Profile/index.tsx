import { Button, TextInput, Spinner, Heading, Menu, Text, toaster, FilePicker } from "evergreen-ui";
import React, { useContext, useEffect, useRef, useState } from "react";
import { Me } from "../../api/interfaces";
import Main from "../../components/Main";

import * as profileAPI from "../../api/profileAPI";


import './styles.css';
import useImage from "../../hooks/useImage";
import ProfileImage from "../../components/ProfileImage";
import { AuthContext } from "../../context/auth";

export default function Profile() {

    const { image, uploadImage, loadImage, removeImage } = useImage()

    const { updateLocalStorage } = useContext(AuthContext)

    const nameRef = useRef<HTMLInputElement | null>(null)
    const userNameRef = useRef<HTMLInputElement | null>(null)
    const currentPassword = useRef<HTMLInputElement | null>(null)
    const newPassword = useRef<HTMLInputElement | null>(null)
    const confirmPassword = useRef<HTMLInputElement | null>(null)

    const [isLoadingProfile, setLoadingProfile] = useState<boolean>()
    const [isSavingBasic, setSavingBasic] = useState<boolean>()
    const [isSavingPassword, setSavingPassword] = useState<boolean>()


    const [profile, setProfile] = useState<Me>()


    useEffect(() => {
        (async () => {
            setLoadingProfile(true)
            const profileDB = await profileAPI.me()
            setProfile(profileDB)

            if (profileDB.image) {
                loadImage(profileDB.image)
            }

            setLoadingProfile(false)
        })()
    }, [])


    useEffect(() => {
        if (profile) {
            if (nameRef.current) {
                nameRef.current.value = profile?.name || ""
            }

            if (userNameRef.current) {
                userNameRef.current.value = profile?.username || ""
            }
        }
    }, [profile])


    async function saveBasicInfo() {
        if (nameRef.current) {
            try {
                setSavingBasic(true)
                const meDB = await profileAPI.updateBasicInfo({ name: nameRef.current.value, image })
                setProfile(meDB)
                updateLocalStorage()
                toaster.success("Informações básicas atualizadas")
            } catch (error) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setSavingBasic(false)
            }
        }
    }

    async function savePassword() {
        if (currentPassword.current && newPassword.current && confirmPassword.current) {

            if (newPassword.current.value !== confirmPassword.current.value) {
                toaster.danger("A nova senha e a confirmação não são identicas")
                return
            }

            try {
                setSavingPassword(true)
                await profileAPI.changePassword(currentPassword.current.value, newPassword.current.value, confirmPassword.current.value)
                toaster.success("Senha atualizada")

                currentPassword.current.value = ''
                newPassword.current.value = ''
                confirmPassword.current.value = ''

            } catch (error) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setSavingPassword(false)
            }
        }

    }



    if (!profile) {
        return (
            <Main>
                <div className="Learn">
                    <section className="Learn-Loading">
                        <Spinner size={64} />
                        <Heading size={300} marginTop={16}>Carregando seus dados</Heading>
                    </section>
                </div>
            </Main>
        )
    }

    return (
        <Main>

            <div className="Profile Form">

                <Heading size={600} marginTop={32}>Perfil</Heading>

                <Heading size={400} marginTop={32}>Informações básicas</Heading>
                <Menu.Divider />

                <Text>Username</Text>

                <TextInput name="username"
                    placeholder="username"
                    readOnly
                    disabled
                    ref={userNameRef}
                    autoComplete="off"
                />

                <Text>Nome</Text>
                <TextInput name="name"
                    autoFocus
                    placeholder="Profile"
                    ref={nameRef}
                    autoComplete="off"
                />

                <FilePicker
                    className="Form-DivInput"
                    multiple
                    marginBottom={32}
                    onChange={files => uploadImage(files)}
                    placeholder="Selecione a imagem de perfil"
                />

                {image ?
                    <div style={{
                        marginBottom: 64,
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'column'
                    }}>
                        <ProfileImage image={image} size={200} />
                        <Button onClick={()=>removeImage()} marginTop={16} disabled={isLoadingProfile || isSavingBasic} intent={"danger"} appearance="primary">Remover imagem</Button>
                    </div>
                : null}

                <div style={{ marginTop: 16 }}>
                    <Button onClick={saveBasicInfo} disabled={isLoadingProfile || isSavingBasic} appearance="primary">Atualizar informações báscias</Button>
                    <Spinner hidden={!isSavingBasic} marginLeft={16} size={32} />
                </div>

                <Heading size={400} marginTop={32}>Acesso</Heading>
                <Menu.Divider />

                <Text>Senha atual</Text>
                <TextInput name="password"
                    type="password"
                    placeholder="Senha atual"
                    ref={currentPassword}
                    autoComplete="off"
                />

                <Text>Nova senha</Text>
                <TextInput name="password"
                    type="password"
                    placeholder="Senha atual"
                    ref={newPassword}
                    autoComplete="off"
                />

                <Text>Confirmação da nova senha</Text>
                <TextInput name="password"
                    type="password"
                    placeholder="Senha atual"
                    ref={confirmPassword}
                    autoComplete="off"
                />

                <div style={{ marginTop: 16 }}>
                    <Button onClick={savePassword} disabled={isLoadingProfile || isSavingPassword} appearance="primary">Atualizar senha</Button>
                    <Spinner hidden={!isSavingPassword} marginLeft={16} size={32} />
                </div>

            </div>

        </Main>
    );
}