import { faArrowLeft, faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Pane, Heading, Badge, Text, Button, Spinner, Alert, toaster } from "evergreen-ui";
import React, { Fragment, useContext, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import CourseTreeView from "../../components/CourseTree/CourseTreeView";
import Main from "../../components/Main";
import Progress from "../../components/Progress";
import SlideSheetMenu from "../../components/SlideSheetMenu";
import BlockedCourseAlert from "../../components/BlockedCourseAlert";
import UserSection from "../../components/UserSection";
import InputDialog from "../../components/InputDialog";
import { LearningContext } from "../../context/learning";
import history from "../../history";
import useImage from "../../hooks/useImage";
import * as courseAPI from "../../api/courseAPI";

import './styles.css'
import ClosedCourseComponent from "../../components/ClosedCourseAlert";
import { COURSE_STATUS } from "../../api/interfaces";

export default function Course() {

    const { course,
        courseId,
        enrollHandler,
        isLoadingCourse,
        isEnrolling,
        isEnrolled,
        getCurrentPage,
        coursePct,
        isAdmin,
        isOwner,
        refresh,
        isRefreshing
    } = useContext(LearningContext)

    const { image, loadImage } = useImage()
    const [isSideShown, setSideShown] = useState(false)
    const [courseProgress, setCourseProgress] = useState<number>(0)

    const [isBlocking, setBlocking] = useState(false)
    const [isBlockPopupShow, setShowBlockPopup] = useState(false)

    let location = useLocation()

    useEffect(() => {
        window.scrollTo(0, 0)

        if (course) {

            if (location.pathname.indexOf('resume') > -1) {
                const currentPageId = getCurrentPage()

                if (currentPageId === null) {
                    history.replace(`/course/${courseId}`)
                    return
                }

                history.replace(`/course/${courseId}/page/${currentPageId}`)
            }

            if (course.image) {
                loadImage(course.image)
            }

            setCourseProgress(coursePct(courseId))
        }
    }, [course])


    useEffect(() => {
        if (isEnrolling === false) {
            setCourseProgress(coursePct(courseId))
        }
    }, [isEnrolling])


    const goBack = () => {
        if (!isAdmin()) {
            history.push(`/my-courses`)
        } else {
            history.push('/')
        }
    }

    const goToResume = () => {
        history.push(`/course/${courseId}/resume`)
    }

    const renderMainButton = () => {

        if (course && isAdmin() && course.status === COURSE_STATUS.PUBLISHED) {
            return (
                <>
                    <Button className="Course-Enroll-Resume" disabled={isBlocking} intent="danger" onClick={() => setShowBlockPopup(true)} appearance="primary">
                        {isBlocking ? `Bloqueando curso` : `Bloquear curso`}
                    </Button>
                    <Spinner hidden={!isBlocking} size={32} marginRight={16} />
                </>

            )
        }

        if (isEnrolled() && !isAdmin()) {
            if (courseProgress < 100) {
                return <Button className="Course-Enroll-Resume" onClick={goToResume} appearance="primary">Continuar apendendo</Button>
            }
        } else {
            if (!isOwner() && !isAdmin()) {
                return (
                    <>
                        <Button className="Course-Enroll-Resume" disabled={isEnrolling} intent="success" onClick={enrollHandler} appearance="primary">
                            {isEnrolling ? 'Se matriculando no curso' : 'Matricular-se no curso'}</Button>
                        <Spinner hidden={!isEnrolling} size={32} marginRight={16} />
                    </>
                )
            }
        }
    }

    const blockCourseHandler = async (reason: string) => {

        setShowBlockPopup(false)

        try {
            if (courseId) {

                if (!isAdmin()) {
                    return
                }

                setBlocking(true)
                await courseAPI.instructors.blockCourse(courseId, reason)

                toaster.success("O curso foi bloqueado, acompanhe as alterações do professor para desbloqueá-lo")
                await refresh()

            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setBlocking(false)
        }

    }


    if (location.pathname.indexOf('resume') > -1) {
        return (
            <Main className="Main-CoursePage">
                <section className="Course">
                    <section className="Course-Loading">
                        <Spinner size={64} />
                        <Heading size={700} style={{ textAlign: "center" }}>Buscando a página em que você parou</Heading>
                    </section>
                </section>
            </Main>
        )
    }

    return (
        <>
            <Pane className="Header-Course" display="flex">
                <section>
                    <FontAwesomeIcon onClick={() => goBack()} style={{ color: 'white' }} size="lg" icon={faArrowLeft} />

                    <FontAwesomeIcon onClick={() => setSideShown(true)} style={{ color: 'white' }} size="lg" icon={faBars} />

                    <div style={{ width: '100%' }}>
                        {!isLoadingCourse ?
                            <div className="Course-Header-Details">
                                <Heading size={900}>{course?.title}</Heading>
                                <Text size={300}>{course?.createdBy?.name}</Text>
                                <Text size={300}>{`${course?.sectionsCount} Seções / ${course?.pagesCount} Páginas`}</Text>
                                <div className="Shelf-Tags">
                                    {course?.tags.map((tag: string) => <Badge key={tag} color="neutral" marginRight={8}>{tag}</Badge>)}
                                </div>
                            </div> : null}
                    </div>

                    <UserSection />
                </section>

                <Progress pct={courseProgress} />
            </Pane>
            <Main className="Main-CoursePage">
                <section className="Course">

                    {isLoadingCourse ?

                        <section className="Course-Loading">
                            <Spinner size={64} />
                            <Heading size={800}>Carregando curso</Heading>
                        </section>
                        :
                        <>
                            <ClosedCourseComponent />
                            {course && course.statusHistory &&
                                <BlockedCourseAlert
                                    maxWidth="initial"
                                    className=""
                                    onUpdateStatus={async () => await refresh()}
                                    courseId={courseId}
                                    statusHistory={course.statusHistory[0]}
                                    showUpdateStatus />
                            }

                            {image ? <div className="Course-Banner" style={{ backgroundImage: `url(${image})` }}></div> : null}

                            <div className="Course-ButtonSection">
                                {renderMainButton()}
                                <Spinner hidden={!isRefreshing} size={32} marginRight={16} />
                            </div>

                            {courseProgress === 100 && <Alert
                                intent="success"
                                title="Você concluiu esse curso"
                                marginTop={32}
                            />}

                            {course?.description ?
                                <div className="Course-Description">
                                    <Heading size={700}>Descrição</Heading>
                                    <Text size={300}>{course?.description.split('\n').map((item: any, key: any) => {
                                        return <Fragment key={key}>{item}<br /></Fragment>
                                    })}</Text>
                                </div>
                                : null}


                            <CourseTreeView style={{ marginTop: 32 }} hasLink expandAll />

                        </>

                    }

                </section>
            </Main>

            { isAdmin() ?

                <InputDialog
                    isVisible={isBlockPopupShow}
                    onClose={() => setShowBlockPopup(false)}
                    onSubmit={blockCourseHandler}
                    submitLabel="Bloquear"
                    title="Bloqueando curso"
                    notes={`Ao bloquear o curso nenhum aluno terá acesso ate o desbloqueio.<br/><br/> 
                            Informe o motivo para que o professor possa corrigir o problema e <br/> 
                            depois você terá que desbloquear esse curso.<br/><br/>`}
                    placeHolder="Motivo do bloqueio" />

                : null}

            <SlideSheetMenu isSideShown={isSideShown} setSideShown={setSideShown} />
        </>
    );
}