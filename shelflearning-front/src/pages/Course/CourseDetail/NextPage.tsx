import { ArrowLeftIcon, ArrowRightIcon, Button, Text, TickCircleIcon } from 'evergreen-ui'
import React, { useContext, useEffect, useRef, useState } from 'react'
import { Page } from '../../../api/interfaces'
import { LearningContext } from '../../../context/learning'
import history from '../../../history'
import useIsElementInViewPort from '../../../hooks/useIsElementInViewPort'
import useScrollPosition from '../../../hooks/useScrollPosition'

export default function NextPage(props: any) {

    const {
        isLoadingPage,
        getNextPage,
        updateProgress,
        courseId
    } = useContext(LearningContext)

    const nextPageRef = useRef<HTMLDivElement>(null)

    const { pageId, onUpdateProgress, isDone } = props

    const [nextPage, setNextPage] = useState<Page>()

    const [isEndOfPage, checkIfReachesEnd, setEndOfPage] = useIsElementInViewPort(nextPageRef)
    const [, positionY] = useScrollPosition()

    useEffect(() => {
        setEndOfPage(false)
        if (!isLoadingPage) {
            setNextPage(getNextPage(pageId))
        }
    }, [pageId, isLoadingPage])

    useEffect(() => {
        if (!isLoadingPage) {
            checkIfReachesEnd()
        }
    }, [positionY, isLoadingPage])

    useEffect(() => {
        if (isEndOfPage && !isLoadingPage) {
            const endOfPage = true
            updateProgress(pageId, endOfPage, () => {
                onUpdateProgress();
            })
        }

    }, [isEndOfPage, isLoadingPage])

    const goNextPage = async () => {
        if (!isDone) {
            const endOfPage = true
            updateProgress(pageId, endOfPage, () => {
                onUpdateProgress()
            })
        }

        history.push(`/course/${courseId}/page/${nextPage?.id}`)
    }


    function goBack() {

        if (!isDone) {
            const endOfPage = true
            updateProgress(pageId, endOfPage, () => {
                onUpdateProgress()
            })
        }
        
        history.push(`/course/${courseId}`)
    }

    if (isLoadingPage) {
        return (<></>)
    }

    return (
        <div className="CourseDetailDescription CourseDetailNextPage" ref={nextPageRef}>
            <section style={isDone ? { opacity: 1 } : { opacity: 0 }}>
                <TickCircleIcon color="success" marginRight={16} /><Text>Página visualizada</Text>
            </section>
            {nextPage ?
                <section>
                    <Button height={40} appearance="primary" onClick={goNextPage} iconAfter={ArrowRightIcon}>
                        Próxima página
                        </Button>
                    <Text>{nextPage?.title}</Text>
                </section>
                :
                <section>
                    <Button height={40}
                        appearance="primary"
                        intent="success"
                        onClick={goBack}
                        iconBefore={ArrowLeftIcon}>Voltar para a página do curso</Button>
                </section>
            }
        </div>
    )
}
