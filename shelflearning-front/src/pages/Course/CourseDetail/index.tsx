import { faArrowLeft, faBars, faThList } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Pane, Heading, Text, Spinner, SideSheet, Button, TickCircleIcon, toaster } from "evergreen-ui";
import React, { useContext, useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { COURSE_STATUS, Page } from "../../../api/interfaces";
import CourseTreeView from "../../../components/CourseTree/CourseTreeView";
import Main from "../../../components/Main";
import Progress from "../../../components/Progress";
import SlideSheetMenu from "../../../components/SlideSheetMenu";
import Stepts from "../../../components/Steps";
import { LearningContext } from "../../../context/learning";
import history from "../../../history";
import useElementSize from "../../../hooks/useElementSize";
import NextPage from "./NextPage";
import * as courseAPI from "../../../api/courseAPI";

import './styles.css'
import UserSection from "../../../components/UserSection";
import ClosedCourseComponent from "../../../components/ClosedCourseAlert";
import BlockedCourseAlert from "../../../components/BlockedCourseAlert";
import InputDialog from "../../../components/InputDialog";

export default function CourseDetail() {

    const { course,
        courseId,
        loadPageHandler,
        currentSectionAndPage,
        updateProgress,
        isPageDone,
        coursePct,
        isAdmin,
        isLoadingPage,
        refresh
    } = useContext(LearningContext)


    //#region states
    const [page, setPage] = useState<Page>()
    const [current, setCurrent] = useState<number[]>([])
    const [courseProgress, setCourseProgress] = useState<number>(0)
    //#endregion

    //#region states flgas
    const [isSideShown, setSideShown] = useState<boolean>(false)
    const [isPageMenuShown, setPageMenuShown] = useState<boolean>(false)
    const [isDone, setDone] = useState<boolean>(false)
    const [isBlockPopupShow, setShowBlockPopup] = useState(false)
    const [isBlocking, setBlocking] = useState(false)
    //#endregion

    //#region refs
    const sectionMenuRef = useRef<HTMLButtonElement>(null)
    //#endregion

    //#region hooks
    const [, height] = useElementSize('.Header-Course.CourseDetail')
    //#endregion

    const params: any = useParams<any>();

    //#region effects
    //first load
    useEffect(() => {
        (async () => {
            if (course) {
                const dbPage = await loadPageHandler(params.pageId)
                setPage(dbPage)
                updateProgress(params.pageId, false)
            }
        })()
    }, [params.pageId, course])

    useEffect(() => {
        if (isLoadingPage === false) {
            setCourseProgress(coursePct(courseId))
            setCurrent(currentSectionAndPage(params.pageId))
            setDone(isPageDone(params.pageId))
            window.scrollTo(0, 0)
        }
    }, [isLoadingPage, params.pageId, courseId])

    useEffect(() => {
        if (sectionMenuRef.current) {
            const headerHeight = document.querySelector('.Header-Course.CourseDetail')?.clientHeight || 0
            sectionMenuRef.current.style.top = `${headerHeight + 50}px`
        }
    }, [height, sectionMenuRef.current])
    //#endregion]

    //#region functions
    function goBack() {
        history.push(`/course/${courseId}`)
    }

    const blockCourseHandler = async (reason: string) => {

        setShowBlockPopup(false)

        try {
            if (courseId) {

                if (!isAdmin()) {
                    return
                }

                setBlocking(true)
                await courseAPI.instructors.blockCourse(courseId, reason)

                toaster.success("O curso foi bloqueado, acompanhe as alterações do professor para desbloqueá-lo")
                await refresh()

            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setBlocking(false)
        }

    }
    //#endregion

    //#region renders
    function renderMain() {

        if (!course || !page) {
            return renderFirstLoad()
        }

        return (
            <>
                <Main className="Main-CoursePage">
                    <section className="CourseDetail">

                        {course && course.statusHistory &&
                            <BlockedCourseAlert
                                maxWidth="initial"
                                className=""
                                onUpdateStatus={async () => await refresh()}
                                courseId={courseId}
                                statusHistory={course.statusHistory[0]}
                                showUpdateStatus />
                        }

                        {renderBlockCourseButton()}

                        {isLoadingPage ?
                            <section style={{
                                padding: 92,
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'center'
                            }}>
                                <Spinner size={64} />
                                <Heading size={600} style={{ opacity: 0.4 }} >Carregando conteudo</Heading>
                            </section>
                            : <>
                                <ClosedCourseComponent />
                                <div className="CourseDetailDescription" dangerouslySetInnerHTML={{ __html: page?.description || '' }}></div>
                            </>
                        }

                        {renderFooterPage()}
                    </section>
                </Main>
                {renderCourseMenu()}
            </>
        )
    }

    function renderBlockCourseButton() {
        if (course && isAdmin() && course.status === COURSE_STATUS.PUBLISHED) {
            return (
                <section style={{ display: 'flex', alignItems: 'center', marginTop: 24 }}>
                    <Button className="Course-Enroll-Resume" style={{ marginTop: 0 }} disabled={isBlocking} intent="danger" onClick={() => setShowBlockPopup(true)} appearance="primary">
                        {isBlocking ? `Bloqueando curso` : `Bloquear curso`}
                    </Button>
                    <Spinner hidden={!isBlocking} size={32} marginLeft={16} />
                </section>

            )
        }
    }

    function renderFirstLoad() {
        return (
            <section className="Course-Loading">
                <Spinner size={64} />
                <Heading size={800}>Carregando página</Heading>
            </section>
        )
    }

    function renderHeader() {

        return (
            <Pane className="Header-Course CourseDetail" display="flex">
                <section>
                    <FontAwesomeIcon onClick={goBack} style={{ color: 'black' }} size="lg" icon={faArrowLeft} />
                    <FontAwesomeIcon onClick={() => setSideShown(true)} style={{ color: 'black' }} size="lg" icon={faBars} />

                    <div style={{ width: '100%' }}>
                        <div className="Course-Header-Details">
                            <Heading size={900}>
                                {!isLoadingPage && isDone ? <TickCircleIcon color="success" marginRight={16} /> : null}
                                {isLoadingPage ? `...` : page?.title}
                            </Heading>
                            {isLoadingPage ? <Text size={300}>{`...`}</Text> : <Text size={300}>{`Sessão ${current?.[0]}, Página ${current?.[1]}`}</Text>}
                            <Stepts steps={current?.[2] || 0} done={current?.[1] || 0} />
                        </div>
                    </div>

                    <UserSection />

                    <div className="CoursePage-SectionsMenu-sm">
                        <button onClick={() => { setCourseProgress(coursePct(courseId)); setPageMenuShown(true); }}>
                            <FontAwesomeIcon style={{ color: '#0788DE' }} size="2x" icon={faThList} />
                        </button>
                    </div>
                </section>
                <Progress height={6} pct={courseProgress} />

                <button className="CoursePage-SectionsMenu" ref={sectionMenuRef} onClick={() => { setCourseProgress(coursePct(courseId)); setPageMenuShown(true); }}>
                    <div>Ver Seções e Páginas</div>
                    <FontAwesomeIcon style={{ color: 'white' }} size="lg" icon={faThList} />
                </button>
            </Pane>
        )
    }

    function renderCourseMenu() {
        return (
            <SideSheet
                position={"right"}
                width={340}
                isShown={isPageMenuShown}
                onCloseComplete={() => setPageMenuShown(false)}
            >
                <section className="CoursePage-SideMenuSections">
                    <header className={"CoursePage-SideMenuSections-Header"}>
                        <Heading size={100}>{page?.title}</Heading>
                        <Button marginRight={16} onClick={goBack} appearance="minimal">Voltar para a pagina do curso</Button>
                        <Progress pct={courseProgress} />
                    </header>

                    {current && <CourseTreeView defaultOpen={[course?.sections[current[0] - 1]?.id]} style={{ marginTop: 32 }} noTitle hasLink />}
                </section>

            </SideSheet>
        )
    }

    function renderFooterPage() {
        return (<NextPage
            isDone={isDone}
            pageId={params.pageId}
            onUpdateProgress={() => {
                setCourseProgress(coursePct(courseId))
                setDone(isPageDone(params.pageId))
            }} />)
    }
    //#endregion

    return (
        <>
            {renderHeader()}
            {renderMain()}

            <SlideSheetMenu isSideShown={isSideShown} setSideShown={setSideShown} />
            { isAdmin() ?

                <InputDialog
                    isVisible={isBlockPopupShow}
                    onClose={() => setShowBlockPopup(false)}
                    onSubmit={blockCourseHandler}
                    submitLabel="Bloquear"
                    title="Bloqueando curso"
                    notes={`Ao bloquear o curso nenhum aluno terá acesso ate o desbloqueio.<br/><br/> 
Informe o motivo para que o professor possa corrigir o problema e <br/> 
depois você terá que desbloquear esse curso.<br/><br/>`}
                    placeHolder="Motivo do bloqueio" />

                : null}
        </>
    )
}