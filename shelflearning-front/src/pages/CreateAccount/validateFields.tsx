export const validatePassword = (account:any) => {
    const { password } = account
    const re = /^(?=.*[\*.! @#\$%\^&\(\)\{\}\[\]:;<>,\.\?\/~_\+\-=\|])(?=.*[0-9])(?=.*[a-z]).{6,}$/
    if (!re.test(String(password).toLowerCase())) {
        return 'Senha deve ter 6 digitos contendo numero letra e caracter especial'
    }
    return false
}

export const validateConfirm = (account:any) => {
    const { password, confirm } = account
    if (password !== confirm) {
        return 'A senha deve ser a mesma'
    }
    return false
}

export const validateEmail = (account:any) => {
    const { username } = account
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(username).toLowerCase())) {
        return 'E-mail inválido'
    }
    return false
}
