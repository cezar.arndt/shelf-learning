import { Pane, Heading, TextInputField, Button, Spinner, toaster } from "evergreen-ui";
import React, { useContext, useEffect, useState } from "react";

import '../Login/styles.css';
import { AuthContext } from "../../context/auth";
import history from "../../history";

import {
    validateEmail,
    validatePassword,
    validateConfirm
} from './validateFields'


export default function CreateAccount(props: any) {

    const {
        handleCreateAccount
    } = useContext(AuthContext)

    const [isLoading, setLoading] = useState<boolean>(false)
    const [errors, setErrors] = useState<any>({})
    const [touched, setTouched] = useState<any>({})
    const [account, setAccount] = useState<any>({ name: "", username: "", password: "", confirm: "" })

    useEffect(() => {
        setErrors(validate())
    }, [account.username, account.password, account.confirm])

    const submitCreateAccount = async () => {

        if (Object.values(errors).every(e => e === false)) {
            setLoading(true)

            try {
                await handleCreateAccount(account)
                setAccount({ name: null, username: null, password: null, confirm: null })
                history.push('/activate-account')
                toaster.success(
                    "Conta criada",
                )
            } catch (error) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setLoading(false)
            }

        }
    }

    const goBackLogin = () => {
        history.push('/')
    }

    const disabledCreateButton = () => {
        const { name, username, password, confirm } = account
        return isLoading || !name || !username || !password || !confirm || password !== confirm
    }

    const handleChange = (evt: any) => {
        const { name, value: newValue } = evt.target;
        setAccount({
            ...account,
            [name]: newValue
        })

        setTouched({
            ...touched,
            [name]: true
        })
    }

    const validations = {
        name: () => { return false },
        username: validateEmail,
        password: validatePassword,
        confirm: validateConfirm
    }

    const validate = () => {
        const errors = {}
        for (var i in validations) {
            //@ts-ignore
            errors[i] = validations[i](account)
        }
        return errors
    }

    return (
        < Pane
            className="Login createAccount"
            elevation={1}
            float="left"
            display="flex"
            flexDirection="column"
        >
            <section>
                <Heading size={800}>Shelf learning</Heading>
                <Heading size={500}>Criar conta</Heading>
            </section>

            <section>

                <TextInputField
                    autoFocus={true}
                    name="name"
                    label="Nome"
                    value={account.name}
                    disabled={isLoading}
                    onChange={handleChange}
                />

                <TextInputField
                    type="email"
                    label="E-mail"
                    name="username"
                    disabled={isLoading}
                    onChange={handleChange}
                    isInvalid={touched.username && errors.username}
                    validationMessage={touched.username && errors.username}
                />

                <TextInputField name="password"
                    label="Senha"
                    type="password"
                    disabled={isLoading}
                    onChange={handleChange}
                    isInvalid={touched.password && errors.password}
                    validationMessage={touched.password && errors.password}
                />

                <TextInputField
                    name="confirm"
                    label="Confirmação da senha"
                    type="password"
                    disabled={isLoading}
                    onChange={handleChange}
                    isInvalid={touched.confirm && errors.confirm}
                    validationMessage={touched.confirm && errors.confirm}
                />

                <div className="box">
                    <div></div>
                    <div>
                        <Spinner hidden={!isLoading} size={32} marginRight={16} />
                        <Button height={40} appearance="primary" disabled={disabledCreateButton()} intent="success" onClick={submitCreateAccount}>
                            {isLoading ? 'Criando conta' : 'Criar'}
                        </Button>
                    </div>
                    <div></div>
                </div>

                <div className="box">
                    <div></div>
                    <div>
                        <Button disabled={isLoading} onClick={goBackLogin} appearance="minimal">Voltar para o login</Button>
                    </div>
                    <div></div>
                </div>

            </section>

        </Pane >
    )

}