import { Pane, Heading, TextInputField, Button, Spinner, toaster, Text } from "evergreen-ui";
import React, { useContext, useState } from "react";

import '../Login/styles.css';
import { AuthContext } from "../../context/auth";
import history from "../../history";
import SendActivationCodeDialog from "../../components/SendActivationCodeDialog";


export default function ValidateActivationCode(props: any) {

    const {
        handleActivateAccount
    } = useContext(AuthContext)

    const [isLoading, setLoading] = useState<boolean>(false)
    const [isResendingCode, setResendingCode] = useState<boolean>(false)
     
    const [code, setCode] = useState<any>("")

    const submiteActivateAccount = async () => {

        try {
            setLoading(true)
            await handleActivateAccount(code)
            setCode("")
            history.push('/')
            toaster.success(
                "Conta ativada",
            )
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setLoading(false)
        }

    }


    const goBackLogin = () => {
        history.push('/')
    }

    return (
        <Pane
            className="Login activateAccount"
            elevation={1}
            float="left"
            display="flex"
            flexDirection="column"
        >

            <section>
                <Heading size={800}>Shelf learning</Heading>
                <Heading size={500}>Ativar conta</Heading>
            </section>


            <div className="box" style={{
                textAlign: 'center',
                margin: '8px 0 32px',
            }}>
                <Text>Se não recebeu o código de ativação em seu e-mail você pode enviar um novo código.</Text>
                <br/>
                <Button disabled={isLoading} marginTop={16} onClick={()=>setResendingCode(true)} appearance="minimal">Enviar um novo código de ativação</Button>
            </div>

            <section>

                <TextInputField
                    autoFocus={true}
                    name="text-input-username"
                    label="Código de ativação"
                    autoComplete={"off"}
                    disabled={isLoading}
                    onChange={(e: any) => setCode(e.target.value)}
                />

                <div className="box">
                    <div></div>
                    <div>
                        <Spinner hidden={!isLoading} size={32} marginRight={16} />
                        <Button height={40} appearance="primary" disabled={code === "" || isLoading} intent="success" onClick={submiteActivateAccount}>
                            {isLoading ? 'Ativando conta' : 'Ativar conta'}
                        </Button>
                    </div>
                    <div></div>
                </div>

                <div className="box">
                    <div></div>
                    <div>
                        <Button disabled={isLoading} onClick={goBackLogin} appearance="minimal">Voltar para o login</Button>
                    </div>
                    <div></div>
                </div>

            </section>
            
            <SendActivationCodeDialog activation isVisible={isResendingCode} onClose={()=>setResendingCode(false)} />

        </Pane>
    )

}