import { Heading, Spinner } from "evergreen-ui";
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import * as courseAPI from "../../api/courseAPI";
import { Course } from "../../api/interfaces";
import Main from "../../components/Main";
import MainSearch from "../../components/MainSearch";
import Shelf, { LINK_COURSE } from "../../components/Shelf";

import blankImage from '../../assets/blank.png';
import noData from '../../assets/no-registers.png';

import './styles.css';

export default function Learn() {

    const [courses, setCourses] = useState<Course[]>([])
    const [isLoading, setLoading] = useState<boolean>(true)
    const [filter, setFilter] = useState<string>()

    let location = useLocation()

    useEffect(() => {
        listCourses()
    }, [location])


    const listCourses = () => {
        const search = new URLSearchParams(location.search)
        const filter = search.get('filter') || undefined
        setFilter(filter)
        setLoading(true)

        courseAPI.list(filter).then((courses: Course[]) => {
            setCourses(courses)
            setLoading(false)
        })
    }


    function renderCourses() {


        if (filter && !courses.length) {
            return (
                <div className="courses-Nothing">
                    <Heading size={600}>Nenhum curso foi encontrado com essa pesquisa</Heading>
                    <img src={noData} alt="" style={{ filter: 'grayscale(100%)' }} />
                </div>
            )

        }

        if( courses.length ){
            return <Shelf title="Cursos" goTo={LINK_COURSE.TOHOME} courses={courses} />
        }


        return (
            <div className="courses-Nothing">
                <Heading size={600}>Não existe nenhum curso publicado ate o momento</Heading>
                <img alt="" src={blankImage}/>
            </div>
        )

    }

    return (
        <Main>

            <MainSearch smallOnly />

            <div className="Learn">
                {isLoading ?
                    <section className="Learn-Loading">
                        <Spinner size={64} />
                        <Heading size={800}>Carregando cursos</Heading>
                    </section>

                    :
                    renderCourses()
                }
            </div>
        </Main>
    );
}