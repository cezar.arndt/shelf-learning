import { Button, Heading, Pane, Spinner, Table, toaster } from "evergreen-ui";
import React, { useEffect, useRef, useState } from "react";
import { Tag } from "../../api/interfaces";
import * as tagAPI from "../../api/tagAPI";
import Main from "../../components/Main";
import history from "../../history";


import './styles.css';

export default function TagList() {

    const [isLoadingTags, setLoadingTags] = useState<boolean>(false)
    const [tags, setTags] = useState<Tag[]>([])
    const [search, setSearch] = useState<string>("")
    const [isExporting, setExporting] = useState<boolean>(false)

    const timeoutRef = useRef<any>()

    useEffect(() => {

        (async () => {

            try {
                setLoadingTags(true)
                setTags([...(await tagAPI.list())])
            } catch (error) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setLoadingTags(false)
            }
        })()

    }, [])

    useEffect(() => {

        if (timeoutRef.current) {
            clearTimeout(timeoutRef.current)
        }

        timeoutRef.current = setTimeout(async () => {
            try {
                setLoadingTags(true)
                setTags([...(await tagAPI.list(search))])
            } catch (error) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setLoadingTags(false)
            }
        }, 500)

    }, [search])

    async function exportData() {
        setExporting(true)

        try {
            await tagAPI.exportData(search)    
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setExporting(false)
        }

    }   

    function renderContent() {

        if (isLoadingTags) {
            return loadingData()
        } else if (tags.length) {
            return tableData()
        } else {
            return emptyTable()
        }
    }

    function loadingData() {
        return <Spinner size={32} marginRight={16} style={{ margin: '20px auto', height: 'auto' }} />
    }

    function tableData() {
        return (<Table.Body height={240}>
            {tags.map(tag => (
                <Table.Row key={tag.name} isSelectable onSelect={() => history.push(`/tags/${tag.id}`)}>
                    <Table.TextCell>{tag.name}</Table.TextCell>
                    <Table.TextCell>{tag.coursesCount}</Table.TextCell>
                </Table.Row>
            ))}
        </Table.Body>)
    }

    function emptyTable() {
        return (<Table.Body height={240}>
            <Table.Row>
                <Table.TextCell>{search.length ? `Nenhuma tag encontrada` : `Não existe nenhuma tag cadastrada`}</Table.TextCell>
            </Table.Row>
        </Table.Body>)
    }

    return (
        <Main>

            <Pane
                className="Admin-tagList-Header" 
                height={60}
                display="flex"
                alignItems="center"
                justifyContent="center"
                border={false}>

                <div>
                    <Heading size={600}>Lista de Tags</Heading>
                </div>

                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Spinner size={32} marginRight={8} hidden={!isExporting} />
                    <Button onClick={exportData} disabled={isExporting} size={400}>Exportar tags</Button>
                </div>

            </Pane>

            <Table className="Admin-TagList">
                <Table.Head>
                    <Table.SearchHeaderCell onChange={value => setSearch(value)} />
                    <Table.HeaderCell>Cursos Utilizando</Table.HeaderCell>
                </Table.Head>
                {renderContent()}
            </Table>
        </Main>
    );
}