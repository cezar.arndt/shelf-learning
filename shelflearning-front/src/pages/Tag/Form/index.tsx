import { Button, TextInput, Spinner, ArrowLeftIcon, IconButton, Alert, toaster, Heading } from "evergreen-ui";
import React, {useEffect, useRef, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Tag, COURSE_STATUS } from "../../../api/interfaces";
import Main from "../../../components/Main";

import * as tagAPI from "../../../api/tagAPI";


import './styles.css';
import history from "../../../history";
import ConfirmDialog from "../../../components/ConfirmDialog";

export default function TagForm() {


    const titleRef = useRef<HTMLInputElement | null>(null)

    const [isLoadingTag, setLoadingTag] = useState<boolean>()
    const [tag, setTag] = useState<Tag>()
    const [deleteTagId, setDeleteTagId] = useState<any[]>()

    const params: any = useParams()

    useEffect(() => {
        if (params.id) {
            (async () => {
                try {
                    setLoadingTag(true)
                    const tagDB = await tagAPI.get(params.id)
                    setTag(tagDB)
                } catch ({ error }) {
                    toaster.danger(
                        error.message,
                    )
                    history.replace('/tags')
                } finally {
                    setLoadingTag(false)
                }
            })()
        }
    }, [params.id])


    useEffect(() => {
        if (tag) {
            if (titleRef.current) {
                titleRef.current.value = tag.name
            }
        }
    }, [tag])


    async function saveTag() {
        if (tag?.id && titleRef.current) {
            try {
                setLoadingTag(true)
                await tagAPI.update(tag.id, titleRef.current.value)
                toaster.success("Tag atualizada")
            } catch ({ error }) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setLoadingTag(false)
            }
        }
    }

    function onDeleteTag(tagId: any) {
        setDeleteTagId(tagId)
    }

    async function removeTag() {
        if (tag?.id) {
            try {
                setLoadingTag(true)
                await tagAPI.remove(tag.id)
                toaster.success("Tag removida")
                history.replace('/tags')
            } catch ({ error }) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setLoadingTag(false)
            }
        }
    }


    function renderCoursesQuantity() {
        if (tag?.coursesCount) {
            return (
                <section className={'Form'} style={{ marginBottom: 0 }}>
                    <Alert
                        intent="none"
                        title={`Essa tag esta sendo utilizada em ${tag?.coursesCount} ${tag?.coursesCount === 1 ? 'curso' : 'cursos'}`}
                        marginTop={32}
                    >
                        <div style={{ display: "flex", flexDirection: "column", alignItems: "start" }}>
                            {tag.courses && tag.courses.map(course => {

                                return (
                                    <Button is={Link}
                                        to={`/course/${course.id}`}
                                        target="_blank"
                                        marginTop={4}
                                        disabled={course.status === COURSE_STATUS.DRAFT}
                                        appearance="minimal" key={course.id}>{course.title} ({course.status})</Button>
                                )
                            })}
                        </div>

                    </Alert>
                </section>
            )
        }
    }


    if (!tag) {
        return (
            <Main>
                <div className="Learn">
                    <section className="Learn-Loading">
                        <Spinner size={64} />
                        <Heading size={300} marginTop={16}>Carregando tag</Heading>
                    </section>
                </div>
            </Main>
        )
    }

    return (
        <Main>

            <Link className="Form-Back" to={`/tags`}>
                <IconButton icon={ArrowLeftIcon} />
                <span>Voltar para a listagem</span>
            </Link>

            {renderCoursesQuantity()}

            <div className="TagForm Form">

                <TextInput name="title"
                    autoFocus
                    placeholder="Tag"
                    ref={titleRef}
                    autoComplete="off"
                />

                {/* <div style={{marginTop:32}}>
                    <Button className="Form-SaveButton" disabled={isSavingTag || isLoadingTag} onClick={saveTag} appearance="primary">Salvar</Button>
                    <Spinner hidden={!isSavingTag} size={32} marginRight={16} />
                </div> */}

                <div>
                    <Spinner hidden={!isLoadingTag} size={32} />
                    <Button onClick={saveTag} disabled={isLoadingTag} className="Form-SaveButton" appearance="primary">Salvar</Button>
                    <Button onClick={() => onDeleteTag(tag?.id)} disabled={isLoadingTag} className="Form-SaveButton" appearance="primary" intent={"danger"}>Deletar tag</Button>
                </div>

            </div>

            <ConfirmDialog
                isVisible={deleteTagId !== undefined}
                title="Remover Tag"
                text="A exclusão não pode ser revertida<br/> você tem certeza que deseja deletar essa tag ?"
                onClose={() => onDeleteTag(undefined)}
                onSubmit={() => { removeTag() }}
                submitLabel="Sim, deletar tag"
                cancelLabel="Não, mudei de idéia"
                intentButton="danger"
            />

        </Main>
    );
}