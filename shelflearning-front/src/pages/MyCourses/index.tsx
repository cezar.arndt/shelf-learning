import { Heading, Spinner } from "evergreen-ui";
import React, { useContext, useEffect, useState } from "react";
import Main from "../../components/Main";
import Shelf, { LINK_COURSE } from "../../components/Shelf";
import { LearningContext } from "../../context/learning";

import blankImage from '../../assets/blank.png';
import noData from '../../assets/no-registers.png';
import { useLocation } from "react-router-dom";
import MainSearch from "../../components/MainSearch";


export default function MyCourses() {

    const { myCoursesHandler, isLoadingMyCourses, myCourses } = useContext(LearningContext)
    const [filter, setFilter] = useState<string>()

    let location = useLocation()

    useEffect(() => {
        listCourses()
    }, [location])


    const listCourses = () => {
        const search = new URLSearchParams(location.search)
        const filter = search.get('filter') || undefined
        setFilter(filter)
        myCoursesHandler(filter)
    }


    function renderCourses() {

        if (filter && !myCourses.length) {
            return (
                <div className="courses-Nothing">
                    <Heading size={600}>Nenhum curso foi encontrado com essa pesquisa</Heading>
                    <img src={noData} alt="" style={{ filter: 'grayscale(100%)' }} />
                </div>
            )
        }

        if (myCourses.length) {
            return (
                <Shelf title="Meus cursos" enrollOnly goTo={LINK_COURSE.TOPAGE} alwaysShowPct courses={myCourses} />
            )
        }

        return (
            <div className="courses-Nothing">
                <Heading size={600}>Você não esta inscrito em nenhum curso ainda</Heading>
                <img alt="" src={blankImage}/>
            </div>
        )
    }

    return (
        <Main className="Main-MyCourses">

            <MainSearch smallOnly />

            <div className="Learn">
                {isLoadingMyCourses ?
                    <section className="Learn-Loading">
                        <Spinner size={64} />
                        <Heading size={800}>Carregando meus cursos</Heading>
                    </section>

                    :
                    renderCourses()
                }
            </div>
        </Main>
    )

}