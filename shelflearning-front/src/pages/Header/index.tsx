import React, { useContext, useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'

import { Heading, Pane, Tab, TabNavigation } from "evergreen-ui";

import './styles.css'
import { Link, useLocation } from 'react-router-dom';
import { TeachContext } from "../../context/teach";
import SlideSheetMenu from "../../components/SlideSheetMenu";
import useLoggedUser from "../../hooks/useLoggedUser";
import MainSearch from "../../components/MainSearch";
import UserSection from "../../components/UserSection";
import history from "../../history";
import { AuthContext } from "../../context/auth";

export default function Header() {

    const { course, isLoadingCourse } = useContext(TeachContext)
    const { isAdmin } = useLoggedUser()
    const { me } = useContext(AuthContext)

    let location = useLocation();

    const [isEditingPage, setEditingPage] = useState(false)
    const [current, setCurrent] = useState('/')
    const [isSideShown, setSideShown] = useState(false)


    let mainMenu: any[] = [
        { name: 'Aprender', link: '/' },
        { name: 'Meus cursos', link: '/my-courses' },
        { name: 'Ensinar', link: '/teach' },
    ]

    if(me.hasCourses > 0) 
        mainMenu.push({ name: 'Estatísticas', link: '/dashboard' })

    if (isAdmin()) {
        mainMenu = [
            { name: 'Cursos', link: '/' },
            { name: 'Tags', link: '/tags' },
            { name: 'Usuários', link: '/users' }
        ]
    }


    if (location.pathname.indexOf('/profile') > -1) {
        mainMenu = [
            {
                name: 'Sair do perfil', onClick: () => {
                    history.push("/")
                }
            },
        ]
    }

    useEffect(() => {
        setCurrent(location.pathname)
        setEditingPage((new RegExp("/teach/course/.+/page/.+", "g")).test(location.pathname))


    }, [location]);

    return (
        <>
            <Pane className="Header" display="flex">
                <FontAwesomeIcon onClick={() => setSideShown(true)} style={{ color: '#66788A' }} size="lg" icon={faBars} />

                <div>
                    <Heading size={800}>Shelf learning</Heading>
                </div>

                <MainSearch />
                <UserSection />
            </Pane>

            { isEditingPage ?
                <div className="Header-Navigation gray">
                    {isLoadingCourse ? `Carregando curso...` : course?.title}
                </div>
                :
                <TabNavigation className={`Header-Navigation${isAdmin() ? ' admin' : ''}`}>
                    {mainMenu.map(tab => {
                        if ('onClick' in tab) {
                            return (
                                <Tab key={tab.name} onSelect={() => tab.onClick()} id={tab.name}>
                                    {tab.name}
                                </Tab>
                            )
                        } else {
                            return (
                                <Tab key={tab.name} is={Link} to={tab.link} id={tab.name} isSelected={current.slice(1).split('/')[0] === tab.link.slice(1)}>
                                    {tab.name}
                                </Tab>
                            )
                        }
                    })}
                </TabNavigation>

            }


            <SlideSheetMenu isSideShown={isSideShown} setSideShown={setSideShown} />

        </>
    );
}