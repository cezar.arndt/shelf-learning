import { Button, Heading, Pane, Spinner, Table, toaster } from "evergreen-ui";
import React, { useEffect, useRef, useState } from "react";
import { User, USER_TYPE } from "../../api/interfaces";
import * as userAPI from "../../api/userAPI";
import Main from "../../components/Main";
import history from "../../history";


import './styles.css';

export default function UserList() {

    const [isLoadingUsers, setUsersLoading] = useState<boolean>(false)
    const [users, setUsers] = useState<User[]>([])
    const [search, setSearch] = useState<string>("")
    const [isExporting, setExporting] = useState<boolean>(false)

    

    const timeoutRef = useRef<any>()

    useEffect(() => {

        if (timeoutRef.current) {
            clearTimeout(timeoutRef.current)
        }

        timeoutRef.current = setTimeout(async () => {
            try {
                setUsersLoading(true)
                setUsers([...(await userAPI.list(search))])
            } catch (error) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setUsersLoading(false)
            }
        }, 500)

    }, [search])

    async function exportData() {
        setExporting(true)

        try {
            await userAPI.exportData(search)    
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setExporting(false)
        }

    }   

    function renderContent() {

        if (isLoadingUsers) {
            return loadingData()
        } else if (users.length) {
            return tableData()
        } else {
            return emptyTable()
        }
    }

    function loadingData() {
        return <Spinner size={32} marginRight={16} style={{ margin: '20px auto', height: 'auto' }} />
    }

    function tableData() {
        return (<Table.Body height={240}>
            {users.map(user => (
                <Table.Row
                    intent={user.disabledAt ? 'warning' : 'none'}
                    key={user.username} isSelectable onSelect={() => history.push(`/users/${user.id}`)}>
                    <Table.TextCell>{user.username}{`${user.type === USER_TYPE.ADMIN ? ' (Administrador)' : ''}`}</Table.TextCell>
                    <Table.TextCell>{user.disabledAt ? (new Date(user.disabledAt)).toLocaleString() : null}</Table.TextCell>
                    <Table.TextCell>{user.isActive ? 'Sim' : 'Não'}</Table.TextCell>
                </Table.Row>
            ))}
        </Table.Body>)
    }

    function emptyTable() {
        return (<Table.Body height={240}>
            <Table.Row>
                <Table.TextCell>{search.length ? `Nenhum usuário encontrado` : `Não existe nenhum usuários cadastrado`}</Table.TextCell>
            </Table.Row>
        </Table.Body>)
    }

    return (
        <Main>

            <Pane
                className="Admin-UserList-Header" 
                height={60}
                display="flex"
                alignItems="center"
                justifyContent="center"
                border={false}>

                <div>
                    <Heading size={600}>Lista de Usuários</Heading>
                </div>
                
                <div style={{display:'flex',alignItems:'center'}}>
                    <Spinner size={32} marginRight={8} hidden={!isExporting}/>
                    <Button onClick={exportData} disabled={isExporting} size={400}>Exportar usuários</Button>
                </div>

            </Pane>

            <Table className="Admin-UserList">
                <Table.Head>
                    <Table.SearchHeaderCell onChange={value => setSearch(value)} />
                    <Table.TextHeaderCell>
                        Desabilitado em
                    </Table.TextHeaderCell>
                    <Table.TextHeaderCell>
                        Conta Ativada
                    </Table.TextHeaderCell>
                </Table.Head>
                {renderContent()}
            </Table>
        </Main>
    );
}