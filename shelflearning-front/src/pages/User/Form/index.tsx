import { Button, Spinner, ArrowLeftIcon, IconButton, Text, toaster, Heading, Switch, Menu } from "evergreen-ui";
import React, { useEffect, useRef, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { User } from "../../../api/interfaces";
import Main from "../../../components/Main";

import * as userAPI from "../../../api/userAPI";


import './styles.css';
import history from "../../../history";

export default function UserForm() {


    const userNameRef = useRef<HTMLInputElement | null>(null)
   
    const [isLoadingUser, setLoadingUser] = useState<boolean>()
    const [user, setUser] = useState<User>()
    const [active, setActive] = useState<boolean>()

    const params:any = useParams()

    useEffect(() => {
        if( params.id ){
            (async () => {

                try {
                    setLoadingUser(true)
                    const userDB = await userAPI.get(params.id)
                    setUser(userDB)
                    setActive( userDB.disabledAt === null )
                } catch ({ error }) {
                    toaster.danger(
                        error.message,
                    )
                    history.replace('/users')
                } finally {
                    setLoadingUser(false)
                }
            })()
        }
    }, [params.id])

    
    useEffect(() => {
        if (user) {
            if (userNameRef.current) {
                userNameRef.current.value = user.username
            }
        }
    }, [user])


    async function saveUser() {
       
            try {
                setLoadingUser(true)
                if( user ) {
                    let userDB = null

                    if( active ) {
                        userDB = await userAPI.enable(user.id)
                        toaster.success("Usuário habilitado")
                    } else {
                        userDB = await userAPI.disable(user.id)
                        toaster.success("Usuário desabilitado")
                    }

                    setUser(userDB)

                }
            } catch ({error}) {
                toaster.danger(
                    error.message,
                )
            } finally {
                setLoadingUser(false)
            }

        
    }

    
    if( !user ){
        return (
            <Main>
                <div className="Learn">
                <section className="Learn-Loading">
                    <Spinner size={64} />
                    <Heading size={300} marginTop={16}>Carregando usuário</Heading>
                </section>
                </div>
            </Main>
        )
    }

    return (
        <Main>
            
            <Link className="Form-Back" to={`/users`}>
                <IconButton icon={ArrowLeftIcon} />
                <span>Voltar para a listagem</span>
            </Link>
        
            <div className="UserForm Form">
                <section>
                    <Text>Conta ativada:</Text>
                    <Text>{user.isActive ? 'Sim' : 'Não'}</Text>
                </section>
                <Menu.Divider />
                <section>
                    <Text>Username:</Text>
                    <Text>{user.username}</Text>
                </section>
                <Menu.Divider />
                <section>
                    <Text>Nome:</Text>
                    <Text>{user.name || '--'}</Text>
                </section>
                <Menu.Divider />
                <section>
                    <Text>Tipo:</Text>
                    <Text>{user.type}</Text>
                </section>
                <Menu.Divider />
                <section>
                    <Text>Habilitado</Text>
                    <Switch marginBottom={16} 
                    height={24}
                    checked={active}
                    onChange={e => setActive(e.target.checked)} />
                    {user.disabledAt && <Text style={{margin:'1px 0 0px 12px',opacity: 0.5}}>(Desabilitado em: {(new Date(user.disabledAt)).toLocaleString()})</Text>}
                </section>
               

                <div>
                    <Spinner hidden={!isLoadingUser} size={32} />
                    <Button onClick={saveUser} disabled={isLoadingUser || active === (user.disabledAt === null) } className="Form-SaveButton" appearance="primary">Salvar</Button>
                </div>

            </div>

        </Main>
    );
}