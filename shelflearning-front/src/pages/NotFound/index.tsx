import { ArrowLeftIcon, Button, Heading, Pane } from "evergreen-ui";
import React from "react";


import './styles.css'
import notFoundImage from '../../assets/404.png';
import history from "../../history";

export default function NotFound() {

    const goBack = () => {
        history.push(`/`)
    }

    return (
            <Pane
                className="NotFound"
                elevation={1}
                float="left"
                display="flex"
            >

                <section>
                <Heading size={400}>Shelf learning</Heading>
                <Heading size={600}>Página não encontrada</Heading>
                    <img alt="" src={notFoundImage}/>
                    
                    <Button height={40} appearance="minimal" onClick={goBack} iconBefore={ArrowLeftIcon}>
                        Voltar para a plataforma
                    </Button>
                </section>
            </Pane>
    );
}