import { ArrowLeftIcon, Button, Heading, Pane, Text } from "evergreen-ui";
import React from "react";


import './styles.css'
import blockedImage from '../../assets/blocked.png';
import history from "../../history";

export default function BlockedCourse() {

    const goBack = () => {
        history.push(`/my-courses`)
    }

    return (
            <Pane
                className="BlockedCourse"
                elevation={1}
                float="left"
                display="flex"
                flexDirection="column"
            >

                <section>
                <Heading size={400}>Shelf learning</Heading>
                <Heading size={600}>Esse curso está bloqueado</Heading>
                    <img src={blockedImage} alt=""/>
                    
                    <section>
                        <Text>Um administrador bloqueou esse curso pois seu conteúdo pode estar infringindo as políticas da plataforma.</Text>
                        <Text marginTop={16}>Assim que o professor fizer os ajustes solicitados pelo administrador, o curso volta a ficar disponível.</Text>
                    </section>

                    <Button height={40} appearance="minimal" onClick={goBack} iconBefore={ArrowLeftIcon}>
                        Voltar para os meus cursos
                    </Button>
                </section>
            </Pane>
    );
}