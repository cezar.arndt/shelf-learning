import { Pane, Heading, Button, Text } from "evergreen-ui";
import React from "react";

import '../Login/styles.css';
import './styles.css';
import history from "../../history";

export default function DataDeletion(props: any) {


    const goBackLogin = () => {
        history.push('/')
    }

    return (
        <Pane
            className="DataDeletion"
            display="flex"
        >
            <section>
                <Heading size={800}>Shelf learning</Heading>
                <Heading size={500}>Facebook data deletion</Heading>
            </section>

            <section style={{alignSelf:'flex-start',marginTop:64}}>

                <Heading size={800}>This is an app with study purpose only</Heading>
                <br/>
                <Text>
                    This app named "shelflearning" is just an app built as a final project to the course "DWFS from PUC Minas EAD (Brazil)",
                    and has <strong>no intention to become a product in fact</strong>, after my evaluation the app will be removed as well the database.
                    <br/><br/>
                    If you got on this app by accident and logged in with your facebook account don't worry in this app i only store your e-mail, Name and profile photo (at the time the account was create).
                    
                    <br/><br/>
                    just send me an e-mail so i can remove you user from de database and sent you proves
                    <br/><br/>
                    <strong>cezar.arndt@gmail.com</strong>
                </Text>


                <div className="box" style={{
                    width: 200,
                    margin: '64px auto'
                }}>
                    <div></div>
                    <div>
                        <Button onClick={goBackLogin} appearance="minimal">Voltar para a página inicial</Button>
                    </div>
                    <div></div>
                </div>

            </section>

        </Pane >
    )

}