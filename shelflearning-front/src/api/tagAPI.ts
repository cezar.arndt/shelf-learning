import Api from './api';
import downloadFile from './downloadFile';
import { Tag } from './interfaces';

//#region course
var endpointPublic = `/v1/tag`

const list = async (filter?: string) => {
    try {
        const { data } = await Api.get<Tag[]>(`${endpointPublic}${filter ? `?name=${filter}` : ''}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const get = async (id: string) => {
    try {
        const { data } = await Api.get<Tag>(`${endpointPublic}/${id}`)
        return data
    } catch ({error,status}) {
        throw {error, status}
    }
}

const update = async (id: string, name:string) => {
    try {
        const { data } = await Api.put<Tag>(`${endpointPublic}/${id}`,{name})
        return data
    } catch ({error,status}) {
        throw {error, status}
    }
}

const remove = async (id: string) => {
    try {
        const { data } = await Api.delete<Tag>(`${endpointPublic}/${id}`)
        return data
    } catch ({error,status}) {
        throw {error, status}
    }
}

const exportData = async (filter?: string) => {
    try {
        const {data, headers} = await Api.get<Blob>(`${endpointPublic}/export${filter ? `?name=${filter}` : ''}`)
        downloadFile(data,headers)
    } catch ({error,status}) {
        throw {error, status}
    }
}

//#endregion

export { list, get, update, remove, exportData }