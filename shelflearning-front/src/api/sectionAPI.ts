import Api from './api';
import { Section } from './interfaces';


var endpointInstructor = `/v1/course/instructor`

const create = async (courseId: string, title: string) => {

    try {
        const { data } = await Api.post<Section>(`${endpointInstructor}/${courseId}/section`, { title })
        return data
    } catch ({ error }) {
        throw error
    }
}


const list = async (courseId: string) => {

    try {
        const { data } = await Api.get<Section[]>(`${endpointInstructor}/${courseId}/section`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const changeOrder = async (courseId: string, id: string, order: number) => {
    try {
        const { data } = await Api.put<Section>(`${endpointInstructor}/${courseId}/section/${id}/order`, { order })
        return data
    } catch ({ error }) {
        throw error
    }
}

const updateTitle = async (courseId: string, id: string, title: string) => {
    try {
        const { data } = await Api.put<Section>(`${endpointInstructor}/${courseId}/section/${id}/title`, { title })
        return data
    } catch ({ error }) {
        throw error
    }
}

const remove = async (courseId:string, id: string) => {
    try {
        const { data } =  await  Api.delete<Section[]>(`${endpointInstructor}/${courseId}/section/${id}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

export { create, list, changeOrder, updateTitle, remove }