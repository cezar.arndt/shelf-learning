import Axios from "axios";
import history from "../history";

const Api = Axios.create({
    baseURL: '/api',
    timeout: 30000,
    headers: {
        "Cache-Control": "no-cache, no-store, must-revalidate",
        Pragma: "no-cache",
        "Content-Type": "application/json"
    }
});


const setupInterceptors = () => {

    Api.interceptors.request.use(async config => {

        const token = localStorage.getItem('access-token');

        if (token) {
            config.headers['access-token'] = token
        }

        return config
    })

    Api.interceptors.response.use(response => {
        return response;
    }, error => {

        if (window.location.pathname !== '/auth' && error.response && error.response.status === 401) {
            localStorage.clear()

            if( window.location.pathname.indexOf('/authenticate') === -1){
                history.push('/auth', { referer: window.location.pathname })
            }
        }

        if (error.response && error.response.data.error) {
            throw { ...error.response.data, status:error.response.status }
        } else {
            throw error
        }

    });

}

export { setupInterceptors }
export default Api;