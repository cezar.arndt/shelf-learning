import Api from './api';

var endpointPublic = `/v1/statistics`

const coursePerStatus = async () => {
    try {
        const { data } = await Api.get(`${endpointPublic}/course-per-status`)
        return data
    } catch ({ error }) {
        throw error
    }
}


const userAndStudents = async () => {
    try {
        const { data } = await Api.get(`${endpointPublic}/user-and-students`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const studentsPerCourse = async () => {
    try {
        const { data } = await Api.get(`${endpointPublic}/students-per-course`)
        return data
    } catch ({ error }) {
        throw error
    }
}


const doneRatePerCourse = async () => {
    try {
        const { data } = await Api.get(`${endpointPublic}/done-rate-per-course`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const enrollPerMonth = async () => {
    try {
        const { data } = await Api.get(`${endpointPublic}/enroll-per-month`)
        return data
    } catch ({ error }) {
        throw error
    }
}





export { coursePerStatus, userAndStudents, studentsPerCourse, doneRatePerCourse, enrollPerMonth }