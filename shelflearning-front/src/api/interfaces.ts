export interface User {
    type: string
    name: string
    username: string
    id: string,
    image?: string,
    disabledAt?:Date
    isActive?:boolean
}

export enum USER_TYPE {
    USER = "USER",
    ADMIN = "ADMIN"
}

export interface Me {
    id?: string,
    name?: string
    username?: string
    image?: string|null,
    type?: string
}

export interface UserCourse {
    id: string
    user: string
    course: string
    currentPage?: string
    pagesDone?:string[]
    sectionsDone?:string[]
    progress?: number
    lastAccessAt?: Date
    certifiedEmmitedAt?: Date
}

export enum COURSE_STATUS {
    DRAFT = "DRAFT",
    PUBLISHED = "PUBLISHED",
    CLOSED = "CLOSED",
    BLOCKED = "BLOCKED"
}

export interface Tag {
    id?: string
    name: string,
    coursesCount?:number,
    courses?:{
        id?:string,
        title?:string,
        status?:COURSE_STATUS
    }[]
}

export interface Page {
    id: string
    title: string
    description?: string
    order: number,
    descriptionLength: number
}

export interface Section {
    id: string
    title: string
    order: number,
    pages: Page[],
    open: boolean
}

export interface CourseStatusHistory {
    changedBy?: User
    atDate?: Date
    reason: string
    from?: COURSE_STATUS
    to: COURSE_STATUS
}

export interface Course {

    id?: string
    createdBy?: User
    createdAt?: Date
    tags: string[]

    title: string
    description?: string

    status?: COURSE_STATUS

    sections?: Section[]
    pagesCount?: number
    sectionsCount?: number
    studentsCount?: number

    statusHistory?: CourseStatusHistory[]

    image?: string

    progress?:number
}

export interface CourseProgress {
    currentPage:string
    isEndPage?:boolean
}