import Api from './api';
import * as queryString from 'query-string';

const login = async (username: string, password: string) => {
    try {
        const { data } = await Api.post("/v1/auth", { username, password })
        return data
    } catch ({error}) {
        throw error
    }
}

const createAccount = async (account:any) => {
    try {
        const { data } = await Api.post("/v1/auth/create-account", account)
        return data
    } catch ({error}) {
        throw error
    }
}

const activateAccount = async (validationToken:string) => {
    try {
        const { data } = await Api.post("/v1/auth/activate-account", {validationToken})
        return data
    } catch ({error}) {
        throw error
    }
}


const sendActivationCode = async (email:string) => {
    try {
        const { data } = await Api.post("/v1/auth/reset-activation-code", {username:email})
        return data
    } catch ({error}) {
        throw error
    }
}

const sendPasswordCode = async (email:string) => {
    try {
        const { data } = await Api.post("/v1/auth/reset-password-code", {username:email})
        return data
    } catch ({error}) {
        throw error
    }
}

const resetPassword = async (account:any, validationToken:string) => {
    try {
        const { data } = await Api.post("/v1/auth/reset-password", {account, validationToken})
        return data
    } catch ({error}) {
        throw error
    }
}

const logout = async () => {
    localStorage.clear();
}

const urlToLoginWithFacebook = () => {
    
    const client_id = 206147704633681
    const redirect_uri = `${window.location.origin}/authenticate/facebook`

    const stringifiedParams = queryString.stringify({
        client_id,
        redirect_uri,
        scope: ['email', 'public_profile'].join(','), 
        response_type: 'code',
        auth_type: 'rerequest',
        display: 'popup',
      });
      
      return `https://www.facebook.com/v4.0/dialog/oauth?${stringifiedParams}`;
}

export enum PROVIDER {
    Facebook = "Facebook"
}

const externalLogin = async (code:string, provider:PROVIDER) => {
    try {
        const { data } = await Api.post("/v1/auth/external-login", {code, provider})
        return data
    } catch ({error}) {
        throw error
    }
}

export { login, logout, createAccount, activateAccount, sendActivationCode, sendPasswordCode, resetPassword, urlToLoginWithFacebook, externalLogin }