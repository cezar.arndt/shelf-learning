const downloadFile = async (data:any,headers:any) => {
    try {
        var attatchment = new Blob([data], {type: 'text/csv'});
            var csvURL = window.URL.createObjectURL(attatchment);
            const tempLink = document.createElement('a');
            tempLink.href = csvURL;
            tempLink.download = headers['content-disposition'].split('=')[1];
            tempLink.click();

    } catch ({error,status}) {
        throw {error, status}
    }
}

export default downloadFile