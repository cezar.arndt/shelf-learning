import Api from './api';
import downloadFile from './downloadFile';
import { Course, CourseProgress, CourseStatusHistory, COURSE_STATUS } from './interfaces';

//#region course
var endpointPublic = `/v1/course/public`

const list = async (filter?: string) => {
    try {
        const { data } = await Api.get<Course[]>(`${endpointPublic}${filter ? `?filter=${filter}` : ''}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const get = async (id: string) => {
    try {
        const { data } = await Api.get<Course>(`${endpointPublic}/${id}`)
        return data
    } catch ({error,status}) {
        throw {error, status}
    }
}

const enrollmentInfo = async () => {
    try {
        const { data } = await Api.get<Course[]>(`${endpointPublic}/enrollment-info`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const enroll = async (id: string) => {
    try {
        const { data } = await Api.post<void>(`${endpointPublic}/${id}/enroll`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const enrollment = async (filter?: string) => {
    try {
        const { data } = await Api.get<Course[]>(`${endpointPublic}/enrollment${filter ? `?filter=${filter}` : ''}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const updateProgress = async (id:string, courseProgress:CourseProgress) => {
    try {
        const { data } = await Api.put<Course>(`${endpointPublic}/${id}/progress`,courseProgress)
        return data
    } catch ({ error }) {
        throw error
    }
}

const exportData = async (filter?: string) => {
    try {
        const {data, headers} = await Api.get<Blob>(`${endpointPublic}/export${filter ? `?filter=${filter}` : ''}`)
        downloadFile(data,headers)
    } catch ({error,status}) {
        throw {error, status}
    }
}


const exportDataEnroll = async (filter?: string) => {
    try {
        const {data, headers} = await Api.get<Blob>(`${endpointPublic}/export-enroll${filter ? `?filter=${filter}` : ''}`)
        downloadFile(data,headers)
    } catch ({error,status}) {
        throw {error, status}
    }
}

const exportTree = async (courseId: string) => {
    try {
        const {data, headers} = await Api.get<Blob>(`${endpointPublic}/${courseId}/export-tree`)
        downloadFile(data,headers)
    } catch ({error,status}) {
        throw {error, status}
    }
}


//#endregion

//#region instructor
var endpointInstructor = `/v1/course/instructor`
const create = async (course: Course) => {
    try {
        const { data } = await Api.post<Course>(endpointInstructor, course)
        return data
    } catch ({ error }) {
        throw error
    }
}


const listInstructor = async (filter?:string) => {
    try {
        const { data } = await Api.get<Course[]>(`${endpointInstructor}${filter ? `?filter=${filter}` : ''}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const getInstructor = async (id: string) => {
    try {
        const { data } = await Api.get<Course>(`${endpointInstructor}/${id}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const getInstructorInfo = async (id: string) => {
    try {
        const { data } = await Api.get<Course>(`${endpointInstructor}/${id}/info`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const updateInstructor = async (id: string, course: Course) => {
    try {
        const { data } = await Api.put<Course>(`${endpointInstructor}/${id}`, course)
        return data
    } catch ({ error }) {
        throw error
    }
}


const changeStatusInstructor = async (id: string, toStatus: CourseStatusHistory) => {
    try {
        const { data } = await Api.put<Course>(`${endpointInstructor}/${id}/status`, toStatus)
        return data
    } catch ({ error }) {
        throw error
    }
}

const removeCourse = async (id: string) => {
    try {
        await Api.delete<null>(`${endpointInstructor}/${id}`)
    } catch ({ error }) {
        throw error
    }
}

const blockCourse = async (id:string, reason:string) => {
    try {
        const { data } = await Api.put<Course>(`${endpointInstructor}/${id}/status`,{to:COURSE_STATUS.BLOCKED,reason})
        return data
    } catch ({ error }) {
        throw error
    }
}

const unblockCourse = async (id:string, reason:string) => {
    try {
        const { data } = await Api.put<Course>(`${endpointInstructor}/${id}/status`,{to:COURSE_STATUS.PUBLISHED,reason})
        return data
    } catch ({ error }) {
        throw error
    }
}

const exportDataInstructor = async (filter?: string) => {
    try {
        const {data, headers} = await Api.get<Blob>(`${endpointInstructor}/export${filter ? `?filter=${filter}` : ''}`)
        downloadFile(data,headers)
    } catch ({error,status}) {
        throw {error, status}
    }
}

const exportTreeInstructor = async (courseId: string) => {
    try {
        const {data, headers} = await Api.get<Blob>(`${endpointInstructor}/${courseId}/export-tree`)
        downloadFile(data,headers)
    } catch ({error,status}) {
        throw {error, status}
    }
}

//#endregion

const instructors = {
    create,
    list: listInstructor,
    get: getInstructor,
    update: updateInstructor,
    info: getInstructorInfo,
    changeStatus: changeStatusInstructor,
    removeCourse,
    blockCourse,
    unblockCourse,
    exportData:exportDataInstructor,
    exportTree:exportTreeInstructor
}

export { list, get, enrollment, enrollmentInfo, enroll, updateProgress, exportData, exportTree, exportDataEnroll, instructors }