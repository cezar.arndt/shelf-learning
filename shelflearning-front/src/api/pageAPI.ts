import Api from './api';
import { Page } from './interfaces';

//#region public

var endpointPublic = `/v1/course/public`
const getPublic = async (courseId: string, id: string) => {
    try {
        const { data } = await Api.get<Page>(`${endpointPublic}/${courseId}/page/${id}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

//#endregion

//#region instructor
var endpointInstructor = `/v1/course/instructor`

const create = async (courseId: string, sectionId: string, title: string) => {
    try {
        const { data } = await Api.post<Page>(`${endpointInstructor}/${courseId}/section/${sectionId}/page`, { title })
        return data
    } catch ({ error }) {
        throw error
    }
}

const update = async (courseId: string, sectionId: string, id: string, title: string) => {
    try {
        const { data } = await Api.put<Page>(`${endpointInstructor}/${courseId}/section/${sectionId}/page/${id}`, { title })
        return data
    } catch ({ error }) {
        throw error
    }
}

const changeOrder = async (courseId: string, sectionId: string, id: string, order: number) => {
    try {
        const { data } = await Api.put<Page>(`${endpointInstructor}/${courseId}/section/${sectionId}/page/${id}/order`, { order })
        return data
    } catch ({ error }) {
        throw error
    }
}

const updateDescription = async (courseId: string, sectionId: string, id: string, description: string) => {
    try {
        const { data } = await Api.put<Page>(`${endpointInstructor}/${courseId}/section/${sectionId}/page/${id}/description`, { description })
        return data
    } catch ({ error }) {
        throw error
    }
}

const get = async (courseId: string, sectionId: string, id: string) => {
    try {
        const { data } = await Api.get<Page>(`${endpointInstructor}/${courseId}/section/${sectionId}/page/${id}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const remove = async (courseId: string, sectionId: string, id: string) => {
    try {
        const { data } =  await Api.delete<Page[]>(`${endpointInstructor}/${courseId}/section/${sectionId}/page/${id}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

//#endregion

export { create, update, changeOrder, get, getPublic, updateDescription, remove }