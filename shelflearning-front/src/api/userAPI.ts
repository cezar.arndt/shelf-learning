import Api from './api';
import downloadFile from './downloadFile';
import { User } from './interfaces';

//#region course
var endpointPublic = `/v1/user`

const list = async (filter?: string) => {
    try {
        const { data } = await Api.get<User[]>(`${endpointPublic}${filter ? `?name=${filter}` : ''}`)
        return data
    } catch ({ error }) {
        throw error
    }
}

const get = async (id: string) => {
    try {
        const { data } = await Api.get<User>(`${endpointPublic}/${id}`)
        return data
    } catch ({error,status}) {
        throw {error, status}
    }
}

const enable = async (id: string) => {
    try {
        const { data } = await Api.put<User>(`${endpointPublic}/${id}/enable`)
        return data
    } catch ({error,status}) {
        throw {error, status}
    }
}

const disable = async (id: string) => {
    try {
        const { data } = await Api.put<User>(`${endpointPublic}/${id}/disable`)
        return data
    } catch ({error,status}) {
        throw {error, status}
    }
}

const exportData = async (filter?: string) => {
    try {
        const {data, headers} = await Api.get<Blob>(`${endpointPublic}/export${filter ? `?name=${filter}` : ''}`)
        downloadFile(data,headers)
    } catch ({error,status}) {
        throw {error, status}
    }
}

//#endregion

export {  enable, disable, get, list, exportData }