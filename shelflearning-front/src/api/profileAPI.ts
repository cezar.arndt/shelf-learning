import Api from './api';
import { Me } from './interfaces';


var endpointProfile = `/v1/auth/me`

const me = async () => {
    try {
        const { data } = await Api.get(`${endpointProfile}`)
        return data
    } catch ({error}) {
        throw error
    }
}

const updateBasicInfo = async (me:Me) => {
    try {
        const { data } = await Api.put<Me>(`${endpointProfile}/basic`, me)
        return data
    } catch ({error}) {
        throw error
    }
}

const changePassword = async (currentPassword:string, newPassword:string, newPasswordConfirm:string) => {
    try {
        const { data } = await Api.put<null>(`${endpointProfile}/password`, {currentPassword, newPassword, newPasswordConfirm})
        return data
    } catch ({error}) {
        throw error
    }
}

export { me, updateBasicInfo, changePassword }