import { useLayoutEffect, useState } from 'react';

export default function useScrollPosition() {
    const [position, setPosition] = useState([0, 0]);

    useLayoutEffect(() => {
        function updatePosition() {
            setPosition([window.scrollX, window.scrollY]);
        }
        window.addEventListener('scroll', updatePosition);
        updatePosition();
        return () => window.removeEventListener('scroll', updatePosition);
    }, []);
    
    return position;
}