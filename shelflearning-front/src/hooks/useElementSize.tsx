import { useLayoutEffect, useState } from 'react';

export default function useElementSize(target:string) {
    const [size, setSize] = useState([0, 0]);

    //@ts-ignore
    const ro = new ResizeObserver(entries => {
        const rect = entries[0].target.getBoundingClientRect()
        setSize([rect.width, rect.height]);
      });

    useLayoutEffect(() => {
        const node = document.querySelector(target)
        if( node ) {
            ro.observe(node);
        }
    }, []);
    
    return size;
}