import { useLayoutEffect, useState } from 'react';
import { User, USER_TYPE } from '../api/interfaces';

export default function useLoggedUser() {
    const [user, setUser] = useState<User>();


    function isAdmin():boolean{
        if( user ){
            return user.type === USER_TYPE.ADMIN
        }   
        return false
    }

    function isUser():boolean{
        if( user ){
            return user.type === USER_TYPE.USER
        }   
        return false
    }

    useLayoutEffect(() => {
        const me = JSON.parse( localStorage.getItem("me") || "{}" )
        setUser(me)
    }, []);
    
    return {user, isAdmin, isUser};
}