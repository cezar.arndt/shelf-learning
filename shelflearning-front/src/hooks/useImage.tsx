import { useState } from 'react'


export default function useImage() {

    const [image, setImage] = useState<string|null>()


    const loadImage = (image: any) => {
        setImage(`data:${image.contentType};base64,${image.data}`)
    }

    const removeImage = () => setImage(null)

    const uploadImage = (files: any) => {

        const file = files[0];

        if (file) {

            const reader = new FileReader()
            reader.readAsDataURL(file)

            reader.onload = async (e: any) => {

                const img = document.createElement('img')
                img.src = e.target.result

                img.onload = (ee: any) => {

                    const canvas = document.createElement('canvas')
                    let ctx = canvas.getContext('2d')
                    ctx?.drawImage(img, 0, 0)

                    const MAX_WIDTH = 1024
                    const MAX_HEIGHT = 768

                    let { naturalWidth: width, naturalHeight: height } = ee.target

                    if (width > height) {
                        if (width > MAX_WIDTH) {
                            height *= MAX_WIDTH / width
                            width = MAX_WIDTH
                        }
                    } else {
                        if (height > MAX_HEIGHT) {
                            width *= MAX_HEIGHT / height;
                            height = MAX_HEIGHT
                        }
                    }

                    canvas.width = width;
                    canvas.height = height;
                    ctx = canvas.getContext('2d');
                    ctx?.drawImage(img, 0, 0, width, height);

                    //@ts-ignore
                    setImage(canvas.toDataURL(file.type));
                }
            }

        }


    }


    return { image, uploadImage, loadImage, removeImage }


}