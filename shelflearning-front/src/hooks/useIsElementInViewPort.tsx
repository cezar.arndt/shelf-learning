import { useState } from 'react';

export default function useIsElementInViewPort(element:any):any[] {
    const [isVisible, setIsVisible] = useState<boolean>(false);
    let timer:any = null

    function updateVisible():void{
        setIsVisible(false)

        timer = setTimeout(()=>{
            if( element.current ) {
                const rect = element.current.getBoundingClientRect()
                setIsVisible(
                    rect.top < window.innerHeight &&
                    rect.bottom >= 0
                )
            }

        },500)

    }

    return [isVisible, updateVisible, setIsVisible]
}