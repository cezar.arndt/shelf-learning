import React, { createContext, useEffect, useState } from 'react';
import { toaster } from "evergreen-ui";

import { Course, CourseProgress, UserCourse, USER_TYPE } from '../api/interfaces';
import * as courseAPI from "../api/courseAPI";
import * as pageAPI from '../api/pageAPI';
import history from '../history';


export const LearningContext = createContext<any>({});

export function LearningProvider(props: any) {
    return (
        <LearningContext.Provider value={{ ...useLearning() }}>
            {props.children}
        </LearningContext.Provider>
    )
}

export function useLearning() {

    //#region setup
    const [isLoadingCourse, setLoading] = useState<boolean>(true);
    const [isRefreshing, setRefreshing] = useState<boolean>(true);
    const [isLoadingMyCourses, setLoadingMyCourses] = useState<boolean>(false);
    const [myCourses, setMyCourses] = useState<Course[]>([]);
    const [isEnrolling, setEnrolling] = useState<boolean>(false);
    const [course, setCourse] = useState<Course | undefined>();
    const [courseId, setCourseId] = useState<string | undefined>()
    const [isLoadingPage, setLoadingPage] = useState<boolean>(true);


    useEffect(() => {
        const [, basepath, courseParam] = window.location.pathname.split('/')

        if (basepath === 'course' && courseParam && courseParam !== 'blocked' && courseId !== courseParam) {
            setCourseId(courseParam)
        } else if (!courseParam) {
            setCourse(undefined)
            setCourseId(undefined)
        }
    }, [window.location.pathname])


    useEffect(() => {

        (async () => {
            setLoading(true)
            await refresh()
            setLoading(false)
        })()

    }, [courseId])
    
    async function refresh() {
        try {
            setRefreshing(true)
            if (courseId) {
                const courseDB = await courseAPI.get(courseId)
                setCourse(courseDB)
            }
        } catch ({ error, status }) {
            const statusBlocked = 403
            if (status === statusBlocked) {
                history.replace(`/course/blocked/${courseId}`)
            } else {
                toaster.danger(
                    error.message,
                )
                history.replace(`/`)
            }
        } finally {
            setRefreshing(false)
        }
    }

    //#endregion

    //#region  course
    async function enrollHandler() {
        try {
            if (courseId) {
                setEnrolling(true)
                await courseAPI.enroll(courseId)

                const subCourses = await courseAPI.enrollmentInfo()
                localStorage.setItem('enrollment', JSON.stringify(subCourses))
                setEnrolling(false)

                toaster.success(
                    "Você foi matriculado no curso, bons estudos!",
                )
            }
        } catch (error) {
            setEnrolling(false)
            toaster.danger(
                error.message,
            )
        }
    }

    async function myCoursesHandler(filter?: string) {
        try {
            setLoadingMyCourses(true)
            const myCoursesDB = await courseAPI.enrollment(filter)
            setMyCourses(myCoursesDB)
            setLoadingMyCourses(false)
        } catch (error) {
            setLoadingMyCourses(false)
            toaster.danger(
                error.message,
            )
        }
    }


    function isEnrolled() {
        const enrolledCourses = JSON.parse(localStorage.getItem('enrollment') || '[]')
        return enrolledCourses.find((sc: UserCourse) => sc.course === courseId) || false
    }

    function getCurrentPage() {
        const enrolledCourses = JSON.parse(localStorage.getItem('enrollment') || '[]')
        const course: UserCourse = enrolledCourses.find((sc: UserCourse) => sc.course === courseId) || {}

        if (course) {
            return course.currentPage
        }
    }

    function isPageDone(id: string) {
        const enrolledCourses = JSON.parse(localStorage.getItem('enrollment') || '[]')

        const course: UserCourse = enrolledCourses.find((sc: UserCourse) => sc.course === courseId) || {}

        if (course) {
            return course.pagesDone?.find(e => e === id) !== undefined
        }
    }

    function isSectionDone(id: string) {
        const enrolledCourses = JSON.parse(localStorage.getItem('enrollment') || '[]')

        const course: UserCourse = enrolledCourses.find((sc: UserCourse) => sc.course === courseId) || {}

        if (course) {
            return course.sectionsDone?.find(e => e === id) !== undefined
        }
    }

    function getNextPage(pageId: string) {
        try {
            if (courseId && course) {
                for (let i = 0, j = course.sections?.length || 0; i < j; i++) {
                    const currentSection = course.sections?.[i]
                    for (let m = 0, n = currentSection?.pages?.length || 0; m < n; m++) {
                        const currentPage = currentSection?.pages?.[m]
                        if (currentPage?.id === pageId) {
                            const nextpage = currentSection?.pages?.[m + 1]
                            const nextSection = course.sections?.[i + 1]
                            if (nextpage) {
                                return nextpage
                            } else if (nextSection) {
                                return nextSection.pages?.[0]
                            }
                        }
                    }
                }
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }

    function isAdmin() {
        const { type = USER_TYPE.USER } = JSON.parse(localStorage.getItem('me') || '{}')
        return type === USER_TYPE.ADMIN
    }

    function isOwner() {
        const { id } = JSON.parse(localStorage.getItem('me') || '{}')
        return id === course?.createdBy?.id
    }

    async function updateProgress(pageId: string, isEndPage: boolean = false, callback = ()=>{}) {

        try {
            if (courseId) {

                if (isAdmin() || isOwner() || (isEndPage && isPageDone(pageId))) {
                    return
                }

                const progress: CourseProgress = {
                    currentPage: pageId,
                    isEndPage
                }
                const page = await courseAPI.updateProgress(courseId, progress)

                const subCourses = await courseAPI.enrollmentInfo()
                localStorage.setItem('enrollment', JSON.stringify(subCourses))

                callback()
                return page
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }

    function coursePct(id: string) {
        try {
            const enrolledCourses = JSON.parse(localStorage.getItem('enrollment') || '[]')
            const course: UserCourse = enrolledCourses.find((sc: UserCourse) => sc.course === id) || {}

            return course.progress
        }
        catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }

    //#endregion

    //#region section

    //#endregion


    //#region page
    async function loadPageHandler(id: string) {

        try {
            if (courseId) {
                setLoadingPage(true)
                const page = await pageAPI.getPublic(courseId, id)
                return page
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
            history.replace(`/course/${courseId}`)
        } finally {
            setLoadingPage(false)
        }

    }

    /**
     * Return
     * Current Section
     * Current Page
     * Total os Pages
     */
    function currentSectionAndPage(pageId: string) {

        try {
            if (courseId && course) {
                for (let i = 0, j = course.sections?.length || 0; i < j; i++) {
                    const currentSection = course.sections?.[i]
                    for (let m = 0, n = currentSection?.pages?.length || 0; m < n; m++) {
                        const currentPage = currentSection?.pages?.[m]
                        if (currentPage?.id === pageId) {
                            return [i + 1, m + 1, n]
                        }
                    }
                }
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }

    }
    //#endregion

    return {
        isLoadingCourse,
        isEnrolling,
        course,
        courseId,
        isLoadingMyCourses,
        myCourses,
        isAdmin,
        coursePct,
        isSectionDone,
        isPageDone,
        updateProgress,
        currentSectionAndPage,
        getNextPage,
        loadPageHandler,
        enrollHandler,
        isEnrolled,
        myCoursesHandler,
        getCurrentPage,
        isOwner,
        refresh,
        isRefreshing,
        isLoadingPage
    };
}