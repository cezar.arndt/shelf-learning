import React, { createContext, useEffect, useState } from 'react';
import { toaster } from "evergreen-ui";

import { Course, COURSE_STATUS, Tag } from '../api/interfaces';
import * as courseAPI from "../api/courseAPI";
import * as sectionAPI from '../api/sectionAPI';
import * as pageAPI from '../api/pageAPI';
import * as tagAPI from "../api/tagAPI";

import history from '../history';


export const TeachContext = createContext<any>({});

export function TeachProvider(props: any) {

    return (
        <TeachContext.Provider value={{ ...useTeach() }}>
            {props.children}
        </TeachContext.Provider>
    )

}

export function useTeach() {

    //#region setup
    const [isLoadingCourse, setLoading] = useState<boolean>(true);
    const [isLoadingTags, setLoadingTags] = useState<boolean>(true);
    const [isSavingCourse, setSavingCourse] = useState<boolean>(false);
    const [isPublishingCourse, setPublishingCourse] = useState<boolean>(false);
    const [course, setCourse] = useState<Course | undefined>();
    const [allTags, setAllTags] = useState<Tag[]>([])

    const [courseId, setCourseId] = useState<string | undefined>()

    useEffect(() => {
        const [, basepath, , courseParam] = window.location.pathname.split('/')
        if (basepath === 'teach' && courseParam && courseId !== courseParam) {
            setCourseId(courseParam)
        } else if (!courseParam) {
            setCourse(undefined)
            setCourseId(undefined)
        }
    }, [window.location.pathname])


    useEffect(() => {

        (async () => {
            try {
                setLoading(true)

                if (courseId) {
                    const courseDB = await courseAPI.instructors.get(courseId)
                    setCourse(courseDB)
                }
            } catch (error) {

                if (error.code === "C08") {
                    history.replace("/teach")
                }

                toaster.danger(
                    error.message,
                )
            } finally {
                setLoading(false)
            }
        })()

    }, [courseId])

    //#endregion

    //#region  course
    async function saveCourseHandler(course: Course) {
        try {

            setSavingCourse(true)

            if (courseId) {

                await courseAPI.instructors.update(courseId, course)

                toaster.success(
                    `Informações atualizadas`,
                )

            } else {
                const courseDB = await courseAPI.instructors.create(course)

                history.push(`/teach/course/${courseDB.id}/sections-n-pages`)
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setSavingCourse(false)
        }

    }

    async function closeCourse(reason: string) {
        try {

            if (courseId) {
                const toPublished = {
                    to: COURSE_STATUS.CLOSED,
                    reason: reason || 'Changed to closed'
                }

                await courseAPI.instructors.changeStatus(courseId, toPublished)

                setLoading(true)
                const courseDB = await courseAPI.instructors.get(courseId)
                setCourse(courseDB)
                setLoading(false)

                toaster.success(
                    "Seu curso foi fechado",
                )

            }

        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }

    async function removeCourseHandler(reason: string) {
        try {

            if (courseId) {
                await courseAPI.instructors.removeCourse(courseId)
                toaster.success(
                    "Seu curso foi deletado",
                )
            }

        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }


    async function publishCourseHandler(course: Course) {

        try {

            if (courseId) {
                setPublishingCourse(true)

                const toPublished = {
                    to: COURSE_STATUS.PUBLISHED,
                    reason: 'Changed to Published'
                }

                await courseAPI.instructors.changeStatus(courseId, toPublished)

                setLoading(true)
                const courseDB = await courseAPI.instructors.get(courseId)
                setCourse(courseDB)
                setLoading(false)


                toaster.success(
                    "Seu curso foi publicado!",
                )

            }

        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setPublishingCourse(false)
        }

    }

    //#endregion

    //#region section
    async function addSectionHandler(title: string) {
        try {
            if (courseId) {
                const section = await sectionAPI.create(courseId, title)
                course?.sections?.push(section)
                return section
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }

    async function updateSectionTitleHandler(sectionId: string, title: string) {
        try {
            if (courseId) {
                const section = course?.sections?.find(e => e.id === sectionId)
                if (section) {
                    const sectionDB = await sectionAPI.updateTitle(courseId, sectionId, title)
                    section.title = sectionDB.title

                    return [sectionDB, course?.sections]
                }
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }

    async function removeSectionHandler(sectionId: string) {
        try {
            if (courseId) {
                const sectionIndex = course?.sections?.findIndex(e => e.id === sectionId)
                if (sectionIndex) {
                    const updateSections = await sectionAPI.remove(courseId, sectionId)

                    if (course?.sections) {
                        course.sections = updateSections
                    }

                    toaster.success(
                        "A Seção e todas as suas páginas foram removida",
                    )
                }
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }

    async function changeSectionOrderHandler(sectionId: string, direction: number) {

        if (courseId && course && course.sections) {

            const section = course.sections.find(e => e.id === sectionId)

            if (section) {
                const newOrder = section.order + direction

                if (newOrder > -1 && newOrder < course.sections.length) {
                    const index = course.sections.indexOf(section)

                    try {
                        await sectionAPI.changeOrder(courseId, sectionId, newOrder)
                        course.sections[index + direction].order = section.order
                        section.order = newOrder

                        course.sections.sort((a, b) => a.order < b.order ? -1 : 1)

                    } catch (error) {
                        toaster.danger(
                            error.message,
                        )
                    }
                }
            }

            return course.sections
        }

    }
    //#endregion

    //#region page
    async function addPageHandler(sectionId: string, title: string) {
        try {

            if (courseId) {
                const section = course?.sections?.find(e => e.id === sectionId)
                if (section) {
                    const page = await pageAPI.create(courseId, sectionId, title)
                    section.pages.push(page)

                    return [page, course?.sections]
                }
            }

        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }

    async function removePageHandler(pageId: string, sectionId: string) {
        try {
            if (courseId) {
                const section = course?.sections?.find(e => e.id === sectionId)
                if (section) {

                    const page = section.pages.find((e: any) => e.id === pageId)

                    if (page) {
                        const updatedPages = await pageAPI.remove(courseId, sectionId, pageId)
                        section.pages = updatedPages

                        toaster.success(
                            "A Página foi removida",
                        )
                    }
                }
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }

    async function updatePageTitleHandler(pageId: string, sectionId: string, title: string) {
        try {
            if (courseId) {
                const section = course?.sections?.find(e => e.id === sectionId)
                if (section) {

                    const page = section.pages.find((e: any) => e.id === pageId)

                    if (page) {
                        const pageDB = await pageAPI.update(courseId, sectionId, pageId, title)
                        page.title = pageDB.title
                        return pageDB
                    }

                }
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }
    }


    async function changePageOrderHandler(pageId: string, sectionId: string, direction: number) {

        if (courseId && course && course.sections) {

            const section = course.sections.find(e => e.id === sectionId)

            if (section) {

                const page = section.pages.find((e: any) => e.id === pageId)

                if (page) {
                    const newOrder = page.order + direction

                    if (newOrder > -1 && newOrder < section.pages.length) {

                        try {
                            const index = section.pages.indexOf(page)

                            await pageAPI.changeOrder(courseId, sectionId, pageId, newOrder)

                            section.pages[index + direction].order = page.order
                            page.order = newOrder

                            section.pages.sort((a: any, b: any) => a.order < b.order ? -1 : 1)
                        } catch (error) {
                            toaster.danger(
                                error.message,
                            )
                        }
                    }
                }
            }
        }
    }


    async function loadPageHandler(id: string) {

        try {
            if (courseId) {

                const section = findSectionByPage(id)

                if (section) {
                    const page = await pageAPI.get(courseId, section.id, id)
                    return page
                } else {

                    history.replace(`/teach/course/${courseId}/sections-n-pages`)
                    toaster.danger(
                        "Página não encontrada",
                    )
                }
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }

    }

    async function updateDescriptionHandler(id: string, description: string) {

        try {
            if (courseId) {

                const section = findSectionByPage(id)

                if (section) {
                    const page = await pageAPI.updateDescription(courseId, section.id, id, description)
                    toaster.success(
                        `Descrição salva`,
                    )
                    return page
                }
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        }

    }

    //#endregion

    async function listTagsHandler() {
        try {
            setLoadingTags(true)
            setAllTags([...(await tagAPI.list())])
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setLoadingTags(false)
        }
    }

    function findSectionByPage(pageId: string) {

        if (courseId) {
            const section = course?.sections?.find(e => {
                const page = e.pages.find(ee => ee.id === pageId)

                if (page) {
                    return true
                }

            })

            return section || null
        }

    }
    //#endregion

    return {
        isLoadingCourse,
        course,
        courseId,
        saveCourseHandler,
        isSavingCourse,
        addSectionHandler,
        removeSectionHandler,
        updateSectionTitleHandler,
        changeSectionOrderHandler,
        addPageHandler,
        removePageHandler,
        updatePageTitleHandler,
        changePageOrderHandler,
        findSectionByPage,
        loadPageHandler,
        updateDescriptionHandler,
        publishCourseHandler,
        closeCourse,
        removeCourseHandler,
        listTagsHandler,
        isLoadingTags,
        allTags,
        isPublishingCourse
    };
}