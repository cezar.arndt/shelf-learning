import { toaster } from 'evergreen-ui';
import React, { createContext, useEffect, useState } from 'react';
import { login, createAccount, activateAccount, sendActivationCode, resetPassword, sendPasswordCode } from "../api/authAPI";
import { enrollmentInfo } from "../api/courseAPI";
import { me as getUserInfo } from "../api/profileAPI";

import { Me, USER_TYPE } from '../api/interfaces';

import history from '../history'

export const AuthContext = createContext<any>({});

export function AuthProvider(props: any) {

    return (
        <AuthContext.Provider value={{ ...useAuth() }}>
            {props.children}
        </AuthContext.Provider>
    )

}

export function useAuth() {

    const [authenticated, setAuthenticated] = useState(false)
    const [loading, setLoading] = useState(true)
    const [me, setMe] = useState<Me>()

    useEffect(() => {
        (async () => {
            const token = localStorage.getItem('access-token')

            if (token) {
                await setLocalStorage(token)
                setAuthenticated(true)
            }

            setLoading(false)
        })()
    }, [])

    async function handleLogin(user: string, password: string, referer: any = null) {
        try {
            const { token } = await login(user, password)
            await setLocalStorage(token)
            setAuthenticated(true)

            if (referer) {
                history.push(referer)
            } else {
                history.push('/')
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )

            return error.code
        }

    }

    async function handleCreateAccount(account: any) {
        await createAccount(account)
    }

    async function handleActivateAccount(token: string) {
        await activateAccount(token)
    }

    async function handleSendActivationCode(email: string) {
        await sendActivationCode(email)
    }

    async function handleSendPasswordCode(email: string) {
        await sendPasswordCode(email)
    }

    async function handleResetPassword(account:any, token: string) {
        await resetPassword(account, token)
    }

    async function setLocalStorage(token: string) {
        localStorage.removeItem('enrollment')
        localStorage.setItem('access-token', token)

        const userDB = await getUserInfo()

        if (userDB.image) {
            userDB.image = `data:${userDB.image.contentType};base64,${userDB.image.data}`
        }

        localStorage.setItem('me', JSON.stringify(userDB))

        if (userDB.type !== USER_TYPE.ADMIN) {
            const subCourses = await enrollmentInfo()
            localStorage.setItem('enrollment', JSON.stringify(subCourses))
        }

        setMe(userDB)
    }

    async function updateLocalStorage() {
        const token = localStorage.getItem('access-token')
        if (token) {
            await setLocalStorage(token)
        }
    }

    function handleLogout() {
        setAuthenticated(false)
        localStorage.clear()
        history.push('/auth')
    }

    return {
        authenticated,
        loading,
        handleLogin,
        handleLogout,
        me,
        setAuthenticated,
        setLocalStorage,
        updateLocalStorage,
        handleCreateAccount,
        handleActivateAccount,
        handleSendActivationCode,
        handleResetPassword,
        handleSendPasswordCode
    };
}