import React, { useEffect, useState } from "react";
import {
  Router, Route, Switch, useLocation,
} from "react-router-dom";
import { setupInterceptors } from "./api/api";



import './App.css';
import PrivateRoute from "./components/PrivateRoute";
import { AuthProvider } from "./context/auth";
import Learn from "./pages/Learn";
import Login from "./pages/Login";
import MyCourses from "./pages/MyCourses";
import Teach from "./pages/Teach";
import TeachDashboard from "./pages/Teach/TeachDashboard";
import CourseFormBasic from "./pages/Teach/CourseForm/Basic";
import CourseFormSectionsNPages from "./pages/Teach/CourseForm/SectionsNPages";
import CourseFormPublish from "./pages/Teach/CourseForm/Publish";
import PageForm from "./pages/Teach/PageForm";
import TagList from "./pages/Tag";
import TagForm from "./pages/Tag/Form";


import { TeachProvider } from "./context/teach";
import { LearningProvider } from "./context/learning";
import history from './history'
import Course from "./pages/Course";
import CourseDetail from "./pages/Course/CourseDetail";
import BlockedCourse from "./pages/BlockedCourse";
import NotFound from "./pages/NotFound";
import UserList from "./pages/User";
import UserForm from "./pages/User/Form";
import Profile from "./pages/Profile";
import CreateAccount from "./pages/CreateAccount";
import ValidateActivationCode from "./pages/ValidateActivationCode";
import ResetPassword from "./pages/ResetPassword";
import FacebookRedirect from "./pages/FacebookRedirect";
import DataDeletion from "./pages/DataDeletion";
import PrivacyPolicy from "./pages/PrivacyPolicy";


//Workaround =(
function NotFoundNestedRoutes({ children }: any) {

  const location = useLocation()
  const [notFound, setNotFound] = useState<boolean>(false)

  const exp = [
    /my-courses\/?$/g,
    /teach\/course\/[\w]+\/?$/g,
    /teach\/new\/?$/g,
    /teach\/?$/g,
    /teach\/course\/[\w]+\/sections-n-pages\/?$/g,
    /teach\/course\/[\w]+\/publish\/?/g,
    /teach\/course\/[\w]+\/page\/[\w]+\/?$/g,
    /course\/[\w]+\/?$/g,
    /course\/blocked\/[\w]+\/?$/g,
    /course\/[\w]+\/resume\/?$/g,
    /course\/[\w]+\/page\/[\w]+\/?$/g,
  ]

  useEffect(() => {
    if (location.pathname.indexOf('teach') > -1 ||
      location.pathname.indexOf('my-courses') > -1 ||
      location.pathname.indexOf('course') > -1) {
      setNotFound(exp.every(e => !e.test(location.pathname)))
    } else {
      setNotFound(false)
    }
  }, [location, exp])

  if (notFound) {
    return (
      <div style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', display: "flex" }}>
        <NotFound />
      </div>
    )
  }

  return children

}

function App() {
  setupInterceptors()
  return (
    <div className="App">
      <AuthProvider>
        <Router history={history}>
          <NotFoundNestedRoutes>
            <Switch>

              <Route path="/teach" render={() => (
                <>
                  <TeachProvider>
                    <PrivateRoute path="/teach" exact component={Teach} />
                    <PrivateRoute path="/teach/new" component={CourseFormBasic} />
                    <PrivateRoute path="/teach/course/:id" exact component={CourseFormBasic} />
                    <PrivateRoute path="/teach/course/:id/sections-n-pages" component={CourseFormSectionsNPages} />
                    <PrivateRoute path="/teach/course/:id/publish" component={CourseFormPublish} />
                    <PrivateRoute path="/teach/course/:courseId/page/:id" component={PageForm} />
                  </TeachProvider>
                </>
              )} />

              <Route path="/course" render={() => (
                <>
                  <LearningProvider>
                    <PrivateRoute path="/course/:id" exact component={Course} />
                    <PrivateRoute path="/course/:id/resume" exact component={Course} />
                    <PrivateRoute path="/course/:id/page/:pageId" exact component={CourseDetail} />
                  </LearningProvider>
                  <PrivateRoute path="/course/blocked/:id" exact component={BlockedCourse} />
                </>
              )} />


              <Route path="/my-courses" render={() => (
                <LearningProvider>
                  <PrivateRoute path="/my-courses" exact component={MyCourses} />
                </LearningProvider>
              )} />

              


              <PrivateRoute path="/" exact component={Learn} />
              
              <Route path="/facebook-data-deletion" exact component={DataDeletion} />
              <Route path="/facebook-privacy-policy" exact component={PrivacyPolicy} />
              
              <Route path="/auth" exact component={Login} />
              <Route path="/authenticate/facebook" exact component={FacebookRedirect} />
              <Route path="/create-account" exact component={CreateAccount} />
              <Route path="/activate-account" exact component={ValidateActivationCode} />
              <Route path="/reset-password" exact component={ResetPassword} />

              <PrivateRoute path="/profile" exact component={Profile} />
              <PrivateRoute path="/dashboard" exact component={TeachDashboard} />

              <PrivateRoute isAdminRoute path="/tags" exact component={TagList} />
              <PrivateRoute isAdminRoute path="/tags/:id" exact component={TagForm} />
              <PrivateRoute isAdminRoute path="/users" exact component={UserList} />
              <PrivateRoute isAdminRoute path="/users/:id" exact component={UserForm} />

              <Route component={NotFound} />

            </Switch>
          </NotFoundNestedRoutes>
        </Router>
      </AuthProvider>
    </div >
  );

}

export default App;
