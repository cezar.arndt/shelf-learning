import { Menu, Position, SideSheet, Text } from "evergreen-ui";
import React, { useContext } from "react";
import { Link, useLocation } from "react-router-dom";
import { AuthContext } from "../../context/auth";
import history from "../../history";
import ProfileImage from "../ProfileImage";

import './styles.css';

interface SlideSheetMenuProps {
    isSideShown: boolean,
    setSideShown: (state: boolean) => void
}

export default function SlideSheetMenu(props: SlideSheetMenuProps) {

    const { handleLogout, me } = useContext(AuthContext)

    const mainMenuSmall: any[] = [
        [
            { name: 'Aprender', link: '/' },
            { name: 'Meus cursos', link: '/my-courses' },
        ],
        ['Ensinar'],
        [
            { name: 'Cursos que criei', link: '/teach' },
            { name: 'Criar curso', link: '/teach/new' },
        ]
    ]

    if(me.hasCourses > 0) 
        mainMenuSmall[2].push({ name: 'Estatísticas', link: '/dashboard' })


    const location = useLocation()

    if( location.pathname.indexOf('course') > -1 && location.pathname.indexOf('page') > -1 ) {
        var [,a,b,] = location.pathname.split('/')

        mainMenuSmall.unshift(['Aprender'])
        mainMenuSmall.unshift( [{ name: 'Voltar para página do curso', link: `/${a}/${b}` }])
        mainMenuSmall.unshift(['Curso'])
    }

    const goto = (link:string) => {
        history.push(link)
        props.setSideShown(false)
    }

    let divisionTitle = ''

    return (

        <SideSheet
            width={300}
            position={Position.LEFT}
            isShown={props.isSideShown}
            onCloseComplete={() => props.setSideShown(false)}
        >

            <section className="Header-SmallMenu-User">
                <ProfileImage/>
                <div>
                    <Text>{me.name || me.username}</Text>
                    <Link to="/profile"onClick={()=> history.push('/profile')}>Acessar minha conta</Link>
                    <Link to="" onClick={()=>handleLogout()}>Sair</Link>
                </div>
            </section>
            <section className="Header-SmallMenu-Navigation">
                <Menu>

                    {
                        mainMenuSmall.map( (e,i) => {
                           
                            if( typeof e[0] === 'string' ){
                                divisionTitle = e
                                return <Menu.Divider key={i} />
                            } else {
                                const menuGroup = <Menu.Group title={divisionTitle} key={i}>
                                        {e.map((menu: any) => <Menu.Item key={menu.name} onClick={()=>goto(menu.link)} height={64}>{menu.name}</Menu.Item>)}
                                    </Menu.Group>

                                divisionTitle = ''
                                return menuGroup
                            }
                        }) 
                    }
                </Menu>
            </section>

        </SideSheet>

    )

}