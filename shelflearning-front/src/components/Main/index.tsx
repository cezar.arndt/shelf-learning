import React from "react";

import './styles.css'

export default function Main(props: any) {


    return (
        <div className={`MainContent ${props.className || ''}`} style={{...props.style} || {}}>
            {props.children}
        </div>
    );
}