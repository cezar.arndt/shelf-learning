import { Button, TextInput, Text, Dialog, Spinner } from "evergreen-ui";
import React, { KeyboardEvent, useImperativeHandle, useLayoutEffect, useRef, useState, forwardRef } from "react";


interface InputDialogSection {
    isVisible: boolean
    onClose: () => void
    onSubmit: (value: string) => void
    submitLabel: string
    title: string
    placeHolder: string
    value?: string,
    intent?: "danger" | "none" | "success" | "warning",
    notes?: string
}

function InputDialog(props: InputDialogSection, ref:any) {

    let { isVisible,
        onClose, onSubmit,
        submitLabel, title, placeHolder,
        value,
        intent,
        notes} = props

    const titleRef = useRef<any>()
    const [loading, setLoading] = useState<boolean>()

    const sectionKeyPress = (e: KeyboardEvent) => {
        if (e.key === 'Enter' && titleRef.current) {
            onSubmit(titleRef.current.value)
        }
    }

    useLayoutEffect(() => {
        setTimeout(() => {
            if (titleRef.current) {
                titleRef.current.value = value || ''
                titleRef.current.setSelectionRange(0, titleRef.current.value.length)
            }
        }, 1)
    }, [isVisible])


    useImperativeHandle(ref, () => ({
        setLoading
    }));

    return (
        <Dialog
            isShown={isVisible}
            title={title}
            onCloseComplete={()=>{ setLoading(false); onClose() }}
            hasFooter={false}>

            { notes && <Text><div dangerouslySetInnerHTML={{ __html: notes }}></div></Text>}

            <TextInput name="title"
                ref={titleRef}
                disabled={loading}
                autoComplete={'off'}
                style={{ width: '100%' }}
                onKeyPress={sectionKeyPress}
                autoFocus
                placeholder={placeHolder}
            />

             <Spinner hidden={!loading} size={32} style={{ position: "absolute", bottom: 16, left: 16 }} />

            <Button style={{ float: 'right', marginTop: 16 }}
                disabled={loading}
                onClick={() => onSubmit(titleRef.current.value)}
                intent={intent}
                appearance="primary" >{submitLabel}</Button>

        </Dialog>
    )
}

export default forwardRef(InputDialog)