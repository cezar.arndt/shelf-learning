import React from "react";

import './styles.css';

interface StepsProps {
    steps: number,
    done: number,
    texts?: string[]
}


export default function Stepts(props: StepsProps) {

    const createSteps = (steps: number) => {

        const { texts, done } = props

        const spans: any[] = []
        for (let i = 0; i < steps; i++) {
            spans.push(<span key={i} className={i < done ? 'done' : ''}>{texts ? <label>{texts[i]}</label> : null}</span>)
        }

        return spans
    }

    return (
        <section className="Steps">
            { createSteps(props.steps)}
        </section >
    );
}