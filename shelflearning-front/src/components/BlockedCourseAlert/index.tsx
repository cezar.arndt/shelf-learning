import { Alert, Button, Spinner, Text, toaster } from 'evergreen-ui'
import React, { useState } from 'react'
import * as courseAPI from "../../api/courseAPI";
import { COURSE_STATUS } from '../../api/interfaces';
import InputDialog from '../InputDialog';

export default function BlockedCourseAlert(props: any) {


    const { className = "Form",
        maxWidth = 600,
        statusHistory,
        courseId = undefined,
        showUpdateStatus = false,
        onUpdateStatus = () => {}
    } = props

    const [isChanginStatus, setChangingStatus] = useState<any>()
    const [changingStatusCourse, setShowChangeStatusPopup] = useState<any>()

    const changeStatusCourseHandler = async (reason: string) => {

        setShowChangeStatusPopup(undefined)

        try {
            if (courseId && showUpdateStatus) {
                setChangingStatus(true)

                if( changingStatusCourse === COURSE_STATUS.BLOCKED ) {
                    await courseAPI.instructors.blockCourse(courseId, reason)
                    toaster.success("O motivo foi atualizado, acompanhe as alterações do professor para desbloqueá-lo")

                } else if( changingStatusCourse === COURSE_STATUS.PUBLISHED) {
                    await courseAPI.instructors.unblockCourse(courseId, reason)
                    toaster.success("O curso foi desbloqueado e esta disponível para todos")
                }

                onUpdateStatus()
            }
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setChangingStatus(false)
        }

    }

    return (
        <>
            <section className={className}>
                <Alert
                    style={{ maxWidth }}
                    intent="danger"
                    title={`Esse curso foi bloqueado em ${new Date(statusHistory.atDate).toLocaleString()}`}
                    marginTop={32}
                    marginBottom={32}>

                    <Text>
                        Um administrador bloqueou esse curso, ele esta indisponível para todos os alunos. <br />
                O motivo do bloqueio que foi descrito pelo administrador encontra-se a seguir
            </Text>

                    <blockquote>
                        <Text>{statusHistory.reason}</Text>
                    </blockquote>

                    <Text>
                        Em um curso bloqueado as alterações são restritas. Não é possível criar/remover seções e páginas
                </Text>

                    {showUpdateStatus &&
                        <section style={{ marginTop: 16, display: 'flex', alignItems: 'center' }}>
                            <Button appearance="primary" height={40} intent="danger" disabled={isChanginStatus} onClick={()=> setShowChangeStatusPopup(COURSE_STATUS.BLOCKED)}>
                                {isChanginStatus ? `Atualizizando mensagem` : `Atualizar mensagem`}
                            </Button>
                            <Button marginLeft={16} appearance="primary" height={40} intent="success" disabled={isChanginStatus} onClick={()=> setShowChangeStatusPopup(COURSE_STATUS.PUBLISHED)}>
                                {isChanginStatus ? `Desbloqueando curso` : `Desbloquear curso`}
                            </Button>
                            <Spinner hidden={!isChanginStatus} marginLeft={16} size={32} marginRight={16} />
                        </section>
                    }
                </Alert>
            </section>
            { showUpdateStatus ?
                
                <InputDialog
                    intent={changingStatusCourse === COURSE_STATUS.BLOCKED ? "danger" : "success"}
                    isVisible={changingStatusCourse !== undefined}
                    onClose={() => setShowChangeStatusPopup(undefined)}
                    onSubmit={changeStatusCourseHandler}
                    submitLabel={changingStatusCourse === COURSE_STATUS.BLOCKED ? 'Atualizar motivo' : 'Desbloquear'}
                    title={changingStatusCourse === COURSE_STATUS.BLOCKED ? 'Atualizando motivo' : 'Motivo para o desbloqueio'}
                    notes={changingStatusCourse === COURSE_STATUS.BLOCKED ? 'Atualize a mensagem notificando que ainda existem inconformidades<br/><br/>' : 'Ao desbloquear o curso ele volta a ficar disponível para todos<br/><br/>'}
                    placeHolder={changingStatusCourse === COURSE_STATUS.BLOCKED ? 'Motivo do bloqueio' : 'Não obrigatório' } 
                    value={changingStatusCourse === COURSE_STATUS.BLOCKED ? "" : "O curso já está em conformidade"}/>

                : null}
        </>
    )
}
