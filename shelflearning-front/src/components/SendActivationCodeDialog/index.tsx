import { toaster } from "evergreen-ui";
import React, { useContext, useRef } from "react";
import { AuthContext } from "../../context/auth";
import history from "../../history";
import InputDialog from "../InputDialog";

export default function SendActivationCodeDialog(props: any) {

  const {
    handleSendActivationCode,
    handleSendPasswordCode,
  } = useContext(AuthContext)

  const inputDialogRef = useRef<any>()

  const sendACtivationCode = async (email: string) => {

    inputDialogRef.current.setLoading(true)

    try {

      if (props.activation === true) {
        await handleSendActivationCode(email)
        toaster.success(
          "O código para a ativação da sua conta foi enviado para o seu email",
        )
        history.push('/activate-account')
      } else if (props.password === true) {
        await handleSendPasswordCode(email)
        toaster.success(
          "O link para redefinição da sua senha foi enviado para o seu email",
        )
        history.push('/')
      }

      props.onClose()

    } catch (error) {
      toaster.danger(
        error.message,
      )
    } finally {
      if (inputDialogRef.current) {
        inputDialogRef.current.setLoading(false)
      }
    }

  }


  return (
    <>
      { props.activation &&
        <InputDialog
          intent="success"
          ref={inputDialogRef}
          isVisible={props.isVisible}
          onSubmit={sendACtivationCode}
          onClose={props.onClose}
          submitLabel='Enviar código para meu o e-mail'
          title="Código de ativação"
          placeHolder="Email cadastrado"
          value={props.email || null}
        />
      }

      { props.password &&
        <InputDialog
          intent="success"
          ref={inputDialogRef}
          isVisible={props.isVisible}
          onSubmit={sendACtivationCode}
          onClose={props.onClose}
          submitLabel='Enviar link de redefinição para meu o e-mail'
          title="Link de redefinição de senha"
          placeHolder="Email cadastrado"
          value={props.email || null}
        />
      }
    </>
  );
}
