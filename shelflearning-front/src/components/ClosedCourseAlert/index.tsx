import { Alert } from 'evergreen-ui'
import React, { useContext } from 'react'
import { LearningContext } from '../../context/learning'

export default function ClosedCourseComponent() {

    const { course,
        isAdmin,
        isOwner
    } = useContext(LearningContext)


    if (course && course.status === 'CLOSED') {
        if (course.progress === 100 || isAdmin() || isOwner() ) {
            return (
                <Alert
                    intent="none"
                    title="Esse curso está fechado para novos alunos"
                    marginTop={32}
                    marginBottom={32}
                />
            )
        }

        return (
            <Alert
                intent="none"
                title="Esse curso está fechado para novos alunos, mas você ainda pode concluí-lo"
                marginTop={32}
                marginBottom={32}
            />
        )
    }

    return <></>
}
