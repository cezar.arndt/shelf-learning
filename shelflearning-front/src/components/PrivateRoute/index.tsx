import React, { useContext } from "react";
import { Redirect, Route } from 'react-router-dom';
import { AuthContext } from "../../context/auth";
import useLoggedUser from "../../hooks/useLoggedUser";
import Header from "../../pages/Header";
import NotFound from "../../pages/NotFound";


export default function PrivateRoute({ isAdminRoute=false, component: Component, ...rest }: any) {

    const {isAdmin} = useLoggedUser()
  
    const { loading, authenticated } = useContext(AuthContext);

    if (loading) {
        return <h1></h1>;
    }

    if (!authenticated) {
        <Redirect to="/auth" />
    }

    return (<Route {...rest} render={
        (props) => authenticated ? (
            <>
                { isAdminRoute && !isAdmin()  ? 
                    <NotFound/>
                    : 
                    <>
                        {props.location.pathname.substr(0, 8) === '/course/' ? null : <Header /> }
                        <Component {...props} />
                    </>
                }
            </>
        ) : <Redirect to={{ pathname: "/auth", state: { referer: props.location } }} />
    } />)
}