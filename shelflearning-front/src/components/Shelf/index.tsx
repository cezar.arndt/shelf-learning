import React, { useState } from "react";
import { Avatar, Badge, Button, Heading, Pane, Spinner, Text, toaster } from "evergreen-ui";
import * as courseAPI from "../../api/courseAPI";

import Progress from '../Progress';

import './styles.css';
import history from "../../history";
import { useLocation } from "react-router-dom";

export enum LINK_COURSE {
    TOHOME,
    TOPAGE
}

interface ShelfProps {
    courses: any[]
    title?: string
    goTo: LINK_COURSE
    alwaysShowPct?: boolean,
    enrollOnly?:boolean
}

function PlaceImage(props: any) {

    if (props.img)
        return <div className="Shelf-Image" style={{ backgroundImage: `url("data:${props.img.contentType};base64,${props.img.data}")` }}></div>

    return (<div className="Shelf-Image">
        <Avatar hashValue="id_124" name={props.title} size={100} />
    </div>)
}

export default function Shelf(props: ShelfProps) {

    const { courses, title, alwaysShowPct = false } = props
    const [isExporting, setExporting] = useState<boolean>(false)

    let location = useLocation()

    const goToCourse = (id: string) => {

        if (props.goTo === LINK_COURSE.TOHOME) {
            history.push(`/course/${id}`)
        } else if (props.goTo === LINK_COURSE.TOPAGE) {
            history.push(`/course/${id}/resume`)
        }
    }

    async function exportData() {
        setExporting(true)

        try {
            const search = new URLSearchParams(location.search)
            const filter = search.get('filter') || undefined
            await courseAPI[ props.enrollOnly ? 'exportDataEnroll'  : 'exportData'](filter)    
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setExporting(false)
        }

    }  

    return (
        <section className="Shelf">
            <section style={{ display: "flex", justifyContent: "space-between", alignItems: "baseline" }}>
                <Heading size={600} marginTop="default">{title || null}</Heading>
                <div style={{ display: "flex", alignItems: "center" }}>
                    <Spinner size={24} marginRight={8} style={{visibility:isExporting ? 'visible' : 'hidden'}}/>
                    <Button
                        disabled={isExporting}
                        onClick={exportData}
                        marginTop={4}
                        appearance="minimal">Exportar cursos</Button>
                </div>
            </section>

            <div>
                {courses.map((course, index) =>

                    <Pane
                        onClick={() => goToCourse(course.id)}
                        border={false}
                        key={course.title}
                        display="flex"
                        alignItems="center"
                        justifyContent="center">

                        <PlaceImage img={course.image} title={course.title} />

                        <Progress pct={course.progress} hideLabel alwaysShowPct={course.progress !== undefined || alwaysShowPct} />

                        {course.status === "CLOSED" ?
                            <Badge className="badge-course" color="neutral" isSolid>Encerrado</Badge>
                            : null}

                        {course.status === "BLOCKED" ?
                            <Badge className="badge-course" color="red" isSolid>Bloqueado</Badge>
                            : null}

                        <Heading size={200}>{course.title}</Heading>
                        <Text size={300}>{course.createdBy.name}</Text>
                        <Text size={300}>{`${course.sectionsCount} Seções / ${course.pagesCount} Páginas`}</Text>
                        <div className="Shelf-Tags">
                            {course.tags.map((tag: string) => <Badge key={tag} color="neutral" marginRight={8}>{tag}</Badge>)}
                        </div>
                    </Pane>
                )}
            </div>
        </section >
    );
}