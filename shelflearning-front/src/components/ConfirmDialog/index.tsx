import { Dialog, Text } from "evergreen-ui";
import React, { useLayoutEffect, useState } from "react";


interface ConfirmDialogProps {
    isVisible: boolean
    onClose: () => void
    onSubmit: () => void
    submitLabel?: string
    cancelLabel?: string
    title: string
    text: string
    intentButton?: "none" | "success" | "warning" | "danger" | undefined
}

export default function ConfirmDialog(props: ConfirmDialogProps) {

    const [isLoading, setLoading] = useState<boolean>(false)

    let { isVisible,
        onClose, onSubmit,
        submitLabel = 'Confirm', intentButton,
        cancelLabel = 'Cancel',
        title, text } = props

    useLayoutEffect(()=>{
        if( isVisible === false ){
            setLoading(false)
        }
    },[isVisible])

    return (
        <Dialog
            isShown={isVisible}
            title={title}
            onCloseComplete={onClose}
            isConfirmLoading={isLoading}
            cancelLabel={cancelLabel}
            intent={intentButton}
            confirmLabel={submitLabel}
            onConfirm={() => { setLoading(true); onSubmit() }}
            >
                <Text dangerouslySetInnerHTML={{__html:text}}/>
        </Dialog>
    )
}