import React, { forwardRef, useContext, useEffect, useImperativeHandle, useState } from "react";
import { AddIcon, ArrowDownIcon, ArrowUpIcon, EditIcon, Heading, IconButton, Menu, MoreIcon, Popover, Position, Spinner, TrashIcon, AddToArtifactIcon, Button, toaster } from "evergreen-ui";
import { Link, useParams } from 'react-router-dom';

import * as courseAPI from "../../../api/courseAPI";


import '../styles.css';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import InputDialog from "../../InputDialog";
import { COURSE_STATUS, Section } from "../../../api/interfaces";
import { TeachContext } from "../../../context/teach";
import ConfirmDialog from "../../ConfirmDialog";


interface CourseTreeEditProps {
    disabled: boolean
}

function CourseTreeEdit(props: CourseTreeEditProps, ref: any) {

    const { disabled } = props

    const {
        course,
        addSectionHandler,
        updateSectionTitleHandler,
        changeSectionOrderHandler,
        addPageHandler,
        updatePageTitleHandler,
        changePageOrderHandler,
        removePageHandler,
        removeSectionHandler
    } = useContext(TeachContext)


    const [sections, setSections] = useState<Section[]>([])
    const [isLoading, setLoading] = useState<boolean>(false)
    const [sectionEditing, setSectionEditing] = useState<any>(null)
    const [pageEditing, setPageEditing] = useState<any>(null)
    const [deletePageId, setDeletePageId] = useState<any[]>()

    const [currentSectionId, setCurrentSectionId] = useState(null)
    const [isAddingPage, setAddingPage] = useState(false)
    const [isAddingSection, setAddingSection] = useState(false)
    const [deleteSectionId, setDeleteSectionId] = useState<any>()

    const [isExporting, setExporting] = useState<boolean>(false)


    useEffect(() => {
        if (course && course.sections) {
            setSections([...course.sections])
        }
    }, [course, course?.sections])

    useImperativeHandle(ref, () => ({
        setAddingSection
    }));

    //#region section
    const onEditTitleSection = (id: any, title: string) => {
        setSectionEditing({ id, title })
    }

    const editTitle = async (title: string) => {
        setLoading(true)
        const resp = await updateSectionTitleHandler(sectionEditing.id, title)
        if (resp) {
            const [, updatedSections] = resp
            setSections([...updatedSections])
            setSectionEditing(null)
        }
        setLoading(false)
    }

    const onChangeSectionOrder = async (id: any, direction: number) => {
        setLoading(true)
        const sectionsReordered = await changeSectionOrderHandler(id, direction)
        setSections([...sectionsReordered])
        setLoading(false)
    }

    const addSection = async (title: string) => {
        setLoading(true)
        const section = await addSectionHandler(title)
        if (section) {
            const sec = { ...section, open: false }
            setSections([...sections, sec])
            setAddingSection(false)
        }
        setLoading(false)
    }

    const onRemoveSection = (id: any) => {
        setDeleteSectionId(id)
    }

    const removeSection = async () => {
        await removeSectionHandler(deleteSectionId)
        setDeleteSectionId(undefined)
    }
    //#endregion

    //#region page
    const onAddPage = (sectionId: any) => {
        setCurrentSectionId(sectionId)
        setAddingPage(true)
    }

    const onEditTitlePage = (sectionId: any, id: any, title: string) => {
        setPageEditing({ sectionId, id, title })
    }

    const addPage = async (title: string) => {
        setLoading(true)
        if (await addPageHandler(currentSectionId, title)) {
            setCurrentSectionId(null)
            setAddingPage(false)
        }
        setLoading(false)
    }

    const editPageTitle = async (title: string) => {
        setLoading(true)
        if (await updatePageTitleHandler(pageEditing.id, pageEditing.sectionId, title)) {
            setPageEditing(null)
        }
        setLoading(false)
    }

    const onChangePageOrder = async (sectionId: any, id: any, direction: number) => {
        setLoading(true)
        await changePageOrderHandler(id, sectionId, direction)
        setLoading(false)
    }

    const onRemovePage = (sectionId: any, id: any) => {
        setDeletePageId([sectionId, id])
    }

    const removePage = async () => {
        const [sectionId, id] = deletePageId || []
        await removePageHandler(id, sectionId)
        setDeletePageId(undefined)
    }
    //#endregion

    //#region export
    const exportData = async() => {
        setExporting(true)

        try {
            await courseAPI.instructors.exportTree(course.id)    
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setExporting(false)
        }
    }
    //#endregion

    return (
        <>
            <section className="CourseTree">
                <div style={{marginTop:-20}}>
                    <section style={{ display: 'flex', height: 60, alignItems: "center" }}>
                        <Heading size={600}>Estrutura do curso</Heading>
                        <Spinner hidden={!isLoading} size={16} marginLeft={8} marginTop={4} />
                    </section>

                    <section style={{ display: (course?.sections.length ? 'flex' : 'none'), height: 60, alignItems: "center" }}>
                        <Spinner size={16} marginRight={8} hidden={!isExporting}/>
                        <Button onClick={exportData} disabled={isExporting} appearance="minimal">Exportar</Button>
                    </section>
                </div>

                {sections.map((s: any) => <SectionItem key={s.id} entity={s}
                    onRemovePage={onRemovePage}
                    onRemoveSection={onRemoveSection}
                    onEditTitleSection={onEditTitleSection}
                    onChangeSectionOrder={onChangeSectionOrder}
                    onChangePageOrder={onChangePageOrder}
                    onAddPage={onAddPage}
                    disabled={disabled}
                    onEditTitlePage={onEditTitlePage}
                />)}
            </section>


            <InputDialog
                isVisible={sectionEditing !== null}
                onClose={() => setSectionEditing(null)}
                onSubmit={editTitle}
                submitLabel="Salvar"
                title="Editando seção"
                placeHolder="Título da seção"
                value={sectionEditing && sectionEditing.title} />

            <InputDialog
                isVisible={pageEditing !== null}
                onClose={() => setPageEditing(null)}
                onSubmit={editPageTitle}
                submitLabel="Salvar"
                title="Editando página"
                placeHolder="Título da página"
                value={pageEditing && pageEditing.title} />

            <InputDialog
                isVisible={isAddingPage}
                onClose={() => setAddingPage(false)}
                onSubmit={addPage}
                submitLabel="Adicionar"
                title="Adicionando página"
                placeHolder="Título da página" />

            <InputDialog
                isVisible={isAddingSection}
                onClose={() => setAddingSection(false)}
                onSubmit={addSection}
                submitLabel="Adicionar"
                title="Adicionando seção"
                placeHolder="Título da seção" />


            <ConfirmDialog
                isVisible={deletePageId !== undefined}
                title="Remover Página"
                text="A exclusão não pode ser revertida<br/> você tem certeza que deseja deletar essa página ?"
                onClose={() => setDeletePageId(undefined)}
                onSubmit={() => { removePage() }}
                submitLabel="Sim, deletar página"
                cancelLabel="Não, mudei de idéia"
                intentButton="danger"
            />

            <ConfirmDialog
                isVisible={deleteSectionId !== undefined}
                title="Remover Seção"
                text="A exclusão não pode ser revertida<br/> você tem certeza que deseja deletar essa seção e <strong>todas as suas páginas</strong> ?"
                onClose={() => setDeleteSectionId(undefined)}
                onSubmit={() => { removeSection() }}
                submitLabel="Sim, deletar seção"
                cancelLabel="Não, mudei de idéia"
                intentButton="danger"
            />

        </>
    );
}


interface SectionProps {
    entity: Section,
    onEditTitleSection: (id: any, title: string) => void
    onEditTitlePage: (sectionId: any, id: any, title: string) => void
    onChangeSectionOrder: (id: any, direction: number) => void
    onChangePageOrder: (sectionId: any, id: any, direction: number) => void
    onRemovePage: (sectionId: any, id: any) => void
    onRemoveSection: (id: any) => void
    onAddPage: (sectionId: any) => void,
    disabled: boolean
}
function SectionItem(props: SectionProps) {

    const {
        course,
    } = useContext(TeachContext)

    const { entity, onAddPage, onEditTitleSection, onChangeSectionOrder, onEditTitlePage, onChangePageOrder, onRemovePage, onRemoveSection, disabled } = props
    const { id, title, order, pages } = entity

    const [open, setOpen] = useState(entity.open)

    const openSection = (event: any) => {
        const isClickedOnActions = event.target.closest('[role="dialog"]') || event.target.closest('button')
        if (!isClickedOnActions) {
            setOpen(!open)
        }
    }

    const addPage = () => {
        onAddPage(id)
    }

    return (
        <section className="CourseTree-Section">
            <div className={`${(open ? 'open' : '')} CourseTree-Section-Title`} onClick={openSection}>
                <FontAwesomeIcon style={{ color: '#66788A' }} size="sm" icon={faChevronDown} />
                <Heading size={400}>{title} {pages.length ? <label>{pages.length} Página(s)</label> : null}</Heading>
                {disabled ? <></> :
                    <Popover
                        position={Position.LEFT}
                        content={
                            <Menu>
                                <Menu.Group>
                                    <Menu.Item icon={AddIcon} disabled={course.status !== COURSE_STATUS.DRAFT} intent="success" onClick={addPage}>Adicionar página</Menu.Item>
                                </Menu.Group>
                                <Menu.Divider />
                                <Menu.Group>
                                    <Menu.Item icon={EditIcon} onClick={() => onEditTitleSection(id, title)}>Editar título</Menu.Item>
                                    <Menu.Item icon={TrashIcon} disabled={course.status !== COURSE_STATUS.DRAFT} onClick={() => onRemoveSection(id)} intent="danger">Remover</Menu.Item>
                                </Menu.Group>
                                <Menu.Divider />
                                <Menu.Group>
                                    <Menu.Item icon={ArrowUpIcon} disabled={course.status !== COURSE_STATUS.DRAFT} onClick={() => onChangeSectionOrder(id, -1)}>Mover para cima</Menu.Item>
                                    <Menu.Item icon={ArrowDownIcon} disabled={course.status !== COURSE_STATUS.DRAFT} onClick={() => onChangeSectionOrder(id, +1)}>Mover para baixo</Menu.Item>
                                </Menu.Group>
                            </Menu>
                        }>
                        <IconButton appearance="minimal" className="CourseTreeEdit-Actions" icon={MoreIcon} iconSize={18} />
                    </Popover>}
            </div>

            { open ?
                <>
                    <div className="CourseTree-Section-Pages">
                        {(entity.pages || []).map((p: any) => <Page key={p.id} sectionId={entity.id} disabled={disabled}
                            onChangePageOrder={onChangePageOrder}
                            onRemovePage={onRemovePage}
                            onEditTitlePage={onEditTitlePage} entity={p} />)}
                    </div>
                </>
                : null}
        </section >
    )
}


interface PageProps {
    entity: any
    sectionId: string,
    onEditTitlePage: (sectionId: any, id: any, title: string) => void
    onChangePageOrder: (sectionId: any, id: any, direction: number) => void,
    onRemovePage: (sectionId: any, id: any) => void,
    disabled: boolean
}
function Page(props: PageProps) {

    const {
        course,
    } = useContext(TeachContext)

    const params: any = useParams<any>();

    const { entity, sectionId, onEditTitlePage, onChangePageOrder, disabled, onRemovePage } = props

    const { id, title, descriptionLength, order } = entity

    return (
        <section className="CourseTree-Page">
            <Heading size={400}>{title} {descriptionLength === 0 ? '(Vazio)' : ''}</Heading>

            <Popover
                position={Position.LEFT}
                content={
                    <Menu>
                        <Menu.Group>
                            <Menu.Item icon={AddToArtifactIcon} is={Link} to={`/teach/course/${params.id}/page/${id}`}>
                                {disabled ? `Visualizar página` : `Escrever página`}</Menu.Item>
                        </Menu.Group>
                        {disabled ? <></> : <>
                            <Menu.Divider />
                            <Menu.Group>
                                <Menu.Item icon={EditIcon} onClick={() => onEditTitlePage(sectionId, id, title)}>Editar título</Menu.Item>
                                <Menu.Item icon={TrashIcon} disabled={course.status !== COURSE_STATUS.DRAFT} onClick={() => onRemovePage(sectionId, id)} intent="danger">Remover</Menu.Item>
                            </Menu.Group>
                            <Menu.Divider />
                            <Menu.Group>
                                <Menu.Item icon={ArrowUpIcon} disabled={course.status !== COURSE_STATUS.DRAFT} onClick={() => onChangePageOrder(sectionId, id, -1)}>Mover para cima</Menu.Item>
                                <Menu.Item icon={ArrowDownIcon} disabled={course.status !== COURSE_STATUS.DRAFT} onClick={() => onChangePageOrder(sectionId, id, +1)}>Mover para baixo</Menu.Item>
                            </Menu.Group>
                        </>}
                    </Menu>
                }>
                <IconButton appearance="minimal" icon={MoreIcon} iconSize={18} />
            </Popover>
        </section>
    )
}

export default forwardRef(CourseTreeEdit)