import React, { useContext, useState } from "react";
import { toaster, Heading, Spinner, TickCircleIcon, Button } from "evergreen-ui";
import { useParams } from 'react-router-dom';

import '../styles.css';
import './styles.css';
import { LearningContext } from "../../../context/learning";
import { Section } from "../../../api/interfaces";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import history from "../../../history";

import * as courseAPI from "../../../api/courseAPI";


interface CourseTreeViewProps {
    hasLink?: boolean,
    noTitle?: boolean
    style?: any,
    defaultOpen?:number[]
    expandAll?:boolean
}

function CourseTreeView(props: CourseTreeViewProps) {

    const [isExporting, setExporting] = useState<boolean>(false)

    const { hasLink = false, defaultOpen=[], noTitle = false, style = {}, expandAll = false } = props

    const {
        course
    } = useContext(LearningContext)

    const exportData = async() => {
        setExporting(true)

        try {
            await courseAPI.exportTree(course.id)    
        } catch (error) {
            toaster.danger(
                error.message,
            )
        } finally {
            setExporting(false)
        }
    }

    return (
        <section style={style} className="CourseTree TreeView">
            { !noTitle ? 
                <div style={{justifyContent:"flex-start"}}>
                    <Heading size={700}>Estrutura</Heading>

                    <section style={{ display: (course?.sections.length ? 'flex' : 'none'), margin: '-9px 0 0 16px', alignItems: "center" }}>
                        <Button onClick={exportData} disabled={isExporting}   appearance="minimal">Exportar</Button>
                        <Spinner size={16} marginLeft={8} hidden={!isExporting}/>
                    </section>
                </div>
            : null}

            {course?.sections?.map((s: any) => <SectionItem key={s.id} opened={defaultOpen.indexOf(s.id) > -1} expandAll={expandAll} entity={s} hasLink={hasLink} />)}
        </section>
    )

}

interface SectionItemProps {
    entity: Section,
    hasLink:boolean,
    opened:boolean,
    expandAll:boolean
}

function SectionItem(props: SectionItemProps) {

    const {
        courseId,
        isSectionDone
    } = useContext(LearningContext)

    const { entity: { id, title, pages }, opened, hasLink, expandAll } = props

    const [open, setOpen] = useState(expandAll || opened)

    const openSection = (event: any) => {
        const isClickedOnActions = event.target.closest('[role="dialog"]') || event.target.closest('button')
        if (!isClickedOnActions) {
            setOpen(!open)
        }
    }

    const pageClickHandler = (pageId:string) => {
        if(  hasLink ) {
            history.push(`/course/${courseId}/page/${pageId}`)
        }
    }

    return (
        <section className="CourseTree-Section">
            <div className={`${(open ? 'open' : '')} CourseTree-Section-Title`} onClick={openSection}>
                <FontAwesomeIcon style={{ color: '#66788A' }} size="sm" icon={faChevronDown} />
                <Heading size={400}>{title} {pages.length ? <label>{pages.length} Página(s)</label> : null}</Heading>
                { isSectionDone(id) ? <TickCircleIcon color="success"/>  : null }
            </div>

            { open ?
                <>
                    <div className={["CourseTree-Section-Pages", hasLink ? "clickable" : ""].join(" ")}>
                        {(pages || []).map((p: any) => <Page key={p.id} entity={p} onClick={() => pageClickHandler(p.id)} />) }
                    </div>
                </>
                : null}
        </section>
    )
}

interface PageProps {
    entity: any
    onClick: ()=>void
}
function Page(props: PageProps) {

    const params: any = useParams<any>();

    const {
        isPageDone
    } = useContext(LearningContext)

    const { entity } = props

    const { id, title } = entity

    const styleCurrent = params?.pageId && params?.pageId === id ? {
        fontWeight: 500,
        textDecoration: 'underline'
    } : {}

    return (
        <section className="CourseTree-Page" onClick={ props.onClick }>
            <Heading size={400} style={styleCurrent}>{title}</Heading>
            { isPageDone(id) ? <TickCircleIcon color="success" /> : null}
        </section>
    )
}

export default CourseTreeView;
