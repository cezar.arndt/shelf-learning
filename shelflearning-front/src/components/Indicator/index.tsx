import React from "react";
import { Pane, Text } from "evergreen-ui";


import './styles.css'

export default function Indicator(props: any) {

    return (

        <Pane
            className="Indicator"
            display="flex"
            alignItems="center"
            justifyContent="center"
            border="default"
            style={{ ...props.style } || {}}>
            {props.value}
            <Text>{props.text}</Text>
        </Pane>

    );
}