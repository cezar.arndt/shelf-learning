import React, { useEffect, useRef, useState } from "react";

import './styles.css'
import history from "../../history";
import { CrossIcon, TextInput } from "evergreen-ui";
import { useLocation } from "react-router-dom";


export default function MainSearch(props: any) {

    const {smallOnly = false} = props

    const [isDisabled, setDisabled] = useState(false)
    const [isFiltering, setFiltering] = useState(false)

    const inputSearchRef = useRef<HTMLInputElement>(null)

    let location = useLocation();


    useEffect(() => {

        setDisabled(
            (new RegExp("/teach|/users|/tags|/profile", "g")).test(location.pathname)
        )

        if (inputSearchRef.current) {
            const search = new URLSearchParams(location.search)
            inputSearchRef.current.value = search.get('filter') || ''
        }

    }, [location]);

    const searchKeyPress = (e: KeyboardEvent) => {
        if (e.key === 'Enter' && inputSearchRef.current) {

            const searchLength = inputSearchRef.current.value.length > 0

            setFiltering(searchLength)

            const search = new URLSearchParams(location.search)

            if (searchLength) {
                history.push({
                    search: "?" + new URLSearchParams({ filter: inputSearchRef.current.value }).toString()
                })
            } else if (search.get('filter') && !searchLength) {
                clearFilter()
            }
        }
    }

    const clearFilter = () => {
        if (inputSearchRef.current) {
            setFiltering(false)

            history.push({
                search: ''
            })

            inputSearchRef.current.value = ''
        }
    }


    return (
        <div className={`MainSearch${ smallOnly ? ' sm' : '' }`}>
            {/* @ts-ignore */}
            <TextInput ref={inputSearchRef} disabled={isDisabled} onKeyPress={searchKeyPress} height={48} placeholder="Procure por Titulo ou Tag" />
            {isFiltering ? <CrossIcon onClick={() => clearFilter()} /> : null}

        </div>
    );
}