import React from "react";

import './styles.css'

export default function Progress(props: any) {

    const height = props.height || 15
    const labelTop = -12 + (height/2)
    const hideLabel = props.hideLabel ? 'none' : props.pct === 100 ? 'none' : 'block'
    const alwaysShowPct = props.alwaysShowPct || false
    

    if (props.pct > 0 || alwaysShowPct)
        return (
            <div className="Progress">
                <div style={{ width: `${props.pct}%`, height }}>
                    <label style={{top:labelTop,display:hideLabel}}>{props.pct}%</label>
                </div>
            </div>
        );

    return <></>
}