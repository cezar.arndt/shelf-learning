import React, { useContext } from 'react'
import { Menu, Popover, Position } from 'evergreen-ui'

import './styles.css'
import { AuthContext } from '../../context/auth'
import history from '../../history'
import ProfileImage from '../ProfileImage'

export default function UserSection() {

    const { handleLogout } = useContext(AuthContext)
 
    return (
        <div className="UserSection">
            <Popover
                position={Position.BOTTOM_RIGHT}
                content={
                    <Menu>
                        <Menu.Group>
                            <Menu.Item onClick={()=> history.push('/profile')}>
                                Acessar minha conta
                            </Menu.Item>
                        </Menu.Group>
                        <Menu.Divider />
                        <Menu.Group>
                            <Menu.Item intent="danger" onClick={()=>handleLogout()}>
                                Sair
                            </Menu.Item>
                        </Menu.Group>
                    </Menu>
                }
            >
                <section>
                    <ProfileImage/>
                </section>
            </Popover>
        </div>
    )
}

