import { Avatar } from "evergreen-ui";
import React, { useContext } from "react";
import { AuthContext } from "../../context/auth";

import './styles.css'

interface ProfileImageProps {
    size?: number,
    image?: any
}

export default function ProfileImage(props: ProfileImageProps) {

    const { me } = useContext(AuthContext)

    const { size = 40 } = props

    const image = props.image || me.image

    if (!image) {
        return (<Avatar color="red" name={me.name || me.username} size={40} />)
    }

    return (
        <div className={'ProfileImage'} style={{
            width: size,
            height: size,
            backgroundImage: `url(${image})`
        }}></div>
    );
}